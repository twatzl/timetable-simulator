class Waypoint extends EntityObject implements DrawableMapObject, IChangeable<Waypoint> {
    protected circle: google.maps.Circle;
    private circleDragListener: google.maps.MapsEventListener;
    private circleDragEndListener: google.maps.MapsEventListener;
    private leftClickListener: google.maps.MapsEventListener;
    private rightClickListener: google.maps.MapsEventListener;

    marker: google.maps.Marker;
    private lines: Line[]

    private onDraggedEvent: ObservableEvent<RepositoryEventArgs<Waypoint>>;
    private onDragEndEvent: ObservableEvent<RepositoryEventArgs<Waypoint>>;
    private onLeftClickEvent: ObservableEvent<RepositoryEventArgs<Waypoint>>;
    private onRightClickEvent: ObservableEvent<RepositoryEventArgs<Waypoint>>;
    private onChangedEvent: LockedEvent<RepositoryEventArgs<Waypoint>>;

    public positionBefore: google.maps.LatLng;

    constructor(location: google.maps.LatLng, jsonData?: any) {
        super(WaypointRepository.getInstance().getNextId(), jsonData);

        this.marker = new google.maps.Marker();

        this.circle = new google.maps.Circle({
            strokeColor: '#0080FF',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#0080FF',
            fillOpacity: 0.35,
            draggable: false,
            center: location
        });

        this.onDraggedEvent = new ObservableEvent<RepositoryEventArgs<Waypoint>>();
        this.onDragEndEvent = new ObservableEvent<RepositoryEventArgs<Waypoint>>();
        this.onLeftClickEvent = new ObservableEvent<RepositoryEventArgs<Waypoint>>();
        this.onRightClickEvent = new ObservableEvent<RepositoryEventArgs<Waypoint>>();
        this.onChangedEvent = new LockedEvent<RepositoryEventArgs<Waypoint>>(5);

        this.leftClickListener = google.maps.event.addListener(this.circle, 'click', this.getOnLeftClickEventHandler());
        this.rightClickListener = google.maps.event.addListener(this.circle, 'rightclick', this.getOnRightClickEventHandler());
        this.lines = [];
    }

    public getLines(): Line[] {
        return this.lines;
    }

    public deassignLine(line: Line): void {
        var index = this.lines.indexOf(line)
        if (index < 0) {
            return;
        }
        this.lines.splice(index, 1);
    }

    public getAssignedLines(): Line[] {
        return this.lines;
    }

    public assignLine(line: Line): void {
        this.lines.push(line);
    }

    public getLocation(): google.maps.LatLng {
        return this.circle.getCenter();
    }

    public setLocation(location: google.maps.LatLng): void {
        this.circle.setCenter(location);
    }

    public show(map: google.maps.Map): void {
        this.enableListeners(map);
        this.circle.setMap(map);
    }

    public hide(): void {
        this.disableListeners();
        this.circle.setMap(null);
        this.hideMarker();
    }

    public showMarker(title: string, map: google.maps.Map): void {
        this.marker.setMap(map);
        var label: google.maps.MarkerLabel = { text: title };
        this.marker.setLabel(label);
        this.marker.bindTo("position", this.circle, "center");
    }

    public hideMarker(): void {
        this.marker.setMap(null);
    }

    public highlightWaypoint() {
        this.circle.setOptions({ fillColor: '#FF0000', strokeColor: '#FF0000' });
    }

    public setNormalColorWaypoint() {
        this.circle.setOptions({ fillColor: '#0080FF', strokeColor: '#0080FF' });
    }

    public enableListeners(map: google.maps.Map): void {
        this.circleDragListener = google.maps.event.addListener(this.circle, 'dragstart', this.getOnDragEventHandler());
        this.circleDragEndListener = google.maps.event.addListener(this.circle, 'dragend', this.getOnDragEndEventHandler());
    }

    public disableListeners(): void {
        google.maps.event.removeListener(this.circleDragListener);
        google.maps.event.removeListener(this.circleDragEndListener);
    }

    public reSizeCircle(size: number): void {
        if (size > 0) {
            this.circle.setRadius(size);
        }
    }

    private getOnDragEventHandler(): () => void {
        var waypoint = this;
        return function() {
            var eventArgs = new RepositoryEventArgs(waypoint.id, waypoint);
            waypoint.onDraggedEvent.notifyListeners(eventArgs);
            if (waypoint.getLines().length != 0) {
                if (waypoint.positionBefore == undefined)
                    waypoint.positionBefore = new google.maps.LatLng(waypoint.getLocation().lat(), waypoint.getLocation().lng());
            }
        };
    }


    protected getOnDragEndEventHandler(): () => void {
        var waypoint = this;
        return function() {
            var eventArgs = new RepositoryEventArgs(waypoint.id, waypoint);
            waypoint.onDragEndEvent.notifyListeners(eventArgs);
            waypoint.onChangedEvent.notifyListeners(eventArgs);
        };
    }

    private getOnLeftClickEventHandler(): () => void {
        var waypoint = this;
        return function() {
            waypoint.onLeftClickEvent.notifyListeners(new RepositoryEventArgs(waypoint.id, waypoint));
        }
    }

    private getOnRightClickEventHandler(): () => void {
        var waypoint = this;
        return function() {
            waypoint.onRightClickEvent.notifyListeners(new RepositoryEventArgs(waypoint.id, waypoint));
        }
    }

    public deleted(): void {
        this.hide();
    }

    public getJSONString(): string {
        return "";
    }

    public addWaypointDraggedEventListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onDraggedEvent.addListener(listener);
    }

    public removeWaypointDraggedEventListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onDraggedEvent.removeListener(listener);
    }

    public addWaypointDragEndEventListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onDragEndEvent.addListener(listener);
    }

    public removeDragEndEventListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onDragEndEvent.removeListener(listener);
    }

    public addOnChangedListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onChangedEvent.addListener(listener);
    }

    public removeOnChangedListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onChangedEvent.removeListener(listener);
    }

    public addLeftClickListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onLeftClickEvent.addListener(listener);
    }

    public removeLeftClickListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onLeftClickEvent.removeListener(listener);
    }

    public addRightClickListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onRightClickEvent.addListener(listener);
    }

    public removeRightClickListener(listener: (eventArgs: RepositoryEventArgs<Waypoint>) => void): void {
        this.onRightClickEvent.removeListener(listener);
    }
}