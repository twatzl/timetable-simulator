class StationData {
	// members must be public in order to bind them via angular
	private name: string;
	private size: number;
	private peopleWaiting: number;
	private icon: string; // the icon to show as material-design-icon
	

	constructor() {
		
	}

	public getName(): string {
		return this.name;
	}

	public setName(name: string): void {
		this.name = name;
	}

	public getSize(): number {
		return this.size;
	}

	public setSize(size: number): void {
		if (!isNaN(size)) {
			this.size = size;
		}
	}

	public getPeopleWaiting(): number {
		return this.peopleWaiting;
	}

	public setPeopleWaiting(peopleWaiting: number): void {
		if (!isNaN(peopleWaiting)) {
			this.peopleWaiting = peopleWaiting;
		}
	}

	public getIcon(): string {
		return this.icon
	}

	public setIcon(icon: string): void {
		this.icon = icon;
	}
}