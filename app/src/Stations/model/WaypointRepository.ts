class WaypointRepository extends Repository<Waypoint> {

	private static instance: WaypointRepository;

	constructor() {
		super();
	}

	public static getInstance() {
		if (this.instance === undefined) {
			this.instance = new WaypointRepository();
		}
		return this.instance;
	}
	
	public getStationList(): Dictionary<Station> {
		var waypointList: Dictionary<Waypoint> = this.getObjectList();
		var stationList: Dictionary<Station> = {};
		for (var key in waypointList) {
			if (waypointList[key] instanceof Station) {
				stationList[key] = <Station> waypointList[key];
			}
		}
		
		return stationList;
	}
	
	public getAllStations(): Station[] {
		var stations: Station[] = [];
		var stationDictionary = this.getStationList();
		
		for(var key in stationDictionary) {			
			stations.push(<Station> stationDictionary[key]);
		}
		
		return stations;
	}

}
