class StationSimulation {

	private waitingPeople: Dictionary<Person[]>;

	constructor(jsonData?: any) {
		this.waitingPeople = {};
		if (jsonData != undefined) {
			this.parseFromJson(jsonData);
		}
	}

	public addWaitingPerson(lineName: string, person: Person) {
		if (this.waitingPeople[lineName] == undefined) {
			this.waitingPeople[lineName] = [];
		}
		
		this.waitingPeople[lineName].push();
	}
	
	public collectWaitingPersons(lineName: string, numberOfPersons: number): Person[] {
		var persons: Person[] = [];
		
		for (var i = 0; i < numberOfPersons && this.lineHasPeopleWaiting(lineName); i++) {
			persons.push(this.waitingPeople[lineName].pop());
		}
		return persons;
	}
	
	public lineHasPeopleWaiting(lineName: string) {
		return this.waitingPeople[lineName] != undefined && this.waitingPeople[lineName].length > 0;
	}
	
	public getNumberOfPeopleWaitingForLine(lineName: string): number{
		if (!this.lineHasPeopleWaiting(lineName)) {
			return 0;
		}
		return this.waitingPeople[lineName].length;
	}
	
	public getJSONString(): string {
		return JSON.stringify(this);
	}
	
	public parseFromJson(jsonData: any): void {
		this.waitingPeople = jsonData.waitingPeople;
	}
}