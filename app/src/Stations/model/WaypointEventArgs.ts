class WaypointEventArgs<T> {
	private object: T;

	constructor(object: T) {
		this.object = object;
	}

	public getObject(): T {
		return this.object;
	}
}