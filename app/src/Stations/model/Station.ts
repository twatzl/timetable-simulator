class Station extends Waypoint {
    public data: StationData;

    static mapClickListener: any;
    private circleMouseOverListener: any;
    private circleMouseOutListener: any;
    private circleClickListener: any;
    private onDragEndListener: any;
    private infoWindowCloseClickListener: any;
    private mapZoomedListener: any;

    private infoWindow: google.maps.InfoWindow;

    private lockClosingInfoWindow: boolean;
    private applyButton: any;
    private undoButton: any;

    private APPLY_BUTTON_ID: string;
    private UNDO_BUTTON_ID: string;

    private simulation: StationSimulation;

    private disableApplyButton = false;;

    constructor(name: string, size: number, position: any, jsonData?: any) {
        super(position, jsonData);
        if (jsonData != undefined) {
            this.parseFromJson(jsonData);
        } else {
            this.data = new StationData();
            this.data.setName(name);
            this.data.setSize(size);
            this.data.setPeopleWaiting(0);
            this.simulation = new StationSimulation();
        }
        this.data.setIcon("radio_button_checked");
        this.infoWindow = new google.maps.InfoWindow();

        this.APPLY_BUTTON_ID = "applyButton_" + this.id;
        this.UNDO_BUTTON_ID = "undoButton_" + this.id;
        Finance.getInstance().addBalanceChangeListener(this.getDisableApplyButton());
    }

    public parseFromJson(jsonData: any): void {
        this.data = new StationData();
        this.data.setName(jsonData.name);
        this.data.setSize(jsonData.size);
		this.simulation = jsonData.simulation;
    }

    public getSimulation() {
        return this.simulation;
    }

    static enterAddStationMode(map: google.maps.Map): void {
        if (this.mapClickListener == undefined) {
            this.mapClickListener = google.maps.event.addListener(map, "click", function(event: any) {
                Station.onMapClickedInCreateStationMode(event, map);
            });
        }
    }

    static onMapClickedInCreateStationMode(event: any, map: google.maps.Map): void {
        var status = Finance.getInstance().handleTransaction(new Transaction(TransactionTypes.PURCHASE, Finance.getInstance().getFinanceData().getStationPrice()), false);
        if (status) {
            var newStation = new Station("Station", Station.getNewStationSize(map), event.latLng);
            WaypointRepository.getInstance().addObject(newStation);
            newStation.show(map);
            newStation.enableEditListeners(map);
        }
        Station.leaveAddStationMode();
    }

    static leaveAddStationMode(): void {
        google.maps.event.removeListener(this.mapClickListener);
        this.mapClickListener = undefined;
    }

    public show(map: google.maps.Map): void {
        super.show(map);
        this.enableListeners(map);
        this.infoWindow.bindTo("position", this.circle, "center");
    }

    public hide(): void {
        super.hide();
        this.disableListeners();
    }

    public enableListeners(map: google.maps.Map): void {
        super.enableListeners(map);
        this.reSizeCircle(this.data.getSize());
        this.mapZoomedListener = google.maps.event.addListener(map, 'zoom_changed', this.getSetNewZoomHandlerFunction(this, map));
        this.onDragEndListener = google.maps.event.addListener(this.circle, 'dragend', this.getOnDragEndEventHandlerFunction(this, map));
    }

    public disableListeners(): void {
        super.disableListeners();
        google.maps.event.removeListener(this.mapZoomedListener);
    }

    public enableEditListeners(map: google.maps.Map): void {
        this.circle.setDraggable(true);
        this.circleMouseOverListener = google.maps.event.addListener(this.circle, 'mouseover', this.getMouseOverHandlerFunction(this, map));
        this.circleMouseOutListener = google.maps.event.addListener(this.circle, 'mouseout', this.getMouseOutHandlerFunction(this));
    }

    public disableEditListeners(): void {
        Station.leaveAddStationMode();
        this.circle.setDraggable(false);
        google.maps.event.removeListener(this.circleClickListener);
        google.maps.event.removeListener(this.circleMouseOverListener);
        google.maps.event.removeListener(this.circleMouseOutListener);
        google.maps.event.removeListener(this.onDragEndListener);
    }

    private getSetNewZoomHandlerFunction(station: Station, map: google.maps.Map): any {
        return function() {
            station.data.setSize(Station.getNewStationSize(map));
            station.reSizeCircle(station.data.getSize());
        }
    }

    static getNewStationSize(map: google.maps.Map): number {
        return (22 - map.getZoom()) * 3.5;
    }

    private getPresentationInfoWindowContent(): string {
        var lines = this.getLines();
        var infoWindowContent = "<h3 style='color:#4285F4'> " + this.data.getName() + "</h3>";
        for (var i = 0; i < lines.length; i++) {
            //"4 people are waiting for line 2"
            infoWindowContent += "<p>Line " + lines[i].getId() + ": " + this.simulation.getNumberOfPeopleWaitingForLine(lines[i].getId()) + " people waiting.</p>"
        }

        return infoWindowContent;
    }

    private getMoveInfoWindowContent(): string {
        var infoWindowContent =
            "<div>" +
            "<form>" +
            "<div>" +
            "<h3 style='color:#4285F4'>Is this the new position?</h3>" +
            "<br>" +
            "<div class='btn-group small-margin'>";
        if (this.disableApplyButton) {
            infoWindowContent += "<input type='button' disabled class='btn btn-default' id='" + this.APPLY_BUTTON_ID + "' value='Apply'>";
        }
        else {
            infoWindowContent += "<input type='button' class='btn btn-default' id='" + this.APPLY_BUTTON_ID + "' value='Apply'>";
        }
        infoWindowContent += "<input type='button' class='btn btn-default' id='" + this.UNDO_BUTTON_ID + "' value='Undo'>" +
            "</div>" +
            "</div>" +
            "</form>" +
            "</div>";
        return infoWindowContent;
    }

    public setApplyButtonClickListener(func: () => void): void {
        this.applyButton = this.getStationApplyButtonFromInfoWindow(this);
        this.applyButton.onclick = func;
    }

    public setUndoButtonClickListener(func: () => void): void {
        this.undoButton = this.getStationUndoButtonFromInfoWindow(this);
        this.undoButton.onclick = func;
    }

    private applyButtonAction(): () => void {
        var station = this;
        return function() {
            var distance = (google.maps.geometry.spherical.computeDistanceBetween(station.positionBefore, station.getLocation()));
            var price = Finance.getInstance().getFinanceData().calculateStationToLineAssignmentPrice(distance);
            var status = Finance.getInstance().handleTransaction(new Transaction(TransactionTypes.PURCHASE, price), false);

            if (!status) {
                station.undoButtonAction()();
            }
            else {
                station.positionBefore = undefined;
            }

            station.lockClosingInfoWindow = false;
            station.closeInfoWindow();
        }
    }

    private undoButtonAction(): () => void {
        var station = this;
        return function() {
            if (station.positionBefore != undefined) {
                station.setLocation(station.positionBefore);
                station.positionBefore = undefined;
            }
            station.lockClosingInfoWindow = false;
            station.closeInfoWindow();

            station.getOnDragEndEventHandler()();
        }
    }

    private closeInfoWindow(): void {
        this.infoWindow.close();
        google.maps.event.removeListener(this.infoWindowCloseClickListener);
    }

    public closeInfoWindowWhenNoMoveWindowIsOpen() {
        if (!this.lockClosingInfoWindow) {
            this.infoWindow.close();
        }
    }

    private getStationApplyButtonFromInfoWindow(station: Station): any {
        return document.getElementById(station.APPLY_BUTTON_ID);
    }

    private getStationUndoButtonFromInfoWindow(station: Station): any {
        return document.getElementById(station.UNDO_BUTTON_ID);
    }

    public getMouseOverHandlerFunction(station: Station, map: google.maps.Map): () => void {
        return function() {
            station.setInfoWindowValuesAndOpenIt(station, map, station.getPresentationInfoWindowContent());
        };
    }

    private getOnDragEndEventHandlerFunction(station: Station, map: google.maps.Map): () => void {
        return function() {
            if (station.getLines().length != 0) {
                station.getDisableApplyButton()();
                station.setInfoWindowValuesAndOpenIt(station, map, station.getMoveInfoWindowContent());
                station.lockClosingInfoWindow = true;
                station.setApplyButtonClickListener(station.applyButtonAction());
                station.setUndoButtonClickListener(station.undoButtonAction());

                station.infoWindowCloseClickListener = google.maps.event.addListener(station.infoWindow, 'closeclick', function() {
                    station.undoButtonAction()();
                });
            }
        };
    }

    private getMouseOutHandlerFunction(station: Station): () => void {
        return function() {
            station.closeInfoWindowWhenNoMoveWindowIsOpen();
        };
    }

    private setInfoWindowValuesAndOpenIt(station: Station, map: google.maps.Map, newInfoWindowContent: string): void {
        if (!station.lockClosingInfoWindow) {
            station.infoWindow.setContent(newInfoWindowContent);
            station.infoWindow.setPosition(station.getLocation());
            station.infoWindow.open(map);
        }
    }

    public getJSONString(): string {
        return JSON.stringify({
            location: this.getLocation(),
            size: this.data.getSize(),
            name: this.data.getName(),
            id: this.id,
            peopleWaiting: this.data.getPeopleWaiting(), 
			simulation: this.simulation
        });
    }

    private getDisableApplyButton(): () => void {
        var station = this;
        return function() {
            if (station.positionBefore != undefined) {
                var distance = (google.maps.geometry.spherical.computeDistanceBetween(station.positionBefore, station.getLocation()));
                var price = Finance.getInstance().getFinanceData().calculateStationToLineAssignmentPrice(distance);
                station.disableApplyButton = !Finance.getInstance().checkForEnoughtMoney(price);
            }
        }
    }
}