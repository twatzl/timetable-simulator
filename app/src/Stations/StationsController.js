var station;
app.controller('StationsCtrl', function($scope, googleMapsService) {
    $scope.selectedIndex = -1;
    $scope.stationInfoWindowText = "Show Station Info";
    $scope.showInfoWindows = false;

    $scope.init = function($scope) {
        var repository = WaypointRepository.getInstance();
        $scope.stations = repository.getObjectList();
        onStationsTabEntered.addListener(enterStationTab(googleMapsService.map));
        onStationsTabLeft.addListener(leaveStationTab());
    }

    $scope.init($scope);

    $scope.onStationAdded = function() {
        Station.enterAddStationMode(googleMapsService.map);
    }

    $scope.onStationRemovedById = function(id) {
        WaypointRepository.getInstance().getObjectById(id).hide(googleMapsService.map);
        WaypointRepository.getInstance().deleteObjectById(id);
        $scope.selectedStation = null;
    }

    $scope.onSelectedStationRemoved = function() {
        $scope.selectedStation.hide(googleMapsService.map);
        WaypointRepository.getInstance().deleteObjectById($scope.selectedStation.id);
        $scope.selectedStation = null;
    }

    $scope.onStationSelected = function(id, $index) {
        if ($scope.selectedStation != undefined) {
            $scope.selectedStation.setNormalColorWaypoint();
        }
        $scope.selectedStation = WaypointRepository.getInstance().getObjectById(id);
        $scope.selectedIndex = $index;
        $scope.selectedStation.highlightWaypoint();
        station = $scope.selectedStation;
    }

    $scope.disableInput = function() {
        return $scope.selectedStation == undefined;
    };

    $scope.$watch('selectedStation.data.size', function() {
        if ($scope.selectedStation != undefined) {
            $scope.selectedStation.reSizeCircle(parseInt($scope.selectedStation.data.size));
        }
    });

    $scope.onShowOrHideStationInfoWindowsButton = function() {
        var stations = WaypointRepository.getInstance().getAllStations();
        if (!$scope.showInfoWindows) {
            $scope.showInfoWindows = true;
            $scope.stationInfoWindowText = "Hide Station Info";
            for (i = 0; i < stations.length; i++) {
                stations[i].getMouseOverHandlerFunction(stations[i], googleMapsService.map)();
            }
        }
        else {
            $scope.showInfoWindows = false;
            $scope.stationInfoWindowText = "Show Station Info";
            for (i = 0; i < stations.length; i++) {
                stations[i].closeInfoWindowWhenNoMoveWindowIsOpen();
            }
        }
    };
});


var leaveAddStationModeRightClickListener;

function enterStationTab(map) {
    return function() {
        var stations = WaypointRepository.getInstance().getObjectList();
        for (var key in stations) {
            stations[key].enableEditListeners(map);
        }
        if (station != undefined) {
            station.highlightWaypoint();
        }
        leaveAddStationModeRightClickListener = google.maps.event.addListener(map, 'rightclick', function() { Station.leaveAddStationMode(); });
    }
}

function leaveStationTab() {
    return function() {
        var stations = WaypointRepository.getInstance().getObjectList();
        for (var key in stations) {
            stations[key].disableEditListeners();
        }
        if (station != undefined) {
            station.setNormalColorWaypoint();
        }
        google.maps.event.removeListener(leaveAddStationModeRightClickListener);
    }
}