app.controller('ZonesCtrl', function ($scope, googleMapsService, googleDrawingManagerService) {
	var remote = require('remote');
	var dialog = remote.require('dialog');
	var fs = require('fs');
	$scope.active = {};
	$scope.active["editmode"] = true;
	$scope.active["lockmode"] = false;

	var zoneManager;

	$scope.init = function ($scope) {
		var repository = ZoneRepository.getInstance();
		$scope.zones = repository.getObjectList();
		zoneManager = new ZoneManager(googleMapsService.map);
	}

	$scope.init($scope);

	$scope.onIndustryZoneCreateButtonPressed = function (densityId) {
		zoneManager.enterCreateZoneMode(googleDrawingManagerService.drawingManager, 0, densityId);
	}

	$scope.onResidentialZoneCreateButtonPressed = function (densityId) {
		zoneManager.enterCreateZoneMode(googleDrawingManagerService.drawingManager, 1, densityId);
	}

	$scope.onCommercialZoneCreateButtonPressed = function (densityId) {
		zoneManager.enterCreateZoneMode(googleDrawingManagerService.drawingManager, 2, densityId);
	}
	
	$scope.toggleButtons = function (editMode, lockMode) {
		$scope.active["editmode"] = editMode;
		$scope.active["lockmode"] = lockMode;
	}

	$scope.onEditModeButtonPressed = function () {
		$scope.toggleButtons(true, false);
		zoneManager.enterEditModeListeners();
	}

	$scope.onLockModeButtonPressed = function () {
		$scope.toggleButtons(false, true);
		zoneManager.enterLockModeListeners();
	}

	$scope.onSaveButtonPressed = function () {
		dialog.showSaveDialog(
			{
				filters: [{ name: 'json', extensions: ['json'] }]
			}, function (fileName) {


				if (fileName === undefined) {
					return;
				}
				MapSaver.save(fileName, fs, dialog);
			});
	}

	$scope.onLoadButtonPressed = function () {
		dialog.showOpenDialog(
			{
				filters: [{ name: 'json', extensions: ['json'] }]
			}, function (fileName) {

				if (fileName === undefined) {
					return;
				}
				zoneManager.clearZoneRepository();
				zoneManager.fillZoneRepository(MapLoader.load(fileName[0], fs, dialog));
			});
	}
});