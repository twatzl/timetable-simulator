enum ZoneType {
    Industry,
    Residential,
    Commercial
}

enum DensityType {
    Dense,
    Medium,
    Light
}

class ZoneManager extends ObjectManager<Zone> {
    map: google.maps.Map;
    rectangleCompleteListener: google.maps.MapsEventListener;
    abordCreateZoneListener: google.maps.MapsEventListener;
    mapDragStartListener: google.maps.MapsEventListener;
    enumZoneType: ZoneType;
    fillOpacityOfZones: number;
    isInEditMode: boolean;

    constructor(map: google.maps.Map) {
        super();
        this.map = map;
        this.fillOpacityOfZones = 0.6;
        this.isInEditMode = true;
    }

    public enterCreateZoneMode(drawingManager: google.maps.drawing.DrawingManager, zoneType: number, densityType: number): void {
        this.mapDragStartListener = google.maps.event.addListener(this.map, 'dragstart', this.getCreateZoneAbordHandlerFunction(this, drawingManager));

        drawingManager.setMap(this.map);
        drawingManager.setOptions({
            drawingControl: true,
            rectangleOptions: {
                fillColor: this.getZoneColor(ZoneType[zoneType], DensityType[densityType]),
                fillOpacity: this.fillOpacityOfZones,
                draggable: true,
                editable: true,
                zIndex: 1
            }
        });

        this.createNewZone(this.map, drawingManager, ZoneType[zoneType], DensityType[densityType]);
    }

    public createNewZone(map: google.maps.Map, drawingManager: google.maps.drawing.DrawingManager, zoneType: string, densityType: string): void {
        this.freeListeners(this);
        this.rectangleCompleteListener = google.maps.event.addListener(drawingManager, 'rectanglecomplete', this.getRectangleCompleteHandlerFunction(this, zoneType, densityType, map, drawingManager));
        this.abordCreateZoneListener = google.maps.event.addListener(map, 'rightclick', this.getCreateZoneAbordHandlerFunction(this, drawingManager));
    }

    private getRectangleCompleteHandlerFunction(zoneManager: ZoneManager, zoneType: string, densityType: string, map: google.maps.Map, drawingManager: google.maps.drawing.DrawingManager): any {
        return function(rectangle: google.maps.Rectangle) {
            var newZone = new Zone("Zone", zoneType, densityType);
            newZone.rectangle = rectangle;
            newZone.setBounds(rectangle);
            newZone.show(map);
            newZone.enableListeners(),

                ZoneRepository.getInstance().addObject(newZone);
            zoneManager.freeListeners(zoneManager);
            zoneManager.disableDrawingManager(drawingManager);
        };
    }

    private getCreateZoneAbordHandlerFunction(zoneManager: ZoneManager, drawingManager: google.maps.drawing.DrawingManager): any {
        return function() {
            zoneManager.freeListeners(zoneManager);
            zoneManager.disableDrawingManager(drawingManager);
        }
    }

    public freeListeners(zoneManager: ZoneManager): void {
        google.maps.event.removeListener(zoneManager.mapDragStartListener);
        google.maps.event.removeListener(zoneManager.rectangleCompleteListener);
        google.maps.event.removeListener(zoneManager.abordCreateZoneListener);
    }

    public disableDrawingManager(drawingManager: google.maps.drawing.DrawingManager): void {

        drawingManager.setOptions({
            drawingControl: false
        });

        drawingManager.setMap(null);
    }

    public clearZoneRepository(): void {
        var zones = ZoneRepository.getInstance().getObjectList();

        for (var key in zones) {
            zones[key].deleted();
            ZoneRepository.getInstance().deleteObjectById(zones[key].id);
        }
    }

    public fillZoneRepository(jsonObj: any): void {
        for (var i = 0; i < jsonObj.zones.length; i++) {
            this.createZoneObject(jsonObj.zones[i]);
        }
    }

    public createZoneObject(JSONZone: any): void {
        var newZone = new Zone(JSONZone.name, JSONZone.type, JSONZone.density);

        var rectangle = new google.maps.Rectangle({
            fillColor: this.getZoneColor(JSONZone.type, JSONZone.density),
            fillOpacity: this.fillOpacityOfZones,
            zIndex: 1,
        });

        rectangle.setBounds(new google.maps.LatLngBounds(
            new google.maps.LatLng(JSONZone.south, JSONZone.west),
            new google.maps.LatLng(JSONZone.north, JSONZone.east)));

        newZone.setRectangle(rectangle);
        newZone.show(this.map);
        if (this.isInEditMode) {
            newZone.enableListeners();
        }
        ZoneRepository.getInstance().addObject(newZone);
    }

    public showZones(): void {
        for (var tempZone in ZoneRepository.getInstance().getObjectList()) {
            ZoneRepository.getInstance().getObjectById(tempZone).show(this.map);
        }
    }

    public hideZones(): void {
        for (var tempZone in ZoneRepository.getInstance().getObjectList()) {
            ZoneRepository.getInstance().getObjectById(tempZone).hide();
        }
    }

    public enterEditModeListeners(): void {
        this.isInEditMode = true;
        var zones = ZoneRepository.getInstance().getObjectList();

        for (var key in zones) {
            zones[key].enableListeners();
        }
    }

    public enterLockModeListeners(): void {
        this.isInEditMode = false;
        var zones = ZoneRepository.getInstance().getObjectList();

        for (var key in zones) {
            zones[key].disableListeners();
        }
    }

    public getZoneColor(zoneTypeString: string, zoneDensityString: string): string {
        switch (zoneTypeString) {
            case ZoneType[0]:
                if (DensityType[0] === zoneDensityString) {
                    return '#a86d00';
                }
                else if (DensityType[1] === zoneDensityString) {
                    return '#FFA500';
                }
                else {
                    return '#ffd68a';
                }
            case ZoneType[1]:
                if (DensityType[0] === zoneDensityString) {
                    return '#005c00';
                }
                else if (DensityType[1] === zoneDensityString) {
                    return '#00b800';
                }
                else {
                    return '#61ff61';
                }
            case ZoneType[2]:
                if (DensityType[0] === zoneDensityString) {
                    return '#001357';
                }
                else if (DensityType[1] === zoneDensityString) {
                    return '#5277ff';
                }
                else {
                    return '#a8bbff';
                }
            default:
                return '#FFFFFF';
        }
    }
}