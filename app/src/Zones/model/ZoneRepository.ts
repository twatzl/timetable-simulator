class ZoneRepository extends Repository<Zone> {

	private static instance: ZoneRepository;

	constructor() {
		super();
	}

	public static getInstance() {
		if (this.instance === undefined) {
			this.instance = new ZoneRepository();
		}
		return this.instance;
	}
}
