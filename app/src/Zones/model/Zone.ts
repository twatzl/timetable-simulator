class Zone extends EntityObject implements DrawableMapObject {
    private name: string;
    private type: string;
    private density: string;
    northEast: google.maps.LatLng;
    southWest: google.maps.LatLng;

    boundsChangedListener: google.maps.MapsEventListener;
    rectangleDeleteListener: google.maps.MapsEventListener;
    rectangle: google.maps.Rectangle;

    constructor(name: string, type: string, density: string) {
        super(ZoneRepository.getInstance().getNextId());

        this.name = name;
        this.type = type;
        this.density = density;
    }

    public show(map: google.maps.Map): void {
        this.rectangle.setMap(map);
    }

    public hide(): void {
        this.rectangle.setMap(null);
    }

    public delete(): void {
        ZoneRepository.getInstance().deleteObjectById(this.id);
    }

    public enableListeners(): void {
        this.rectangle.setDraggable(true);
        this.rectangle.setEditable(true);
        this.boundsChangedListener = google.maps.event.addListener(this.rectangle, 'bounds_changed', this.getBoundsChangedHandlerFunction(this));
        this.rectangleDeleteListener = google.maps.event.addListener(this.rectangle, 'rightclick', this.getRectangleDeleteHandlerFunction(this));
    }

    public disableListeners(): void {
        this.rectangle.setDraggable(false);
        this.rectangle.setEditable(false);
        google.maps.event.removeListener(this.boundsChangedListener);
        google.maps.event.removeListener(this.rectangleDeleteListener);
    }

    private getBoundsChangedHandlerFunction(zone: Zone): any {
        return function() {
            zone.setBounds(zone.rectangle);
        }
    }

    private getRectangleDeleteHandlerFunction(zone: Zone): any {
        return function() {
            zone.delete();
        }
    }

    public getName(): string {
        return this.name;
    }

    public getArea(): number {
        var southWest = new google.maps.LatLng(this.southWest.lat(), this.southWest.lng());
        var northEast = new google.maps.LatLng(this.northEast.lat(), this.northEast.lng());
        var southEast = new google.maps.LatLng(this.southWest.lat(), this.northEast.lng());
        var northWest = new google.maps.LatLng(this.northEast.lat(), this.southWest.lng());
        return google.maps.geometry.spherical.computeArea([northEast, northWest, southWest, southEast]);
    }

    public getDensity(): string {
        return this.density;
    }

    public getType(): string {
        return this.type;
    }

    public generateRandomLocationInZone(): GoogleLocation {
        var width = (Math.random() * (this.northEast.lng() - this.southWest.lng())) + this.southWest.lng();
        var height = (Math.random() * (this.northEast.lat() - this.southWest.lat())) + this.southWest.lat();
        return new GoogleLocation(new google.maps.LatLng(height, width));
    }

    public setRectangle(rectangle: google.maps.Rectangle): void {
        this.rectangle = rectangle;
        this.setBounds(rectangle);
    }

    public setBounds(rectangle: google.maps.Rectangle): void {
        this.northEast = rectangle.getBounds().getNorthEast();
        this.southWest = rectangle.getBounds().getSouthWest();
    }

    public deleted(): void {
        this.hide();
    }

    public getJSONString(): string {
        return '{\"name\":\"' + this.name + '\", \"type\":\"' + this.type + '\", \"density\":\"' + this.density + '\", \"north\":\"' + this.northEast.lat() + '\", \"east\":\"' + this.northEast.lng() + '\", \"south\":\"' + this.southWest.lat() + '\", \"west\":\"' + this.southWest.lng() + '\"}';
    }
}