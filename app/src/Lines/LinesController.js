app.controller('LinesCtrl', function($scope, googleMapsService, $uibModal) {
    $scope.selectedIndex = -1;
    $scope.lines = LineRepository.getInstance().getObjectList();
    $scope.stationAssignmentModeEnabled = false;
    $scope.unboundLine = false;
    $scope.streetBoundLine = false;

    Finance.getInstance().addBalanceChangeListener(getStreetBoundLineDisableCallback($scope));
    Finance.getInstance().addBalanceChangeListener(getUnboundLineDisableCallback($scope));

    onLinesTabLeft.addListener(getTabLeftCallback($scope));

    $scope.isStationAssignmentModeEnabled = function() {
        return $scope.stationAssignmentModeEnabled;
    }

    $scope.onAddUnboundLineButtonPressed = function() {
        var status = Finance.getInstance().handleTransaction(new Transaction(TransactionTypes.PURCHASE, Finance.getInstance().getFinanceData().getLinePrice()), false);
        if (status) {
            var newLine = new UnboundLine();
            saveNewLineToRepositoryAndDisplay(newLine, googleMapsService.map);
            selectLine($scope, googleMapsService.map, newLine.id);
            $scope.selectedIndex = $scope.lines.length - 1;
        }
    }

    $scope.onAddStreetBoundLineButtonPressed = function() {
        var status = Finance.getInstance().handleTransaction(new Transaction(TransactionTypes.PURCHASE, Finance.getInstance().getFinanceData().getLinePrice()), false);
        if (status) {
            var newLine = new StreetBoundLine();
            saveNewLineToRepositoryAndDisplay(newLine, googleMapsService.map);
            selectLine($scope, googleMapsService.map, newLine.id);
            $scope.selectedIndex = $scope.lines.length - 1;
        }
    }

    $scope.onLineSelected = function(id, $index) {
        selectLine($scope, googleMapsService.map, id);
        $scope.selectedIndex = $index;
    }

    $scope.onLineRemovedById = function(id) {
        LineRepository.getInstance().deleteObjectById(id);
        $scope.selectedLine = undefined;
    }

    $scope.onSelectedLineRemoved = function() {
        LineRepository.getInstance().deleteObjectById($scope.selectedLine.id);
        $scope.selectedLine = undefined;
    }

    $scope.disableInput = function() {
        return $scope.selectedLine === undefined;
    };

    $scope.toggleStationAssignmentMode = function() {
        if (!$scope.isStationAssignmentModeEnabled()) {
            // enter station assignment mode
            $scope.stationAssignmentModeEnabled = true;
            $scope.selectedLine.getWaypointManager().enterStationAssignmentMode();
        } else {
            // leave station assignment mode
            $scope.stationAssignmentModeEnabled = false;
            $scope.selectedLine.getWaypointManager().leaveStationAssignmentMode();
        }
    }

    $scope.onStationRemovedById = function(id) {
        $scope.selectedLine.removeWayopintById(id);
    }

    $scope.open = function(size) {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'partials/Timetable/ModalTimetables.html',
            controller: 'ModalTimetableCtrl',
            windowClass: 'app-modal-window',
            size: "lg",
            backdrop: 'static',
            resolve: {
                line: function() {
                    return $scope.selectedLine;
                },
                isHelpButtonPressed: function() {
                    return $scope.isHelpButtonPressed;
                },
                helpTexts: function() {
                    return $scope.helpTexts;
                }

            }
        });

        modalInstance.result.then(function(data) {
            onModalViewClosed(googleMapsService.map);
        }, function(canceled) {
            onModalViewClosed(googleMapsService.map);
        });

    };

    $scope.toggleAnimation = function() {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };
});

function saveNewLineToRepositoryAndDisplay(newLine, map) {
    LineRepository.getInstance().addObject(newLine);
    newLine.data.setName("Line " + newLine.id);
    newLine.show(map);
}

function selectLine($scope, map, id) {
    if ($scope.selectedLine != undefined) {
        if ($scope.isStationAssignmentModeEnabled()) {
            $scope.toggleStationAssignmentMode();
        }
        $scope.selectedLine.hideWaypointMarkers();
    }
    $scope.selectedLine = LineRepository.getInstance().getObjectById(id);
    $scope.selectedLine.showWaypointMarkers(map);
}

function onModalViewClosed(map) {
    showAllVehiclesAndStartSimulation(map);
}

function showAllVehiclesAndStartSimulation(map) {
    VehicleInstanceRepository.getInstance().startSimulationAndShowAllVehicles(map);
}

function getTabLeftCallback($scope) {
    return function() {
        if ($scope.stationAssignmentModeEnabled) {
            $scope.toggleStationAssignmentMode();
            console.log("leaving assignment mode")
        }
        console.log("leaving stations tab")
    }
}

function getStreetBoundLineDisableCallback($scope) {
    return function() {
        $scope.streetBoundLine = !Finance.getInstance().checkForEnoughtMoney(Finance.getInstance().getFinanceData().getLinePrice());
    }
}

function getUnboundLineDisableCallback($scope) {
    return function() {
        $scope.unboundLine = !Finance.getInstance().checkForEnoughtMoney(Finance.getInstance().getFinanceData().getLinePrice());
    }
}
