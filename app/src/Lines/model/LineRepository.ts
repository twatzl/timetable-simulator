class LineRepository extends Repository<Line> {

	private static instance: LineRepository;

	constructor() {
		super();
	}

	public static getInstance() {
		if (this.instance === undefined) {
			this.instance = new LineRepository();
		}
		return this.instance;
	}
	
	public getAllLines(): Line[] {
		var lines: Line[] = [];
		
		for (var key in this.entities) {
			lines.push(this.entities[key])
		}
		
		return lines;
	}

}
