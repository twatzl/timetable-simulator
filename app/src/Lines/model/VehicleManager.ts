class VehicleManager extends ObjectManagerExtensionBase<VehicleInstance> {

	public static NAME(): string { return "VehicleManager"; }

	constructor() {
		super(new DrawableMapObjectManager<VehicleInstance>(
			new EntityObjectManager(new ObjectManager<VehicleInstance>(), VehicleInstanceRepository.getInstance())));
		this.registerExtension(VehicleManager.NAME(), this);
	}

}