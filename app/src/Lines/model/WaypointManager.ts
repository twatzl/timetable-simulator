class WaypointManager extends ObjectManagerExtensionBase<Waypoint> {

    public static NAME(): string { return "WaypointManager"; }
    // the maximum distance two waypoints can be away from each other
    // and still be considered as equal
    private static WAYPOINT_EQUAL_DISTANCE = 5;

    private addStationClickCallback: (eventArgs: RepositoryEventArgs<Waypoint>) => void;
    private removeStationClickCallback: (eventArgs: RepositoryEventArgs<Waypoint>) => void;

    private line: Line;

    constructor(line: Line) {
        super(WaypointManager.changeable(
				WaypointManager.drawable(
					WaypointManager.entity(
						WaypointManager.manager()))));

        this.registerExtension(WaypointManager.NAME(), this);

        this.line = line;
    }
	
	/** shitty name because all others were already taken:
	 * - objectManager: taken by base class
	 * - getObjectManager: also taken by base class
	 */
    private static manager(): ObjectManager<Waypoint> {
        return new ObjectManager<Waypoint>();
    }

    private static entity(objectManager: IObjectManager<Waypoint>): EntityObjectManager<Waypoint> {
        return new EntityObjectManager<Waypoint>(objectManager, WaypointRepository.getInstance());
    }

    private static drawable(objectManager: IObjectManager<Waypoint>): DrawableMapObjectManager<Waypoint> {
        return new DrawableMapObjectManager<Waypoint>(objectManager);
    }

    private static changeable(objectManager: IObjectManager<Waypoint>): ChangeableObjectManager<Waypoint> {
        return new ChangeableObjectManager<Waypoint>(objectManager);
    }

    private getAddStationClickCallback(): (eventArgs: RepositoryEventArgs<Waypoint>) => void {
        if (this.addStationClickCallback == undefined) {
            var waypointManager: WaypointManager = this;
            this.addStationClickCallback =
                function(eventArgs: RepositoryEventArgs<Waypoint>) {
                    var station = eventArgs.getObject();
                    waypointManager.add(station);
                }
        }
        return this.addStationClickCallback;
    }

    private getRemoveStationClickCallback(): (eventArgs: RepositoryEventArgs<Waypoint>) => void {
        if (this.removeStationClickCallback == undefined) {
            var waypointManager: WaypointManager = this;
            this.removeStationClickCallback =
                function(eventArgs: RepositoryEventArgs<Waypoint>) {
                    var station = eventArgs.getObject();
                    waypointManager.removeWayopintById(station.id);
                }
        }
        return this.removeStationClickCallback;
    }

    protected indexOfWaypointById(id: string): number {
        for (var i = 0; i < this.getObjectCount(); i += 1) {
            if (this.getAllObjects()[i].id === id) {
                return i;
            }
        }
    }

    public add(waypoint: Waypoint): void {
        if (waypoint instanceof Station) {
            var station = <Station>waypoint;
            station.assignLine(this.line);
        }

        super.add(waypoint);

        if (this.getObjectCount() > 1) {
            this.handleFinance();
        }
    }
    
   private handleFinance(): void {
        var distance = this.line.getDistanceBetweenStationsByIndex(this.getIndexOfWaypoint(this.getStationList()[this.getStationList().length-2]), this.getIndexOfWaypoint(this.getStationList()[this.getStationList().length-1]));
        var price = Finance.getInstance().getFinanceData().calculateStationToLineAssignmentPrice(distance);
        var status = Finance.getInstance().handleTransaction(new Transaction(TransactionTypes.PURCHASE, price), false);

        if (!status) {
            this.removeWayopintById(this.last().id);
        }
    }

    public removeAt(index: number): void {
        var waypoint = this.getWaypointAtIndex(index);
        waypoint.hideMarker();

        if (waypoint instanceof Station) {
            var station = <Station>waypoint;
            station.deassignLine(this.line);
        }

        super.removeAt(index);
    }

    public removeWayopintById(id: string): void {
        var index: number = this.indexOfWaypointById(id);
        if (index != undefined) {
            var eventArgs: ObjectManagerEventArgs = new ObjectManagerEventArgs(index);
            this.removeAt(index);
        }
    }

    public getStationList(): Station[] {
        var stationList: Station[] = [];
        this.forEach((item) => {
            if (item instanceof Station) {
                stationList.push(item)
            }
        })
        return stationList;
    }
    
    public getStationById(id: number): Station {
        var stationList: Station[] = [];
        this.forEach((item) => {
            if (item instanceof Station) {
                if(item.getId() == id.toString())
                {
                    return Station;
                }
            }
        })
        return null;
    }

    public removeAllWaypointsWhichAreNotStations(): void {
        var waypointList: Waypoint[] = [];
        var removecounter = 0;
		
        // copy items to circumvent side effects from removal
        this.getAllObjects().forEach((item) => {
            waypointList.push(item);
        })

        waypointList.forEach((item) => {
            if (!(item instanceof Station)) {
                this.removeWayopintById(item.id);
                removecounter++;
            }
        })
        //console.log('removed ' + removecounter + " waypoints");
    }

    public addRouteWaypointsForStation(stationIndex: number, waypointList: Waypoint[]): void {
        var stationList: Station[] = this.getStationList();
        var waypointIndex: number = this.indexOfWaypointById(stationList[stationIndex].id) + 1;

        for (var key in waypointList) {
            var waypoint = waypointList[key];
            this.insert(waypointIndex, waypoint);
            waypointIndex++;
        }
        //console.log('added ' + waypointList.length + " waypoints");
    }

    public getIndexOfWaypoint(waypoint: Waypoint): number {
        return this.getAllObjects().indexOf(waypoint);
    }

    public getWaypointsBetween(waypoint1: Waypoint, waypoint2: Waypoint): Waypoint[] {
        var startIndex: number = this.getIndexOfWaypoint(waypoint1);
        var endIndex: number = this.getIndexOfWaypoint(waypoint2);
        var waypoints: Waypoint[] = [];

        if (startIndex > endIndex) {
            var tmp = startIndex;
            startIndex = endIndex;
            endIndex = tmp;
        }

        for (var i = startIndex + 1; i < endIndex; i++) {
            waypoints.push(this.getWaypointAtIndex(i));
        }

        return waypoints;
    }

    private areWaypointsEqual(waypoint1: Waypoint, waypoint2: Waypoint): boolean {
        return google.maps.geometry.spherical.computeDistanceBetween(waypoint1.getLocation(), waypoint2.getLocation()) <= WaypointManager.WAYPOINT_EQUAL_DISTANCE;
    }

    public getWaypointAtIndex(index: number): Waypoint {
        return this.getAllObjects()[index];
    }

    public enterStationAssignmentMode(): void {
        var stations = WaypointRepository.getInstance().getStationList();
        for (var key in stations) {
            stations[key].addLeftClickListener(this.getAddStationClickCallback());
            stations[key].addRightClickListener(this.getRemoveStationClickCallback());
        }
    }

    public leaveStationAssignmentMode(): void {
        var stations = WaypointRepository.getInstance().getStationList();
        for (var key in stations) {
            stations[key].removeLeftClickListener(this.getAddStationClickCallback());
            stations[key].removeRightClickListener(this.getRemoveStationClickCallback());
        }
    }

}