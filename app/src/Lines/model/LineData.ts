class LineData {
	// members must be public in order to bind them via angular
	public name: string;
	public color: string; // the color that is used to draw the line
	public icon: string; // the icon to show as material-design-icon
	
	constructor() {
		
	}
	
	public getName(): string {
		return this.name;
	}

	public setName(name: string): void {
		this.name = name;
	}

	public getColor(): string {
		return this.color;
	}

	public setColor(color: string): void {
		this.color = color;
	}
	
	public getIcon(): string {
		return this.icon
	}
	
	public setIcon(icon: string) {
		this.icon = icon;
	}
}