class TimetableManager extends ObjectManagerExtensionBase<Timetable> {

	public static NAME(): string { return "TimetableManager"; }

	constructor() {
		super(new EntityObjectManager(new ObjectManager<Timetable>(), TimetableRepository.getInstance()));
		this.registerExtension(TimetableManager.NAME(), this);
	}

}