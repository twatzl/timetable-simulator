abstract class Line extends EntityObject implements DrawableMapObject {
    protected waypointMarkersEnabled: boolean;

    public data: LineData;
    protected polyline: google.maps.Polyline;
    private waypointManager: WaypointManager;
    private vehicleManager: VehicleManager;
    private timetableManager: TimetableManager;

    private baseVelocity: number = 10;

    constructor(jsonData?: any) {
        super(LineRepository.getInstance().getNextId(), jsonData);
        this.data = new LineData();
        this.polyline = new google.maps.Polyline;

        this.waypointManager = new WaypointManager(this);
        this.vehicleManager = new VehicleManager();
        this.timetableManager = new TimetableManager();

        this.initializeListeners();
    }

    private initializeListeners(): void {
        this.waypointManager.addOnObjectAddedListener(this.getOnChangedListener());
        this.waypointManager.addOnObjectRemovedListener(this.getOnChangedListener());
        var observableWaypointManager: ChangeableObjectManager<Waypoint> = <ChangeableObjectManager<Waypoint>>this.waypointManager.getExtensionByName(ChangeableObjectManager.NAME());
        observableWaypointManager.addOnObjectChangedListener(this.getOnChangedListener());
    }

    protected getOnChangedListener() {
        var line = this;
        return function(eventArgs: ObjectManagerEventArgs): void {
            line.update();
        }
    }

    protected getDrawableVehicleInstanceObjectManager(): DrawableMapObjectManager<VehicleInstance> {
        // todo: replace with method call.
        return <DrawableMapObjectManager<VehicleInstance>>this.getVehicleManager().getExtensionByName(DrawableMapObjectManager.NAME());
    }

    public getWaypointManager(): WaypointManager {
        return this.waypointManager;
    }

    public getVehicleManager(): VehicleManager {
        return this.vehicleManager;
    }

    public getTimetableManager(): TimetableManager {
        return this.timetableManager;
    }

    public addWaypoint(waypoint: Waypoint): void {
        this.waypointManager.add(waypoint);
    }

    public removeWayopintById(id: string): void {
        var waypoint: Waypoint = WaypointRepository.getInstance().getObjectById(id);
        if (this.waypointMarkersEnabled && waypoint instanceof Station) {
            var station: Station = <Station>waypoint;
            station.hideMarker();
        }
        this.waypointManager.removeWayopintById(id);
    }

    public getStationList(): Station[] {
        return this.waypointManager.getStationList();
    }

    public getWaypointAtIndex(index: number): Waypoint {
        return this.waypointManager.getWaypointAtIndex(index);
    }

    protected initPolyline(map: google.maps.Map) {
        var polylineOptions = this.getPolylineOptions();
        this.polyline.setOptions(polylineOptions);
        this.polyline.setMap(map);
    }

    protected getWaypointLatLngList(): google.maps.LatLng[] {
        var points: google.maps.LatLng[] = [];

        for (var i = 0; i < this.getWaypointManager().getObjectCount(); i++) {
            points.push(this.getWaypointManager().getWaypointAtIndex(i).getLocation());
        }
		
        //console.log("line " + this.data.getName() + " has " + points.length + " waypoints to draw");

        return points;
    }

    public showWaypointMarkers(map: google.maps.Map) {
        this.waypointMarkersEnabled = true;
        this.show(map);
    }

    public hideWaypointMarkers(map: google.maps.Map) {
        this.waypointMarkersEnabled = false;
        this.hideMarkers();
    }

    protected showMarkers(map: google.maps.Map) {
        this.getWaypointManager().getStationList().forEach((value: Station, index: number, array: Station[]) => {
            var letters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@";
            value.showMarker(letters[index], map);
        });
    }

    protected hideMarkers() {
        this.getStationList().forEach((value: Station, index: number, array: Station[]) => {
            value.hideMarker();
        });
    }

    protected getPolylineOptions(): google.maps.PolylineOptions {
        return {
            geodesic: true,
            strokeColor: this.data.getColor(),
            strokeOpacity: 2.0
        };
    }

    protected updatePolylinePath() {
        //console.log("line " + this.data.getName() + " update polyline");
        this.polyline.setPath(this.getWaypointLatLngList());
    }

    public deleted(): void {
        var vehicles: VehicleInstance[] = this.vehicleManager.getAllObjects();
        vehicles.forEach((vehicle: VehicleInstance, index: number, array: VehicleInstance[]) => {
            VehicleInstanceRepository.getInstance().deleteObjectById(vehicle.id);
        });
        var timetables: Timetable[] = this.timetableManager.getAllObjects();
        timetables.forEach(function(timetable: Timetable, index: number, array: Timetable[]) {
            timetable.getCourses().forEach(function(kurs: Kurs, index: number, array: Kurs[]) {
                CourseRepository.getInstance().deleteObjectById(kurs.id);
            });
        });

        this.hide();
    }

    public update(): void {
        //console.log("line " + this.data.getName() + " update");
        this.updatePolylinePath();
    }

    public show(map: google.maps.Map): void {
        //console.log("line " + this.data.getName() + " show");

        this.initPolyline(map);
        if (this.getWaypointManager().getObjectCount() == 0) {
            return;
        }

        this.updatePolylinePath();
        this.getDrawableVehicleInstanceObjectManager().showAll(map);
        if (this.waypointMarkersEnabled) {
            this.showMarkers(map);
        }
    }

    public hide(): void {
        this.polyline.setMap(null);
        this.getDrawableVehicleInstanceObjectManager().hideAll();
        this.hideMarkers();
    }

    public getDistanceBetweenStations(startStation: Station, endStation: Station): number {
        var startIndex: number = this.getWaypointManager().getStationList().indexOf(startStation);
        var endIndex: number = this.getWaypointManager().getStationList().indexOf(endStation);
        return this.getDistanceBetweenStationsByIndex(startIndex, endIndex);
    }

    public getDistanceBetweenStationsByIndex(startIndex: number, endIndex: number): number {
        if (endIndex < startIndex) {
            var tmp = endIndex;
            endIndex = startIndex;
            startIndex = tmp;
        }

        var sum = 0;
        for (var i = startIndex; i < endIndex; i++) {
            sum += google.maps.geometry.spherical.computeDistanceBetween(this.getWaypointAtIndex(i).getLocation(), this.getWaypointAtIndex(i + 1).getLocation());
        }

        return sum;
    }

    public getTimeBetweenStations(startStation: Station, endStation: Station): number {
        var startIndex: number = this.getWaypointManager().getAllObjects().indexOf(startStation);
        var endIndex: number = this.getWaypointManager().getAllObjects().indexOf(endStation);
        return this.getTimeBetweenStationsByIndex(startIndex, endIndex);
    }

    public getTimeBetweenStationsByIndex(startIndex: number, endIndex: number): number {
        return Math.floor(this.getDistanceBetweenStationsByIndex(startIndex, endIndex) / this.baseVelocity) *3;
    }

    public getTimeBetweenStationsList(): Array<number> {
        var tempList: Array<number> = [];
        var list = this.getStationList();
        for (var index = 1; index < list.length; index++) {
            if (index == 0) {
                tempList[index-1] = 0;
            }
            else {
               tempList[index-1] = this.getTimeBetweenStations(this.getStationList()[index - 1], this.getStationList()[index]);
            }

        }

        return tempList;

    }
}