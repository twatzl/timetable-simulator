class UnboundLine extends Line {

    constructor(jsonData?: any) {
        super(jsonData);
        this.data.setIcon("directions_subway");
        if (jsonData != undefined) {
            this.parseFromJson(jsonData);
        }
    }

    public parseFromJson(jsonData: any): void {
        this.data.setColor(jsonData.data.color);
        this.data.setName(jsonData.data.name);
        this.data.setIcon(jsonData.data.icon);
        var instance = this;
        if (Object.prototype.toString.call(jsonData.timetableManager) === '[object Array]') {
            jsonData.timetableManager.forEach(function(element: any, index: any) {
                instance.getTimetableManager().add(TimetableRepository.getInstance().getObjectById(element));
            });
        }
        if (Object.prototype.toString.call(jsonData.waypointManager) === '[object Array]') {
            jsonData.waypointManager.forEach(function(element: any, index: any) {
                if (WaypointRepository.getInstance().getObjectById(element) != undefined) {
                    instance.getWaypointManager().add(WaypointRepository.getInstance().getObjectById(element));
                }
            });
        }
        if (Object.prototype.toString.call(jsonData.vehicleManager) === '[object Array]') {
            jsonData.vehicleManager.forEach(function(element: any, index: any) {
                if (VehicleInstanceRepository.getInstance().getObjectById(element) != undefined) {
                    instance.getVehicleManager().add(VehicleInstanceRepository.getInstance().getObjectById(element));
                }
            });
        }
    }

    public update(): void {
        this.updatePolylinePath();
    }

    public getJSONString(): string {
        var waypointData: string[] = [];

        this.getWaypointManager().getAllObjects().forEach(element => {
            waypointData.push(element.id);
        });

        var vehicleData: string[] = [];

        this.getVehicleManager().getAllObjects().forEach(element => {
            vehicleData.push(element.id);
        });

        var timetableData: string[] = [];

        this.getTimetableManager().getAllObjects().forEach(element => {
            timetableData.push(element.id);
        });

        return JSON.stringify(this, function(key, value) {
            switch (key) {
                case "timetableManager":
                    return timetableData;
                case "waypointManager":
                    return waypointData;
                case "vehicleManager":
                    return vehicleData;
                case "polyline":
                    return "UnboundLine";
                default:
                    return value;
            }
        });
    }
}