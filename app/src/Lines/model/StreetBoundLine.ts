class StreetBoundLine extends Line {
    private directionsRenderer: google.maps.DirectionsRenderer;
    private directionsService: google.maps.DirectionsService;

    private onGoogleMapsReady: ObservableEvent<void>;

    private updatesSuspended: boolean;
    private onDirectionUpdated: ObservableEvent<RepositoryEventArgs<StreetBoundLine>>;

    private areUpdatesSuspended(): boolean {
        return this.updatesSuspended;
    }

    private suspendUpdates() {
        this.updatesSuspended = true;
    }

    private resumeUpdates() {
        this.updatesSuspended = false;
    }

    private hasWaypoints(): boolean {
        return this.getWaypointManager().getObjectCount() != 0;
    }

    constructor(jsonData?: any) {
        super(jsonData);
        this.data.setIcon("directions_bus");
        this.directionsService = new google.maps.DirectionsService();
        this.directionsRenderer = new google.maps.DirectionsRenderer();
        this.onDirectionUpdated = new ObservableEvent<RepositoryEventArgs<StreetBoundLine>>();
        this.onGoogleMapsReady = new ObservableEvent<void>();
        this.updateDirections();
        if (jsonData != undefined) {
            this.parseFromJson(jsonData);
        }
    }

    public parseFromJson(jsonData: any): void {
        this.data.setColor(jsonData.data.color);
        this.data.setName(jsonData.data.name);
        this.data.setIcon(jsonData.data.icon);

        var instance = this;
        if (Object.prototype.toString.call(jsonData.timetableManager) === '[object Array]') {
            jsonData.timetableManager.forEach(function(element: any, index: any) {
                instance.getTimetableManager().add(TimetableRepository.getInstance().getObjectById(element));
            });
        }
        if (Object.prototype.toString.call(jsonData.waypointManager) === '[object Array]') {
            jsonData.waypointManager.forEach(function(element: any, index: any) {
                if (WaypointRepository.getInstance().getObjectById(element) != undefined) {
                    instance.getWaypointManager().add(WaypointRepository.getInstance().getObjectById(element));
                }
            });
        }
        if (Object.prototype.toString.call(jsonData.vehicleManager) === '[object Array]') {
            jsonData.vehicleManager.forEach(function(element: any, index: any) {
                if (VehicleInstanceRepository.getInstance().getObjectById(element) != undefined) {
                    instance.getVehicleManager().add(VehicleInstanceRepository.getInstance().getObjectById(element));
                }
            });
        }
    }

    private generateWaypointList(direction: google.maps.DirectionsResult): Waypoint[] {
        return GeoUtilities.getWaypointsFromDirectionsResult(direction);
    }

    private getDirectionResultCallbackForStation(stationIndex: number): (result: google.maps.DirectionsResult, status: google.maps.DirectionsStatus) => void {
        var streetBoundLine: StreetBoundLine = this;
        return function(result: google.maps.DirectionsResult, status: google.maps.DirectionsStatus) {
            if (status == google.maps.DirectionsStatus.OK) {
                console.log("callback for station [" + stationIndex + "]");
                //rendering
				
                var waypoints: Waypoint[] = streetBoundLine.generateWaypointList(result);
                streetBoundLine.suspendUpdates();
                streetBoundLine.getWaypointManager().addRouteWaypointsForStation(stationIndex, waypoints);
                streetBoundLine.resumeUpdates();
                streetBoundLine.updatePolylinePath();

                streetBoundLine.onGoogleMapsReady.notifyListeners(null);
            }
        }
    }

    private drawDirection(map: google.maps.Map): void {
        this.directionsRenderer.setOptions(this.getDirectionsRendererOptions(map));
        if (this.getWaypointManager().getObjectCount() == 0) {
            this.directionsRenderer.setMap(null);
        } else {
            this.directionsRenderer.setMap(map);
        }
    }

    private updateDirections(): void {
        //console.log("updating directions:")
        if (!this.hasWaypoints()) {
            return;
        }

        var stationList = this.getStationList();

        for (var i = 0; i < stationList.length - 1; i++) {
            //console.log("[" + i + "]")
            var directionsRequest: google.maps.DirectionsRequest = this.getDirectionRequestBetweenWaypoints(stationList[i], stationList[i + 1]);
            this.directionsService.route(directionsRequest, this.getDirectionResultCallbackForStation(i));
        }
    }

    private getDirectionRequestBetweenWaypoints(start: Waypoint, end: Waypoint): google.maps.DirectionsRequest {
        return {
            origin: start.getLocation(),
            destination: end.getLocation(),
            travelMode: google.maps.TravelMode.DRIVING,
            //waypoints: this.getDirectionWaypoints(),
            optimizeWaypoints: false, 	// important to deactivate, otherwise players choice of
            // line planning would be overwritten by google algorithm
            avoidHighways: false,
            avoidTolls: false
        };
    }

    private getDirectionsRendererOptions(map: google.maps.Map): google.maps.DirectionsRendererOptions {
        return {
            map: map,
            draggable: false,
            polylineOptions: this.getPolylineOptions(),
            preserveViewport: true,
            suppressMarkers: true
        };
    }

    public update(): void {
        //console.log("streetBoundLine " + this.data.getName() + " update");

        if (!this.areUpdatesSuspended()) {
            this.suspendUpdates();
            this.getWaypointManager().removeAllWaypointsWhichAreNotStations();
            this.resumeUpdates();
            this.updateDirections();
            this.updatePolylinePath();
        }
    }

    public getJSONString(): string {

        var waypointData: string[] = [];

        this.getWaypointManager().getAllObjects().forEach(element => {
            waypointData.push(element.id);
        });

        var vehicleData: string[] = [];

        this.getVehicleManager().getAllObjects().forEach(element => {
            vehicleData.push(element.id);
        });

        var timetableData: string[] = [];

        this.getTimetableManager().getAllObjects().forEach(element => {
            timetableData.push(element.id);
        });

        return JSON.stringify(this, function(key, value) {
            switch (key) {
                case "timetableManager":
                    return timetableData;
                case "waypointManager":
                    return waypointData;
                case "vehicleManager":
                    return vehicleData;
                case "directionsRenderer":
                    return;
                case "directionsService":
                    return;
                case "polyline":
                    return;
                default:
                    return value;
            }
        });
    }

    public buildLine(jsonObject: JSON): void {

    }
    
    public addOnGoogleMapsReadyListener(listener: () => void): void {
        this.onGoogleMapsReady.addListener(listener);
    }

    public removeOnGoogleMapsReadyListener(listener: () => void): void {
        this.onGoogleMapsReady.removeListener(listener);
    }
}