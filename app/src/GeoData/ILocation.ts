interface ILocation {
	
	getLat(): number;
	
	getLon(): number;
	
	setLat(value: number): void;
	
	setLon(value: number): void;
	
	getDistance(other: ILocation): number;
	
}