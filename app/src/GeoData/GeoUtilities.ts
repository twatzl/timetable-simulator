class GeoUtilities {
	
	public static getWaypointsFromDirectionsResult(direction: google.maps.DirectionsResult): Waypoint[] {
		var waypointList: Waypoint[] = [];
		direction.routes[0].overview_path.forEach((item) => {
			waypointList.push(new Waypoint(item));
		})
		return waypointList;
	}
	
	public static calculateDistanceOfRoute(route: Waypoint[]) {
		var distance = 0;
		
		for (var i = 0; i < route.length - 1; i++) {
			distance += google.maps.geometry.spherical.computeDistanceBetween(
				route[i].getLocation(), route[i+1].getLocation()); 
		}
		
		return distance;
	}
	
}