class GoogleLocation implements ILocation {

	location: google.maps.LatLng;

	constructor(location: google.maps.LatLng) {
		this.location = location;
	}
	
	getLatLng(): google.maps.LatLng { 
		return this.location;
	}

	getLat(): number {
		return this.location.lat();
	}

	getLon(): number {
		return this.location.lng();
	}

	setLat(value: number): void {
		var lat = value;
		var lon = this.location.lng();

		this.location = new google.maps.LatLng(lat, lon);
	}

	setLon(value: number): void {
		var lat = this.location.lat();
		var lon = value;

		this.location = new google.maps.LatLng(lat, lon);
	}

	getDistance(): number {
		return 0;
	}
	
	static fromLocation(other: ILocation): GoogleLocation {
		var location = new google.maps.LatLng(other.getLat(), other.getLon());
		return new GoogleLocation(location);
	}
	
	static fromLatLon(lat: number, lon: number) {
		var location = new google.maps.LatLng(lat, lon);
		return new GoogleLocation(location);
	}

}