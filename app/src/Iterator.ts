
enum IterationDirection {
	Forward,
	Backward
}

abstract class Iterator {
	private nextIndex: number;
	private iterationDirection: IterationDirection;
	
	public constructor() {
		this.nextIndex = 1;
		this.iterationDirection = IterationDirection.Forward;		
	}

	protected getNextIndex(): number {
		return this.nextIndex;
	}

	protected incrementNextIndex(): void {
		this.nextIndex++;
	}

	protected decrementNextIndex(): void {
		this.nextIndex--;
	}

	protected getIterationDirection(): IterationDirection {
		return this.iterationDirection
	}

	protected setIterationDirection(iterationDirection: IterationDirection): void {
		this.iterationDirection = iterationDirection;
	}

	protected getCurrentIndex(): number {
		if (this.iterationDirection == IterationDirection.Forward) {
			return this.getNextIndex() - 1;
		} else {
			return this.getNextIndex() + 1;
		}
	}

	protected moveForward(): void {
		if (this.getNextIndex() >= this.getMaxIndex()) {
			this.iterationDirection = IterationDirection.Backward;
			this.nextIndex--;
		} else {
			this.nextIndex++;
		}
	}

	protected moveBackward(): void {
		if (this.getNextIndex() <= this.getMinIndex()) {
			this.iterationDirection = IterationDirection.Forward;
			this.nextIndex++;
		} else {
			this.nextIndex--;
		}
	}

	protected moveToNextIndex(): void {
		if (this.iterationDirection == IterationDirection.Forward) {
			this.moveForward();
		} else {
			this.moveBackward();
		}
	}
	
	protected abstract getMaxIndex(): number;
	
	protected abstract getMinIndex(): number;
	
}