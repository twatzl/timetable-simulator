/// <reference path="google.maps.d.ts"/>

interface DrawableMapObject {

	show(map: google.maps.Map): void;

	hide(): void;

}
