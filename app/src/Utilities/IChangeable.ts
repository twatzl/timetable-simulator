interface IChangeable<T> {
	
	addOnChangedListener(listener: (eventArgs: RepositoryEventArgs<T>) => void): void;
	
	removeOnChangedListener(listener: (eventArgs: RepositoryEventArgs<T>) => void): void;
	
}