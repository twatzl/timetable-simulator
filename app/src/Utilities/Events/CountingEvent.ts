/**
 * An observable event which will notify the listeners 
 * after the event has been fired n times.
 */
class CountingEvent extends ObservableEvent<void> {

	private eventCount: number;
	private currentCount: number;

	constructor(eventCount: number) {
		super();
		this.eventCount = eventCount;
	}

	private resetCurrentCount() {
		this.currentCount = this.eventCount;
	}

	public notifyListeners(eventArgs: void) {
		this.currentCount--;
		if (this.currentCount == 0) {
			super.notifyListeners(eventArgs);
			this.resetCurrentCount();
		}
	}
	
}