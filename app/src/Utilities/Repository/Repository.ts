class Repository<T extends EntityObject> {
	protected entities: Dictionary<T>;
	protected idCounter: number;

	protected onObjectAdded: ObservableEvent<RepositoryEventArgs<T>>;
	protected onObjectRemoved: ObservableEvent<RepositoryEventArgs<T>>;

	constructor() {
		this.idCounter = 0;
		this.entities = {};

		this.onObjectAdded = new ObservableEvent<RepositoryEventArgs<T>>();
		this.onObjectRemoved = new ObservableEvent<RepositoryEventArgs<T>>();
	}

	public getNextId(): number {
		this.idCounter++;
		return this.idCounter;
	}

	public addObject(object: T): void {
		this.entities[object.id] = object;
		this.onObjectAdded.notifyListeners(new RepositoryEventArgs<T>(object.id, object));
	}

	public getObjectById(id: string): T {
		return this.entities[id];
	}

	public deleteObjectById(id: string): void {
		this.onObjectRemoved.notifyListeners(new RepositoryEventArgs<T>(id, this.entities[id]));
		this.entities[id].deleted();
		delete this.entities[id];
	}

	public getObjectList(): Dictionary<T> {
		return this.entities;
	}

	public addOnObjectAddedListener(listener: (eventArgs: RepositoryEventArgs<T>) => void): void {
		this.onObjectAdded.addListener(listener);
	}

	public removeOnObjectAddedListener(listener: (eventArgs: RepositoryEventArgs<T>) => void): void {
		this.onObjectAdded.removeListener(listener);
	}

	public addOnObjectRemovedListener(listener: (eventArgs: RepositoryEventArgs<T>) => void): void {
		this.onObjectRemoved.addListener(listener);
	}

	public removeOnObjectRemovedListener(listener: (eventArgs: RepositoryEventArgs<T>) => void): void {
		this.onObjectRemoved.removeListener(listener);
	}

	public getJSONStringOfRepository(JSONObjectName: string): string {
		var jsonStringBegin = '"' + JSONObjectName + '\":[';
		var jsonStringEnd = '],';
        var mergedZoneJSONStrings = '';

		for (var key in this.entities) {
            if (this.entities[key] != undefined) {
                mergedZoneJSONStrings += this.entities[key].getJSONString() + ',';
            }
        }

        mergedZoneJSONStrings = mergedZoneJSONStrings.slice(0, -1);// Removes the last ','
        return jsonStringBegin + mergedZoneJSONStrings + jsonStringEnd;
	}
}
