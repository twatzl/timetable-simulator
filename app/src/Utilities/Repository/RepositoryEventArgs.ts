class RepositoryEventArgs<T> {
	private index: string;
	private object: T;

	constructor(index: string, object: T) {
		this.index = index;
		this.object = object;
	}

	@deprecated("Use getId() instead.")
	public getIndex(): string {
		return this.index;
	}
	
	public getId(): string {
		return this.index;
	}

	public getObject(): T {
		return this.object;
	}
}