abstract class EntityObject {
	id: string;
	
	constructor(id: any, jsonObject?: any) {
		// 2015-11-25 I do not exactly know if this if is necessary
		// maybe typescript has a bug that causes id to get a number
		// when compiled to javascript.
		if (jsonObject != undefined) {
			this.id = jsonObject.id;
		} else {
			this.id = String(id);
		}
	}
	
	public getId(): string { 
		return this.id;
	}
	
	public abstract deleted(): void;

	abstract getJSONString(): string;
}