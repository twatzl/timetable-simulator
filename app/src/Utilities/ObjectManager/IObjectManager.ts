class ObjectManagerEventArgs {
	private index: number;

	constructor(index: number) {
		this.index = index;
	}

	public getIndex(): number {
		return this.index;
	}
}

interface IObjectManager<T> {
	add(object: T): void;
	
	insert(index: number, object: T): void;

	remove(object: T): void;

	removeAt(index: number): void;

	first(): T;

	last(): T;

	getObjectCount(): number;

	getAllObjects(): T[];

	forEach(callback: (value: T, index: number, array: T[]) => void): void;

	addOnObjectAddedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void;

	removeOnObjectAddedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void;

	addOnObjectRemovedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void;

	removeOnObjectRemovedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void;

	registerExtension(name: string, extension: IObjectManager<T>): void;

	removeExtension(name: string): void;

	getExtensionByName(name: string): IObjectManager<T>;
	
	hasExtension(name: string): boolean;
}