class ChangeableObjectManager<T extends IChangeable<any>> extends ObjectManagerExtensionBase<T> {
	
	// instead of const member, which is currently not supported
	public static NAME(): string { return "ChangeableObjectManager"; }

	protected onObjectChanged: ObservableEvent<ObjectManagerEventArgs>;

	constructor(objectManager: IObjectManager<T>) {
		super(objectManager);
		objectManager.registerExtension(ChangeableObjectManager.NAME(), this);
		this.onObjectChanged = new ObservableEvent<ObjectManagerEventArgs>();
	}
	
	private getOnChangedListener(index: number) : () => void {
		var observableObjectManager = this;
		return function() {
			observableObjectManager.notifyListeners(index);
		};
	}
	
	public add(object: T): void {
		super.add(object);
		var index = this.getAllObjects().indexOf(object);
		object.addOnChangedListener(this.getOnChangedListener(index));
	}

	removeAt(index: number): void {
		var object = this.getAllObjects()[index];
		object.removeOnChangedListener(this.getOnChangedListener(index));
		super.removeAt(index);
	}
	
	private notifyListeners(index: number) {
		this.onObjectChanged.notifyListeners(new ObjectManagerEventArgs(index));
	}

	public addOnObjectChangedListener(listener: (eventArgs: ObjectManagerEventArgs) => void) {
		this.onObjectChanged.addListener(listener);
	}

	public removeOnObjectChangedListener(listener: (eventArgs: ObjectManagerEventArgs) => void) {
		this.onObjectChanged.removeListener(listener);
	}

}