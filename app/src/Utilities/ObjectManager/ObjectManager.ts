class ObjectManager<T> implements IObjectManager<T> {
	private registeredExtensions: Dictionary<IObjectManager<T>>;
	protected objects: T[];
	protected onObjectAdded: ObservableEvent<ObjectManagerEventArgs>;
	protected onObjectRemoved: ObservableEvent<ObjectManagerEventArgs>;

	constructor() {
		this.registeredExtensions = {};
		this.objects = [];
		this.onObjectAdded = new ObservableEvent<ObjectManagerEventArgs>();
		this.onObjectRemoved = new ObservableEvent<ObjectManagerEventArgs>();
	}

	public add(object: T): void {
		this.objects.push(object);
		this.onObjectAdded.notifyListeners(new ObjectManagerEventArgs(this.objects.length - 1));
	}

	public insert(index: number, object: T): void {
		this.objects.splice(index, 0, object);
		this.onObjectAdded.notifyListeners(new ObjectManagerEventArgs(index));
	}

	public remove(object: T): void {
		var index = this.objects.indexOf(object);
		this.removeAt(index);
	}

	public removeAt(index: number): void {
        if (index > -1) {
            this.objects.splice(index, 1);
			this.onObjectRemoved.notifyListeners(new ObjectManagerEventArgs(index));
        }
	}

	public first(): T {
		return this.objects[0];
	}

	public last(): T {
		return this.objects[this.objects.length - 1];
	}

	public getObjectCount(): number {
		return this.objects.length;
	}

	public getAllObjects(): T[] {
		return this.objects;
	}

	public forEach(callback: (value: T, index: number, array: T[]) => void): void {
		this.objects.forEach((value: T, index: number, array: T[]) => {
			callback(value, index, array);
		})
	}

	public addOnObjectAddedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void {
		this.onObjectAdded.addListener(listener);
	}

	public removeOnObjectAddedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void {
		this.onObjectAdded.removeListener(listener);
	}

	public addOnObjectRemovedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void {
		this.onObjectRemoved.addListener(listener);
	}

	public removeOnObjectRemovedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void {
		this.onObjectRemoved.removeListener(listener);
	}

	public registerExtension(name: string, extension: IObjectManager<T>): void {
		if (this.registeredExtensions[name] != undefined) {
			throw new Error("Extension with the name \"" + name + "\" is already registered! Do not register twice!");
		}
		this.registeredExtensions[name] = extension;
	}

	public removeExtension(name: string): void {
		this.registeredExtensions[name] = undefined;
	}

	public getExtensionByName(name: string): IObjectManager<T> {
		if (this.registeredExtensions[name] === undefined) {
			throw new Error("No extension with the name \"" + name + "\" is registered");
		}
		return this.registeredExtensions[name];
	}

	public hasExtension(name: string): boolean {
		return this.registeredExtensions[name] != undefined;
	}

}