class DrawableMapObjectManager<T extends DrawableMapObject> extends ObjectManagerExtensionBase<T> {

	// instead of const member, which is currently not supported
	public static NAME(): string { return "DrawableMapObjectManager"; }

	constructor(objectManager: IObjectManager<T>) {
		super(objectManager);
		objectManager.registerExtension(DrawableMapObjectManager.NAME(), this);
	}

	public showAll(map: google.maps.Map): void {
		this.getObjectManager().forEach((object: T, index: number, array: T[]) => {
			object.show(map);
		});
	}

	public hideAll(): void {
		this.getObjectManager().forEach((object: T, index: number, array: T[]) => {
			object.hide();
		});
	}

}