abstract class ObjectManagerExtensionBase<T> implements IObjectManager<T> {

	private objectManager: IObjectManager<T>;

	constructor(objectManager: IObjectManager<T>) {
		this.objectManager = objectManager;
	}

	protected getObjectManager(): IObjectManager<T> {
		return this.objectManager;
	}

	public add(object: T): void {
		this.objectManager.add(object);
	}

	public insert(index: number, object: T) {
		this.objectManager.insert(index, object);
	}

	public remove(object: T): void {
		this.objectManager.remove(object);
	}

	public removeAt(index: number): void {
		this.objectManager.removeAt(index);
	}

	public first(): T {
		return this.objectManager.first();
	}

	public last(): T {
		return this.objectManager.last();
	}

	public getObjectCount(): number {
		return this.objectManager.getObjectCount();
	}

	public getAllObjects(): T[] {
		return this.objectManager.getAllObjects();
	}

	public forEach(callback: (value: T, index: number, array: T[]) => void): void {
		this.objectManager.forEach(callback);
	}

	public addOnObjectAddedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void {
		this.objectManager.addOnObjectAddedListener(listener);
	}

	public removeOnObjectAddedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void {
		this.objectManager.removeOnObjectAddedListener(listener);
	}

	public addOnObjectRemovedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void {
		this.objectManager.addOnObjectRemovedListener(listener);
	}

	public removeOnObjectRemovedListener(listener: (eventArgs: ObjectManagerEventArgs) => void): void {
		this.objectManager.removeOnObjectRemovedListener(listener);
	}

	public registerExtension(name: string, extension: IObjectManager<T>): void {
		this.objectManager.registerExtension(name, extension);
	}

	public removeExtension(name: string): void {
		this.objectManager.removeExtension(name);
	}

	public getExtensionByName(name: string): IObjectManager<T> {
		return this.objectManager.getExtensionByName(name);
	}

	public hasExtension(name: string): boolean {
		return this.objectManager.hasExtension(name);
	}

}