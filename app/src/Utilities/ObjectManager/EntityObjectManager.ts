class EntityObjectManager<T extends EntityObject> extends ObjectManagerExtensionBase<T> {

	// instead of const member, which is currently not supported
	public static NAME(): string { return "EntityObjectManager"; }

	constructor(objectManager: IObjectManager<T>, repository: Repository<T>) {
		super(objectManager);
		objectManager.registerExtension(EntityObjectManager.NAME(), this);
		repository.addOnObjectRemovedListener(this.getOnObjectRemovedFromRepositoryListener());
	}

	protected getOnObjectRemovedFromRepositoryListener(): (eventArgs: RepositoryEventArgs<T>) => void {
		var entityObjectManager = this;
		return function(eventArgs: RepositoryEventArgs<T>): void {
			entityObjectManager.remove(eventArgs.getObject());
		}
	}

}