abstract class MovableObjectData {

	constructor(currentLocation: google.maps.LatLng, stopped: boolean) {
		this.setCurrentLocation(currentLocation);
		this.Stopped = stopped;
		this.CurrentSpeed = 0;
	}

	private CurrentLocation: google.maps.LatLng;
	public getCurrentLocation(): google.maps.LatLng {
		return this.CurrentLocation;
	}
	public setCurrentLocation(v: google.maps.LatLng) {
		this.CurrentLocation = v;
	}

	private CurrentSpeed: number;
	public getCurrentSpeed(): number {
		return this.CurrentSpeed;
	}
	public setCurrentSpeed(v: number) {
		this.CurrentSpeed = v;

		if (v < 0) {
			this.CurrentSpeed = 0;
		} else if (v > this.getMaxSpeed()) {
			this.CurrentSpeed = this.getMaxSpeed();
		} else {
			this.CurrentSpeed = v;
		}
	}

	private Stopped: boolean;
	public isStopped(): boolean {
		return this.Stopped;
	}
	public setStopped(v: boolean) {
		this.Stopped = v;
	}

	protected abstract getMaxSpeed(): number;

	public abstract getAcceleration(): number;

	public abstract getDeceleration(): number;

}