interface Observable  {

	observers: { (eventArgs: RepositoryEventArgs<any>): void; };
	
	registerObserver(observer: { (eventArgs: RepositoryEventArgs<any>): void; }): void

	removeObserver(observer: { (eventArgs: RepositoryEventArgs<any>): void; }): void

	notifyObservers(message: any): void
}