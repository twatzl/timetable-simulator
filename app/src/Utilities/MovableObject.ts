abstract class MovableObject extends EntityObject {

	private fastClockListener: (eventArgs: FastClockEventArgs) => void;
	protected isShownOnMap: boolean;
	protected onWaypointReachedEvent: ObservableEvent<Waypoint>;
	protected onLastWaypointReachedEvent: ObservableEvent<MovableObject>;

	protected getData(): MovableObjectData {
		return this.data;
	}
	
	protected setWaypointIterator(waypointIterator: WaypointIterator): void {
		this.waypointIterator = waypointIterator;
	}
	
	protected getWaypointIterator(): WaypointIterator {
		return this.waypointIterator;
	}

	public isShown(): boolean {
		return this.isShownOnMap;
	}

	public deleted(): void {
		FastClock.getInstance().removeOnNextSecondEventListener(this.fastClockListener);
	}
	
	constructor(id: number, private data: MovableObjectData, private waypointIterator: WaypointIterator) {
		super(id);
		this.fastClockListener = this.initializeFastClockListener();
		this.onWaypointReachedEvent = new ObservableEvent<Waypoint>();
		this.onLastWaypointReachedEvent = new ObservableEvent<MovableObject>();
		
		FastClock.getInstance().addOnNextSecondEventListener(this.fastClockListener);
		
		this.initWaypointIterator();		
		this.init();
	}
	
	private initWaypointIterator(): void {
		const currentWaypoint = this.getWaypointIterator().getCurrentWaypoint();
		if (currentWaypoint == null || currentWaypoint == undefined) {
			this.getWaypointIterator().next();
		}
	}
	
	protected abstract init(): void;

	protected abstract getWaypointReachedRadius(): number;

	private initializeFastClockListener(): (eventArgs: FastClockEventArgs) => void {
		var movableObject = this;
		return function(eventArgs: FastClockEventArgs): void {
			movableObject.simulateSecond();
		}
	}

	private static getStopDistance(velocity: number, acceleration: number): number {
		//v^2 = 2as 
		return (velocity * velocity) / (2 * acceleration);
	}

	public stopUntil(time: Time) {
		var vehicleInstance = this;
		FastClock.getInstance().notifyAtTime(time, function() {
			vehicleInstance.start();
		});
		this.stop();
		//console.log("Movable Object stopped until " + time.toString())
	}

	public stopFor(milliseconds: number) {
		var vehicleInstance = this;
		FastClock.getInstance().notifyAfter(new Date(milliseconds), function() {
			vehicleInstance.start();
		});
		this.stop();
	}

	public stop(): void {
		this.data.setStopped(true);
	}

	public start(): void {
		this.data.setStopped(false);
	}

	public simulateSecond(): void {
		if (this.data.isStopped() || !this.beforeSimulationStep()) {
			return;
		}
		
		var distanceToNextWaypoint = this.getDistanceToNextWaypoint();
		this.updateCurrentSpeed(distanceToNextWaypoint);
		this.updateCurrentLocation(distanceToNextWaypoint);

		this.afterSimulationStep();

		if (distanceToNextWaypoint < this.getWaypointReachedRadius()) {
			this.onWaypointReached();
			//console.log("waypoint reached");
			//console.log("next index is: " + this.getWaypointIterator().nextIndex);
		}
	}

	protected beforeSimulationStep(): boolean {
		return true;
	}

	protected afterSimulationStep(): void {
		return;
	}

	protected updateCurrentLocation(distanceToNextWaypoint: number): void {
		var nextWaypointLocation = this.getWaypointIterator().getCurrentWaypoint().getLocation();
		var newLocation = google.maps.geometry.spherical.interpolate(
			this.data.getCurrentLocation(),
			nextWaypointLocation,
			this.data.getCurrentSpeed() / distanceToNextWaypoint
		);
		this.data.setCurrentLocation(newLocation);
	}

	protected updateCurrentSpeed(distanceToNextWaypoint: number): void {
		if (distanceToNextWaypoint < MovableObject.getStopDistance(this.data.getCurrentSpeed(), this.data.getAcceleration()) * 1.5) {
			this.decelerate();
		} else {
			this.accelerate();
		}
	}

	protected accelerate(): void {
		var newSpeed = this.data.getCurrentSpeed() + this.data.getAcceleration();
		this.data.setCurrentSpeed(newSpeed);
	}

	protected decelerate(): void {
		var newSpeed = this.data.getCurrentSpeed() - this.data.getAcceleration();
		this.data.setCurrentSpeed(newSpeed);
	}

	protected getDistanceToNextWaypoint(): number {
		var nextWaypointLocation = this.getWaypointIterator().getCurrentWaypoint().getLocation();
		var currentLocation = this.data.getCurrentLocation();
		return google.maps.geometry.spherical.computeDistanceBetween(currentLocation, nextWaypointLocation);
	}

	protected onWaypointReached() {
		var currentWaypoint = this.getWaypointIterator().getCurrentWaypoint();
		if (currentWaypoint != null && currentWaypoint != undefined) {
			this.onWaypointReachedEvent.notifyListeners(currentWaypoint);
		}
		if (!this.getWaypointIterator().hasWaypoints()) {
			this.onLastWaypointReachedEvent.notifyListeners(this);
			return;
		}
		
		this.getWaypointIterator().next();
	}
	
	public addOnWaypointReachedListener(listener: (waypoint: Waypoint) => void) {
		this.onWaypointReachedEvent.addListener(listener);
	}
	
	public removeOnWaypointReachedListener(listener: (waypoint: Waypoint) => void) {
		this.onWaypointReachedEvent.removeListener(listener);
	}

}