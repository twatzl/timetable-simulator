interface IIterator<T> {
	
	next(): T;
	
	getCurrentItem(): T;
	
}