class MapService {
	private static map: google.maps.Map;
	constructor() {}

	public static init(element: HTMLElement, lat: number, lng: number, zoom: number, mapOptions: google.maps.MapOptions): void {
		mapOptions = mapOptions || {};
		// Default Optionen
		// https://developers.google.com/maps/documentation/javascript/reference#MapTypeStyleFeatureType
		mapOptions.center = new google.maps.LatLng(lat, lng);
		mapOptions.zoom = zoom;
		mapOptions.styles = [
			{
				featureType: "poi",
				elementType: "labels",
				stylers: [
					{ visibility: "off" }
				]
			},
			{
				featureType: "transit",
				elementType: "labels",
				stylers: [
					{ visibility: "off" }
				]
			}
		];
		
		// Erzeugt neue Google Karte mit entsprechenden Optionen, bindet sie an das DOM-Element
		this.map = new google.maps.Map(element, mapOptions);
	}

	public static getMap() {
		if (this.map === undefined) {
			//this.map = this.init();
			Error()
		}
		return this.map;
	}
}