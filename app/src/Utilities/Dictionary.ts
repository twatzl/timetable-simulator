interface Dictionary<V> {
	[key: string]: V;
}