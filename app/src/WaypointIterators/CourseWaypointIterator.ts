class CourseWaypointIterator implements WaypointIterator {

	private course: Kurs;
	private courseElements: CourseElement[];

	private currentCourseElement: CourseElement;
	private waypointIterator: SimpleWaypointIterator;

	private onCourseFinishedEvent: ObservableEvent<void>;
	
	//workflow
	// courseWaypointIterator iterates over courseelements
	// gets waypoints between stations from line.
	// iterates over waypoints as well
	
	constructor(course: Kurs) {
		this.course = course;
		this.courseElements = this.course.getCourseElements().reverse();
		this.onCourseFinishedEvent = new ObservableEvent<void>();
		
		this.moveToNextStation();
		//console.log(this.courseElements);
	}

	private moveToNextWaypoint(): void {
		if (!this.waypointIterator.hasWaypoints()) {
			this.moveToNextStation();
		}
		this.waypointIterator.next();
		//console.log("moved to next waypoint");
	}

	private moveToNextStation(): void {
		if (this.courseElements.length == 0) {
			this.onCourseFinishedEvent.notifyListeners(null);
			return;
		}

		this.currentCourseElement = this.courseElements.pop();
		
		var nextCourseElement = this.courseElements[this.courseElements.length - 1];
		if (nextCourseElement != undefined) {
			this.createNewWaypointIterator(this.currentCourseElement.station, nextCourseElement.station);
		}
		//console.log("moved to next station");
		//console.log(this.waypoints);
	}

	private createNewWaypointIterator(waypoint1: Waypoint, waypoint2: Waypoint): void {
		var waypoints = this.course.getTimetable().getLine().getWaypointManager().getWaypointsBetween(waypoint1, waypoint2);
		waypoints.unshift(waypoint1);
		this.waypointIterator = new SimpleWaypointIterator(waypoints);
	}
	
	public hasWaypoints(): boolean {
		return this.courseElements.length > 0 || this.waypointIterator.hasWaypoints();
	}

	public next(): Waypoint {
		this.moveToNextWaypoint();
		return this.getCurrentWaypoint();
	}

	public getCurrentItem(): Waypoint {
		return this.getCurrentWaypoint();
	}

	public getCurrentWaypoint(): Waypoint {
		return this.waypointIterator.getCurrentWaypoint();
	}

	public isCurrentWaypointAStation(): boolean {
		var waypoint = this.getCurrentWaypoint();
		return waypoint instanceof Station;
	}

	public getDepartureTimeForCurrentStation(): Time {
		if (!this.isCurrentWaypointAStation()) {
			return null;
		}
		return this.currentCourseElement.departureTime;
	}

	public addOnCourseFinishedListener(listener: () => void): void {
		this.onCourseFinishedEvent.addListener(listener);
	}

	public removeOnCourseFinishedListener(listener: () => void): void {
		this.onCourseFinishedEvent.removeListener(listener);
	}


}