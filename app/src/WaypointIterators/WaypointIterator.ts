interface WaypointIterator extends IIterator<Waypoint> {

	getCurrentWaypoint(): Waypoint;

	hasWaypoints(): boolean;

}