class LineWaypointIterator extends Iterator implements WaypointIterator {

	private line: Line;

	constructor(line: Line) {
		super();
		this.line = line;
		this.line.getWaypointManager().addOnObjectAddedListener(this.getOnWaypointAddedToLineHander());
		this.line.getWaypointManager().addOnObjectRemovedListener(this.getOnWaypointRemovedFromLineHander());
	}

	protected getOnWaypointAddedToLineHander(): (eventArgs: ObjectManagerEventArgs) => void {
		var lineWaypointIterator: LineWaypointIterator = this;
		return function(eventArgs: ObjectManagerEventArgs): void {
			// TODO: check this checks for errors
			if (lineWaypointIterator.getNextIndex() > eventArgs.getIndex()) {
				lineWaypointIterator.incrementNextIndex();
			}
		}
	}

	protected getOnWaypointRemovedFromLineHander(): (eventArgs: ObjectManagerEventArgs) => void {
		var lineWaypointIterator: LineWaypointIterator = this;
		return function(eventArgs: ObjectManagerEventArgs): void {
			// TODO: check this checks for errors
			if (lineWaypointIterator.getNextIndex() > eventArgs.getIndex()) {
				lineWaypointIterator.decrementNextIndex();
			}
		}
	}
	
	public hasWaypoints(): boolean {
		return true;
		// follows line infinitely => has always waypoints
	}
	
	public next(): Waypoint {
		this.moveToNextIndex();
		return this.getCurrentWaypoint();
	}
	
	public getCurrentItem(): Waypoint {
		return this.getCurrentWaypoint();
	}

	public getCurrentWaypoint(): Waypoint {
		var waypoint: Waypoint = this.line.getWaypointAtIndex(this.getCurrentIndex());
		return waypoint;
	}
	
	protected getMaxIndex(): number {
		return this.line.getWaypointManager().getObjectCount() - 1;
	}

	protected getMinIndex(): number {
		return 0;
	}

}