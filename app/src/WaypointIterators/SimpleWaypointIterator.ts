class SimpleWaypointIterator implements WaypointIterator {
	
	private currentWaypoint: Waypoint;
	private lastWaypointReachedEvent: ObservableEvent<void>;
		
	constructor(private waypoints: Waypoint[]) {
		this.lastWaypointReachedEvent = new ObservableEvent<void>();
	}
	
	private moveToNextWaypoint() {
		if (this.hasWaypoints()) {
			this.currentWaypoint = this.waypoints.pop();
		} else { 
			if (this.currentWaypoint != null) {
				this.lastWaypointReachedEvent.notifyListeners(null);
			}
			this.currentWaypoint = null;
		}
	}
	
	public getRemainingWaypoints(): number {
		return this.waypoints.length;
	}
	
	public hasWaypoints(): boolean {
		return this.getRemainingWaypoints() > 0;
	}
	
	public next(): Waypoint {
		this.moveToNextWaypoint();
		return this.getCurrentWaypoint()
	}
	
	public getCurrentItem(): Waypoint {
		return this.getCurrentWaypoint();
	}

	public getCurrentWaypoint(): Waypoint {
		return this.currentWaypoint;
	}
	
	public addOnLastWapointReachedListener(listener: () => void) {
		this.lastWaypointReachedEvent.addListener(listener);
	}
	
	public removeOnLastWaypointReachedListener(listener: () => void) {
		this.lastWaypointReachedEvent.addListener(listener);
	}
	
}