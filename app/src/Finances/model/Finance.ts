enum TransactionTypes {
    SALE, PURCHASE
}

enum StatusMessageTypes {
    SUCCESS, FAILURE
}

class Finance {
    financeData: FinanceData;
    balance: number;
    transactions: Transaction[];
    statusMessage: Array<string>;
    private isDebugging: boolean = true;
    private onBalanceChanged: ObservableEvent<void>;

    private static instance: Finance;

    public static getInstance() {
        if (this.instance === undefined) {
            this.instance = new Finance();
        }
        return this.instance;
    }

    constructor() {
        this.setBalance(1000);
        this.transactions = [];
        this.financeData = new FinanceData();
        this.statusMessage = [];
        this.statusMessage[StatusMessageTypes.SUCCESS] = "Transaction successful!";
        this.statusMessage[StatusMessageTypes.FAILURE] = "Transaction failed. Please make sure you have enough money for completing the transaction.";
        this.onBalanceChanged = new ObservableEvent<void>();

    }

    public getBalance(): number {
        return this.balance;
    }

    public setBalance(balance: number): void {
        this.balance = Number(balance.toFixed(2));
    }

    public getFinanceData(): FinanceData {
        return this.financeData;
    }

    public setFinanceData(financeData: FinanceData): void {
        this.financeData = financeData;
    }

    public handleTransaction(transaction: Transaction, showMessage: boolean): boolean {
        if (this.isDebugging) {
            return true;
        }
        else {
            switch (transaction.type) {
                case TransactionTypes.PURCHASE:
                    return this.handlePurchase(transaction, showMessage);
                case TransactionTypes.SALE:
                    return this.handleSale(transaction, showMessage);
            }
        }

    }

    public handlePurchase(transaction: Transaction, showMessage: boolean): boolean {
        if (transaction.amount > this.balance) {
            transaction.status = false;
            if (showMessage)
                window.alert(this.statusMessage[StatusMessageTypes.FAILURE]);
            return false;
        }
        else {
            this.balance = this.balance - transaction.amount;
            transaction.status = true;
            this.transactions.push(transaction);
            if (showMessage)
                window.alert(this.statusMessage[StatusMessageTypes.SUCCESS]);
            this.onBalanceChanged.notifyListeners(null);
            return true;
        }
    }

    public handleSale(transaction: Transaction, showMessage: boolean): boolean {
        if (transaction.amount > 0) {
            this.balance = this.balance + transaction.amount;
            transaction.status = true;
            if (showMessage)
                window.alert(this.statusMessage[StatusMessageTypes.SUCCESS]);
            this.onBalanceChanged.notifyListeners(null);
            return true;
        }
        else {
            transaction.status = false;
            if (showMessage)
                window.alert(this.statusMessage[StatusMessageTypes.FAILURE]);
            return false;
        }
    }

    public checkForEnoughtMoney(price: number): boolean {
        return this.balance >= price;
    }

    public addBalanceChangeListener(listener: () => void): void {
        this.onBalanceChanged.addListener(listener);
        this.onBalanceChanged.notifyListeners(null);
    }
}