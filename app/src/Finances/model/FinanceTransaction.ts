class Transaction {
	type: TransactionTypes;
	status: boolean;
	amount: number;
	
	public getType(): TransactionTypes {
		return this.type;
	}
	
	public setType(type: TransactionTypes): void {
		this.type = type;
	}
	
	public getStatus(): boolean {
		return this.status;
	}
	
	public setStatus(status: boolean): void {
		this.status = status;
	}
	
	public getAmount(): number {
		return this.amount;
	}
	
	public setAmount(amount: number): void {
		this.amount = amount;
	}
	
	constructor(type: TransactionTypes, amount: number) {
		this.type = type;
		this.amount = amount;
	}
}