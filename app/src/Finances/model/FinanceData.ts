class FinanceData {
	linePrice: number;							// Fixed
	stationPrice: number;						// Fixed
	
	vehicleOperationBasicPrice: number;			// Actual Price is multiple basic price
	stationToLineAssignmentBasicPrice: number;	// Actual Price is multiple basic price
	
	ticketPrice: number;						// Set by user
	companyImage: number;						// Changes depending on some properties
    
    private LINE_PRICE: number = 10;
    private STATION_PRICE: number = 10;
    private STATION_TO_LINE_ASSIGNMENT_BASIC_PRICE: number = 10;
    private VEHICLE_OPERATION_BASIC_PRICE: number = 10;
	
	public getLinePrice(): number {
		return this.linePrice;
	}
	
	public setLinePrice(linePrice: number): void {
		this.linePrice = linePrice;
	}
	
	public getStationPrice(): number {
		return this.stationPrice;
	}
	
	public setStationPrice(stationPrice: number): void {
		this.stationPrice = stationPrice;
	}
	
	public getTicketPrice(): number {
		return this.ticketPrice;
	}
	
	public setTicketPrice(ticketPrice: number): void {
		this.ticketPrice = ticketPrice;
	}
	
	public getCompanyImage(): number {
		return this.companyImage;
	}
	
	public setCompanyImage(companyImage: number): void {
		this.companyImage = companyImage;
	}
	
	constructor() {
		this.vehicleOperationBasicPrice = this.VEHICLE_OPERATION_BASIC_PRICE;
		this.stationToLineAssignmentBasicPrice = this.STATION_TO_LINE_ASSIGNMENT_BASIC_PRICE;
		this.stationPrice = this.STATION_PRICE;
		this.linePrice = this.LINE_PRICE;
	}
	
	public calculateVehicleOperationPrice(vehicle: Vehicle): number {
		// Take care of: Capacity, MaxSpeed, Acceleration, Length and maybe Type
		// vehicle.getCapacity() is of type string at the moment.
		var price: number = this.vehicleOperationBasicPrice;
		if (vehicle.getMaxSpeed() != undefined)
			price = price + (vehicle.getMaxSpeed() / 100);
		if (vehicle.getAcceleration() != undefined)
			price = price + (vehicle.getAcceleration() / 10);
		if (vehicle.getLength() != undefined)
			price = price + (vehicle.getLength() / 10);
		if ((Number(vehicle.getCapacity())) != undefined)
			price = price + (Number(vehicle.getCapacity()));
		return price;
	}
	
	public calculateStationToLineAssignmentPrice(distance: number): number {
		return ((distance) / 100 * this.stationToLineAssignmentBasicPrice);
	}
	
	public calculateCompanyImage(): void {
		// Value between 0 and 1.
		if (this.getTicketPrice() < 1)
			this.setCompanyImage(1);
		else
			this.setCompanyImage(1 / this.getTicketPrice());
	}
}