app.controller('FinanceCtrl', function ($scope, googleMapsService) {
	$scope.finances = Finance.getInstance();
	$scope.onAddMoneyClicked = function () {
		var transaction = new Transaction("purchase", $scope.value);
		Finance.getInstance().handleTransaction(transaction);
	}
	$scope.onRemoveMoneyClicked = function () {
		var transaction = new Transaction("sale", $scope.value);
		Finance.getInstance().handleTransaction(transaction);
	}
	$scope.recalculateCompanyImage = function () {
		Finance
			.getInstance()
			.getFinanceData()
			.calculateCompanyImage();
	}
});