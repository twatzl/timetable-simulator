app.controller('HelpCtrl', function($scope) {
    $scope.isHelpButtonPressed = false;

    $scope.ptsDynamicPopover = {
        popupWindowUrl: 'partials/Help/HelpTexts/HelpText.html'
    };

    $scope.mapEditorDynamicPopover = {
        popupWindowUrl: 'partials/Help/HelpTexts/HelpText.html'
    };

    $scope.helpTexts = {
        fastClockPauseButton: 'pause',
        fastClockPlayButton: 'normal speed',
        fastClockForwardButton: 'faster speed',
        fastClockFastForwardButton: 'insane speed',
        fastClockCurrentGameDateTextfield: 'The current date in the game.',
        fastClockCurrentGameTimeTextfield: 'The current time in the game.',

        optionsButton: 'Game Options',
        saveGameButton: 'Save the current game state to a file.',
        loadGameButton: 'Load a game state from a file.',

        hideZonesButton: 'Hide the zones on the map',
        showZonesButton: 'Show the zones on the map.',

        linesAddStreetBoundLineButton: 'Add a new bus line. Bus lines can only follow streets, are slower, but cheaper to build.',
        linesAddUnboundLineButton: 'Add a new train line. Train lines are more expensive than bus lines, but are faster and use the direct way between stations.',
        linesRemoveLineButton: 'Delete this line.',
        linesIdOfLineTextbox: 'The unique number of this line.',
        linesNameOfLineTextbox: 'This is the name of the line. It will be shown in the line list.',
        linesLineColorPicker: 'This is the color of the line. This color is used, when the line is shown on the map',
        linesEditStationsButton: 'Assign or deassign stations to this line. The line then follows the stations.' +
        '\n\n Left-Click on a stations assigns the station to this line.' +
        '\n\n Right-Click removes the station from this line.',
        linesEditTimetablesButton: 'Edit the timetables of this line. Timetables are used to set, when and how many vehicles will drive on this line.',

        vehicleAddVehicleButton: 'Add a new vehicle. Vehicles can later be used to be set in timetables.',
        vehicleRemoveVehicleButton: 'Remove this vehicle.',
        vehicleIdOfVehicleTextbox: 'The unique number of this vehicle.',
        vehicleNameOfVehicleTextbox: 'The name of this vehicle. It will be used to refer to this vehicle.',
        vehicleCapacityOfVehicleTextbox: 'The capacity of this vehicle. It tells the game how many people this vehicle can transport.',
        vehicleMaxSpeedOfVehicleTextbox: 'The maximum speed this vehicle can reach. Given in m/s.',
        vehicleAccelerationOfVehicleTextbox: 'The acceleration which this vehicle has. Given in m/s^2',
        vehicleLengthOfVehicleTextbox: 'The length, that this vehicle takes up, when it arrives in a station.',
        vehicleTypeOfVehicleTextbox: 'to be continued...',
        vehicleInfoWindow: 'Show or hide the infowindow of every vehicle.',

        stationAddStationButton: 'Add a new station. Stations can later be used to create lines.',
        stationRemoveStationButton: 'Remove this station.',
        stationIdOfStationTextbox: 'The unique number of this station.',
        stationNameofStationTextbox: 'The name of this station.',
        stationSizeOfStationTextbox: 'The size this stations has. The larger a station is, the more vehicles can stop at the same time.',
        stationInfoWindow: 'Show or hide the infowindow of every station.',

        timetablesValidFromTextfield: 'Date the timetable is running from',
        timetablesValidToTextfield: 'Date the timetable is running to',
        timetablesBeginTimeTimepicker: 'Time the timetable starts to be in use each day',
        timetablesEndTimeTimepicker: 'Time the timetable ends to be in use each day',
        timetablesWeekDayButton: 'Weekdays the timetable is in use',
        timetablesRemoveTimetableButton: 'Removes the timetable',
        timetablesSetIntervalButton: 'Every x minutes a new course will start during the time of operation',
        timetablesSetIntervalTextfield: 'Every x minutes a new course will start during the time of operation',
        timetablesVehicleDropdown: 'Assigns a vehicle to a timetable\'s courses',
        timetablesAddTimetableButton: 'Adds a new timetable',
        timetablesOkButton: 'Closes the modal window and gets you back to the main view.',
        timetablesCancelButton: 'Closes the modal window and gets you back to the main view.',

        financeBalance: 'The money of the company. Money is needed to buy vehicles, stations, lines and to keep the vehicle moving.',
        financeTicketPrice: 'The price for every a ticket. This price get the company for every passenger.',
        financeCompanyImage: 'This is the image of your company. It is important to keep it high, people will use the vehicles more.',

        mapEditorEditModeButton: 'Unlock the zones. Zones can moved or resized.',
        mapEditorLockModeButton: 'Lock the zones. Zones can not moved or resized.',
        mapEditorSaveButton: 'Save the current zones to a file.',
        mapEditorLoadButton: 'Load the zones from a file.',
        mapEditorDenseZone: ' The intensity is dense.',
        mapEditorMediumZone: ' The intensity is medium.',
        mapEditorLightZone: ' The intensity is light.',
        mapEditorResidentialZone: 'Create Residential zones.',
        mapEditorIndustryZone: 'Create Industry zones.',
        mapEditorCommercialZone: 'Create Commercial zones.'


    };

});