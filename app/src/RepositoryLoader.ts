/// <reference path="JSONFileReader.ts"/>

class RepositoryLoader extends JSONFileReader {

	public static load(filePath: string, fs: any, dialog: any): JSON {
		return this.readFromJSONFile(filePath, fs, dialog);
	}
}