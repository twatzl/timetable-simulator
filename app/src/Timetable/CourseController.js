app.controller('ModalCoursesCtrl', function ($scope, $modalInstance, $uibModal, timetableId, interval) {
	$scope.timetable = TimetableRepository.getInstance().getObjectById(timetableId);
	$scope.vehicleRepository = VehicleRepository.getInstance();
	$scope.courseRepository = CourseRepository.getInstance();
	$scope.selectedVehicles = {};
	$scope.intervalCounter = 0;
	
	$scope.onOkClicked = function () {
		$modalInstance.close();
	};

	$scope.onCancelClicked = function () {
		$modalInstance.dismiss('cancel');
	};
	
	$scope.initVehicle = function (kursId) {
		if ($scope.courseRepository.getObjectById(kursId).vehicle != undefined) {
			$scope.selectedVehicles[kursId] = $scope.courseRepository.getObjectById(kursId).vehicle.id;
		} else {
			$scope.selectedVehicles[kursId] = "-1";
		}
	}

	$scope.onVehicleSelected = function (kursId) {
		$scope.courseRepository.getObjectById(kursId).setVehicle($scope.vehicleRepository.getObjectById($scope.selectedVehicles[kursId]));
	};
	
	$scope.onGenerateCoursesClicked = function () {
		$scope.timetable.courses = [];
		$scope.intervalCounter = 0;
		for (var i = 0; i < ($scope.timetable.endTime.getTime() - $scope.timetable.beginTime.getTime()) / (interval*1000); i++) {
			$scope.onAddCourseClicked();
		}
	}
	
	$scope.addStationsToCourseElements = function () {
		var tempCourseElements = [];
		$scope.timetable.line.getWaypointManager().getStationList().forEach(function (element, index) {
			var courseElement = new CourseElement();
			courseElement.station = element;
			tempCourseElements.push(courseElement);
		}, this);
		return tempCourseElements;
	}
	
	$scope.calculateCourseTimes = function (index) {
		var timeBetweenStation = $scope.timetable.line.getTimesBetweenStations()[index];
		timeBetweenStation = timeBetweenStation >= 60 ? timeBetweenStation*1000: 60000;
		return timeBetweenStation;
	}
	
	$scope.addCourseElement = function (lastCourseElement, element, kurs) {
		if (lastCourseElement == undefined) {
			kurs.getCourseElements().push(element);
		}
		else {
			if (lastCourseElement.station != element.station) {
				kurs.getCourseElements().push(element);
			}
		}
	}
	
	$scope.deleteNotNeededCourseElements = function (kurs) {
		kurs.getCourseElements().forEach(function (element, index) {
			if (element.arrivalTime > $scope.timetable.endTime) {
				kurs.getCourseElements().splice(index, kurs.getCourseElements().length - index);
			}
		});
		return kurs;
	}
	
	$scope.onAddCourseClicked = function () {
		var kurs = new Kurs("New Course", $scope.timetable);
		var lastCourseElement, timeIndex;
		var oppositeDirection = false;
		var arrivalTime = new Date($scope.timetable.beginTime.getTime() + (interval * $scope.intervalCounter * 1000));
		var departureTime = new Date($scope.timetable.beginTime.getTime() + (interval * $scope.intervalCounter * 1000));
		
		for (var i = 0; arrivalTime <= $scope.timetable.endTime; i++) {
			var courseElements = $scope.addStationsToCourseElements();
			
			// Reverses course elements' order
			if (oppositeDirection) {
				courseElements.reverse();
			}
			
			// Sets time for one direction of course elements
			courseElements.forEach (function (element, index) {
				element.departureTime = departureTime;
				element.arrivalTime = arrivalTime;
				if (index != 0) {
					if (oppositeDirection) {
						timeIndex = $scope.timetable.line.getTimesBetweenStations().length-index;
					}
					else {
						timeIndex = index -1;
					}
					
					var timeBetweenStation = $scope.calculateCourseTimes(timeIndex);
					
					element.arrivalTime = new Date(element.arrivalTime.getTime() + timeBetweenStation);
					element.departureTime = new Date(element.departureTime.getTime() + timeBetweenStation);
					
					arrivalTime = element.arrivalTime;
					departureTime = element.departureTime;
				}
				$scope.addCourseElement(lastCourseElement, element, kurs);
			});
			oppositeDirection = !oppositeDirection;
			lastCourseElement = kurs.getCourseElements()[kurs.getCourseElements().length -1];
		}
		kurs = $scope.deleteNotNeededCourseElements(kurs);
		$scope.timetable.courses.push(kurs);
		$scope.courseRepository.addObject(kurs);
		$scope.intervalCounter++;
	}

	$scope.onRemoveCourseClicked = function (kurs) {
		$scope.timetable.courses.forEach(function (element, index) {
			if (element.id == kurs.id) {
				$scope.timetable.courses.splice(index, 1);
			}
		}, this);
		CourseRepository.getInstance().deleteObjectById(kurs.id);
	}
});