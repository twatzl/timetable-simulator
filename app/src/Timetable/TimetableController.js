app.controller('ModalTimetableCtrl', function ($scope, $modalInstance, $uibModal, line, isHelpButtonPressed, helpTexts) {
	$scope.timetables = line.getTimetableManager().getAllObjects();
	$scope.vehicleRepository = VehicleRepository.getInstance();
	$scope.courseRepository = CourseRepository.getInstance();
	
	$scope.selectedVehicles = {};
	
	$scope.isHelpButtonPressed = isHelpButtonPressed;
	$scope.helpTexts = helpTexts;
	
	$scope.isOk = true;
	
	$scope.onOkClicked = function () {
		$scope.iterateTimetables();
		if ($scope.isOk == true)
			$modalInstance.close();
	};

	$scope.onCancelClicked = function () {
		$modalInstance.dismiss('cancel');
	};
	
	$scope.onAddTimetableClicked = function () {
		var timetable = new Timetable();
		
		var currentYear = (new Date()).getFullYear();
		var currentMonth = (new Date()).getMonth();
		var currentDay = (new Date()).getDate();
		
		timetable.validFromDate = new Date(currentYear, 1, 1);
		timetable.validToDate = new Date(currentYear, 12, 31);
		timetable.beginTime = new Date(currentYear, currentMonth, currentDay, 6, 0, 0, 0);
		timetable.endTime = new Date(currentYear, currentMonth, currentDay, 22, 0, 0, 0);
		timetable.courses = [];
		timetable.line = line;
		line.getTimetableManager().add(timetable);
		TimetableRepository.getInstance().addObject(timetable);
		$scope.timetables = line.getTimetableManager().getAllObjects();
	}
	
	$scope.onRemoveTimetableClicked = function (timetable) {
		line.getTimetableManager().remove(timetable);
		TimetableRepository.getInstance().deleteObjectById(timetable.id);
		var index = $scope.timetables.indexOf(timetable);
		$scope.timetables.splice(index, 1);
	}
	
	$scope.initVehicle = function (timetableId) {
		var currentTimetable = TimetableRepository.getInstance().getObjectById(timetableId);
		if (currentTimetable.getCourses()[0] == undefined) {
			return;
		}
		if (currentTimetable.getVehicle() == undefined) {
			$scope.selectedVehicles[timetableId] = "-1";
		}
		else {
			$scope.selectedVehicles[timetableId] = currentTimetable.getVehicle().id;
		}
	}
	
	$scope.onRemoveVehicleClicked = function (timetableId) {
		var timetable = TimetableRepository.getInstance().getObjectById(timetableId);
		timetable.setVehicle(undefined);
		timetable.getCourses().forEach(function (myCourse) {
			myCourse.setVehicle(undefined);
		});
		$scope.selectedVehicles[timetableId] = undefined;
	}
	
	$scope.checkIfVehicleAllowed = function (timetableId) {
		var currInterval = TimetableRepository.getInstance().getObjectById(timetableId).interval;
		if (currInterval == 0 || currInterval == undefined) {
			$scope.selectedVehicles[timetableId] = "-1";
		}
	}
	
	$scope.onVehicleSelected = function (timetableId) {
		//if (TimetableRepository.getInstance().getObjectById(timetableId).getCourses().length == 0) {
		/*TimetableRepository.getInstance().getObjectById(timetableId).courses.forEach(function(element, index) {
			element.setVehicle($scope.vehicleRepository.getObjectById($scope.selectedVehicles[timetableId]));	
		});*/
		var currentTimetable = TimetableRepository.getInstance().getObjectById(timetableId); 
		if (currentTimetable.interval == 0 || currentTimetable.interval == undefined) {
			window.alert("Please set interval before setting a vehicle.");
			$scope.selectedVehicles[timetableId] = "-1";
			$scope.isOk = false;
		}
		else {
			$scope.isOk = true;
			if ($scope.selectedVehicles[timetableId] == undefined) {
				return;
			} else {
				currentTimetable.setVehicle(VehicleRepository.getInstance().getObjectById($scope.selectedVehicles[timetableId]));
				currentTimetable.onSetCoursesVehicle();
			}
		}
	};
	
	$scope.iterateTimetables = function () {
		for (var key in $scope.timetables) {
			var currTimetable = $scope.timetables[key];
			var selectedVehicle = $scope.selectedVehicles[currTimetable.id];
			if (selectedVehicle != undefined && selectedVehicle != "-1") {
				if (currTimetable.interval == undefined) {
					currTimetable.setInterval(Timetable.DEFAULT_INTERVAL);
				}
				$scope.handleCoursesAndTimetables(currTimetable.id);
				$scope.isOk = true;
			}
			else {
				window.alert("There was a problem with timetable " + currTimetable.id);
				currTimetable.interval = undefined;
				$scope.isOk = false;
			}
		}
	}
	
	$scope.savedVehicles = {};
	
	$scope.handleCoursesAndTimetables = function (id) {
		var timetable = TimetableRepository.getInstance().getObjectById(id);
		// Saves the vehicle if one was there before.
		if (timetable.courses[0] != undefined) {
			if (timetable.courses[0].getVehicle() != undefined) {
				$scope.savedVehicles[timetable.id] = timetable.courses[0].getVehicle();
			}
		}
		for (var key in timetable.courses) {
			var currentCourse = timetable.courses[key];
			currentCourse.setVehicle(undefined);
		}
		timetable.courses = [];
		$scope.generateCourses(timetable);
		if ($scope.savedVehicles[timetable.id] == undefined) {
			var selectedVehicle = VehicleRepository.getInstance().getObjectById($scope.selectedVehicles[timetable.id]);
			timetable.setVehicle(selectedVehicle);
		} else {
			timetable.setVehicle($scope.savedVehicles[timetable.id]);
		}
		for (var key in timetable.getCourses()) {
			timetable.getCourses()[key].setVehicle(timetable.getVehicle());
		}
	}
	
	$scope.generateCourses = function (timetable) {
		var beginTime = new Time();
		beginTime.convertDateToTime(timetable.beginTime);
		var endTime = new Time();
		endTime.convertDateToTime(timetable.endTime);
		var nextBeginTime = new Time(beginTime);
		var callback = function() {
			if (timetable.line == undefined) {
				return;
			}
			if (timetable.vehicle == undefined) {
				return;
			}
			console.log("Executed simple vehicle callback");
			new SimpleVehicle(timetable.vehicle, timetable.line);
			nextBeginTime.addMinutes(timetable.interval);
			if (nextBeginTime.compareTo(endTime) < 0) {
				FastClock.getInstance().notifyAtTime(nextBeginTime, callback);
			}
		}
		FastClock.getInstance().notifyAtTime(beginTime, callback);
	}
	
	$scope.setOneCourse = function (timetable, currTime) {
		var stop = false;
		
		var kurs = new Kurs();
		kurs.setTimetable(timetable);
		kurs.setName("New Course");
		
		var difference = 0;
		var index = 0;
		var revert = false;
		
		while (!stop) {
			var stations = timetable.line.getWaypointManager().getStationList();
			var courseElement = new CourseElement();
			var departureTime = new Time();
			var newDepartureTime = new Date(currTime.getTime() + (difference * 1000));
			departureTime.convertDateToTime(newDepartureTime);
			if (index < stations.length - 1) {
				difference += $scope.getWayTime(timetable, index);
			} else {
				difference += $scope.getWayTime(timetable, index-1);
			}
			courseElement.setDepartureTime(departureTime);
			courseElement.setStation(stations[index]);
			kurs.getCourseElements().push(courseElement);
			if (index == stations.length-1) {
				revert = true;
			}
			if (!revert) {
				index++;
			} else {
				index--;
			}
			if (kurs.getCourseElements().length == (stations.length * 2) - 1) {
				stop = true;
			}
		}
		return kurs;
	};
	
	$scope.getWayTime = function (timetable, index) {
		return timetable.line.getTimeBetweenStationsList()[index];
	}
});