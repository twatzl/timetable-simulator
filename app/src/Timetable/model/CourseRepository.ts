class CourseRepository extends Repository<Kurs> {

	private static instance: CourseRepository;

	constructor() {
		super();
	}

	public static getInstance() {
		if (this.instance === undefined) {
			this.instance = new CourseRepository();
		}
		return this.instance;
	}

}
