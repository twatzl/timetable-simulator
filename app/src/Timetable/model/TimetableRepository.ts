class TimetableRepository extends Repository<Timetable> {

	private static instance: TimetableRepository;

	constructor() {
		super();
	}

	public static getInstance() {
		if (this.instance === undefined) {
			this.instance = new TimetableRepository();
		}
		return this.instance;
	}

}
