class DayObject {
	weekDay: DaysOfWeek;
	enabled: boolean;

	constructor(weekDay: DaysOfWeek, enabled: boolean) {
		this.weekDay = weekDay;
		this.enabled = enabled;
	}

	public getName(): string {
		return DaysOfWeek[this.weekDay];
	}
}

class Timetable extends EntityObject {
	// Only use if no interval was specified by the user.
	static DEFAULT_INTERVAL: number = 15;
	
	private line: Line;
	private courses: Kurs[];
	private vehicle: Vehicle;
	private validDays: DayObject[];
	private validFromDate: Date;
	private validToDate: Date;
	private beginTime: Date;
	private endTime: Date;
	private interval: number;
	//courseIndexForVehicle: number;
	
	public getInterval(): number {
		return this.interval;
	}
	
	public setInterval(interval: number): void {
		this.interval = interval;
	}

	public getVehicle() : Vehicle {
		return this.vehicle;
	}
	
	public setVehicle(vehicle: Vehicle): void {
		this.vehicle = vehicle;
		if (this.vehicle != undefined) {
			this.vehicle.addOnDeletedListener((eventArgs) => {
				this.setVehicle(undefined);
			});
		}
	}
	
	public getLine() : Line {
		return this.line;
	}
	
	public setLine(line : Line): void {
		this.line = line;
	}

	public getCourses() : Kurs[] {
		return this.courses;
	}
	
	public setCourses(courses: Kurs[]): void {
		this.courses = courses.slice();
	}

	public getValidDays() : DayObject[] {
		return this.validDays;
	}
	
	public setValidDays(validDays: DayObject[]): void {
		this.validDays = validDays.slice();
	}

	public getValidFromDate() : Date {
		return this.validFromDate;
	}
	
	public setValidFromDate(validFromDate: Date) : void {
		this.validFromDate = validFromDate;
	}

	public getValidToDate() : Date {
		return this.validToDate;
	}
	
	public setValidToDate(validToDate: Date) : void {
		this.validToDate = validToDate;
	}

	public getBeginTime() : Date {
		return this.beginTime;
	}
	
	public setBeginTime(beginTime: Date): void {
		this.beginTime = beginTime;
	}
	
	public getEndTime(): Date {
		return this.endTime;
	}
	
	public setEndTime(endTime: Date): void {
		this.endTime = endTime;
	}

	constructor(jsonData?: any) {
		super(TimetableRepository.getInstance().getNextId(), jsonData);
		
		this.initValidDays();
		this.courses = [];
		
		if (jsonData != undefined) {
			this.parseFromJson(jsonData);
		} else {
			this.init();
		}
		
		VehicleRepository.getInstance().addOnObjectRemovedListener(this.updateVehicleOnDeleted());
		LineRepository.getInstance().addOnObjectRemovedListener(this.updateLineOnDeleted());
	}
	
	public parseFromJson(jsonData: any): void {
		this.endTime = new Date(jsonData.endTime);
		this.beginTime = new Date(jsonData.beginTime);
		this.validToDate = new Date(jsonData.validToDate);
		this.validFromDate = new Date(jsonData.validFromDate);
		this.interval = jsonData.interval;
		
		var instance = this;
		
		jsonData.validDays.forEach(function (element: any, index: any) {
			instance.validDays[index].enabled = element.enabled;
		});
		
		this.courses = [];
		
		var instance = this;
		
		jsonData.courses.forEach(function (element: any, index: any) {
			instance.courses.push(CourseRepository.getInstance().getObjectById(element));
		});

		this.line = LineRepository.getInstance().getObjectById(jsonData.line);
		this.vehicle = VehicleRepository.getInstance().getObjectById(jsonData.vehicle);
	}
	
	public init(): void {
		//this.courseIndexForVehicle = 0;
		this.beginTime = new Date();
		this.endTime = new Date();
	}
	
	public initValidDays (): void {
		this.validDays = [];
		this.validDays.push(new DayObject(DaysOfWeek.Monday, true));
		this.validDays.push(new DayObject(DaysOfWeek.Tuesday, true));
		this.validDays.push(new DayObject(DaysOfWeek.Wednesday, true));
		this.validDays.push(new DayObject(DaysOfWeek.Thursday, true));
		this.validDays.push(new DayObject(DaysOfWeek.Friday, true));
		this.validDays.push(new DayObject(DaysOfWeek.Saturday, true));
		this.validDays.push(new DayObject(DaysOfWeek.Sunday, true));
	}
	
	public updateVehicleOnDeleted (): (eventArgs: RepositoryEventArgs<Vehicle>) => void {
		return (eventArgs: RepositoryEventArgs<Vehicle>) : void => {
			if (this.vehicle != undefined) {
				if (this.vehicle.id == eventArgs.getId()) {
					this.vehicle = undefined;
				}
			}
		}
	}
	
	public updateLineOnDeleted (): (eventArgs: RepositoryEventArgs<Line>) => void {
		return (eventArgs: RepositoryEventArgs<Line>): void => {
			if (this.line != undefined) {
				if (this.line.id == eventArgs.getId()) {
					this.setLine(undefined);
				}	
			}
		}
	}
	
	public deleted(): void {
		this.courses.forEach((course: Kurs, index: number, array: Kurs[]) => {
			if (CourseRepository.getInstance().getObjectById(course.id) != undefined) {
				CourseRepository.getInstance().deleteObjectById(course.id);	
			}
		});
	}
	
	public onSetCoursesVehicle(): void {
		if (this.vehicle != undefined) {
			this.courses.forEach(element => {
				FastClock.getInstance().notifyAtTime(element.getCourseElements()[0].getDepartureTime(), this.getVehicleStartCallback(element, this.vehicle));
			});
		}
	}
	
	public getVehicleStartCallback(course: Kurs, vehicle: Vehicle): any {
		return () => { course.setVehicle(vehicle); }
	}
	
	public getJSONString(): string {
		var coursesData: string[] = [];
		
		this.courses.forEach(element => {
			coursesData.push(element.id);
		});

		return JSON.stringify(this, ( key, value) => {
			switch (key) {
				case "courses":
					return coursesData;
				case "line":
					return value.id;
				case "vehicle":
					if (value == undefined) {
						return;
					}
					else {
						return value.id;	
					}
				default:
					return value;
			}
		});
	}
}
