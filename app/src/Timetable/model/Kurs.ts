class Kurs extends EntityObject {
	private vehicle: Vehicle;
	private timetable: Timetable;
	private courseElements: CourseElement[];
	private name: string;
	private courseVehicle: CourseVehicle;
	
	public getVehicle(): Vehicle {
		return this.vehicle;
	}

	public setVehicle(vehicle: Vehicle) {
		this.vehicle = vehicle;
		if (this.courseVehicle != undefined) {
			VehicleInstanceRepository.getInstance().deleteObjectById(this.courseVehicle.id);
			this.courseVehicle = undefined;
		}
		if (this.vehicle != undefined && this.vehicle != null) {
			VehicleRepository.getInstance().addOnObjectRemovedListener(this.updateVehicleOnDeleted());
			if (this.courseElements[0] != undefined) {
				const time = new Time(this.courseElements[0].departureTime);
				time.subtractMinutes(1);
				FastClock.getInstance().notifyAtTime(time, () => {
					this.courseVehicle = new CourseVehicle(this.vehicle, this);
					VehicleInstanceRepository.getInstance().addObject(this.courseVehicle);
				});
			}
		}
	}
	
	public updateVehicleOnDeleted (): (eventArgs: RepositoryEventArgs<Vehicle>) => void {
		return (eventArgs: RepositoryEventArgs<Vehicle>): void => {
			if (this.vehicle != undefined) {
				if (this.vehicle.id == eventArgs.getId()) {
					this.setVehicle(undefined);
					this.setCourseElements([]);
				}
			}
		}
	}

	public getTimetable(): Timetable {
		return this.timetable;
	}

	public setTimetable(timetable: Timetable) {
		this.timetable = timetable;
	}

	public getCourseElements(): CourseElement[] {
		return this.courseElements;
	}
	
	public setCourseElements(courseElements: CourseElement[]): void {
		this.courseElements = courseElements.slice();
	}

	public getName(): string {
		return this.name;
	}
	
	public setName(name : string): void {
		this.name = name;	
	}
	
	constructor(jsonData?: any) {
		super(CourseRepository.getInstance().getNextId(), jsonData);
		
		if (jsonData != undefined) {
			this.parseFromJson(jsonData);
		} else {
			this.courseElements = [];
		}
		
		VehicleRepository.getInstance().addOnObjectRemovedListener(this.updateVehicle());
		TimetableRepository.getInstance().addOnObjectRemovedListener(this.updateTimetable());
	}
	
	public parseFromJson(jsonData: any): void { 
		this.name = jsonData.name;
		this.courseElements = jsonData.courseElements.slice();
		
		this.timetable = TimetableRepository.getInstance().getObjectById(jsonData.timetable);
		this.vehicle = VehicleRepository.getInstance().getObjectById(jsonData.vehicle);
	}
	
	public updateVehicle (): (eventArgs: RepositoryEventArgs<Vehicle>) => void {
		return (eventArgs: RepositoryEventArgs<Vehicle>): void => {
			if (this.vehicle != undefined) {
				if (this.vehicle.id == eventArgs.getId()) {
					this.setVehicle(undefined);
				}	
			}
		}
	}
	
	public updateTimetable (): (eventArgs: RepositoryEventArgs<Timetable>) => void {
		return (eventArgs: RepositoryEventArgs<Timetable>): void => {
			if (this.timetable != undefined) {
				if (this.timetable.id == eventArgs.getId()) { 
					this.setTimetable(undefined);
				}
			}
		}
	}
	
	public deleted(): void {
		if (this.courseVehicle != undefined) {
			VehicleInstanceRepository.getInstance().deleteObjectById(this.courseVehicle.id);	
		}
	}
	
	public getJSONString(): string {
		var courseElementsJSON: any[] = [];
		this.courseElements.forEach(element => {
			courseElementsJSON.push({
					arrivalTime: element.arrivalTime,
					departureTime: element.departureTime,
					manual: element.manual,
					waitAtStation: element.waitAtStation,
					stay: element.stay,
					station: element.station.id
				});
		});
		
		return JSON.stringify(this, function (key, value) {
			switch(key) {
				case "vehicle":
					if (value == undefined) {
						return;
					} else {
						return value.id;	
					}
				case "timetable":
					return value.id;
				case "courseElements":
					return courseElementsJSON;
				default:
					return value;
			}
		});
	}
}
