class CourseElement {
	arrivalTime: Time;
	departureTime: Time;
	station: Station;
	manual: boolean;
	waitAtStation: boolean;
	stay: boolean;
	id: number;
	
	constructor() {
		this.id = Math.random();
	}

	public getArrivalTime() : Time {
		return this.arrivalTime;
	}
	
	public setArrivalTime(arrivalTime: Time) {
		this.arrivalTime = arrivalTime;
	}
	
	public getDepartureTime() : Time {
		return this.departureTime;
	}
	
	public setDepartureTime(departureTime: Time) {
		this.departureTime = departureTime;
	}
	
	public getStation() : Station {
		return this.station;
	}
	
	public setStation(station: Station) {
		this.station = station;
		WaypointRepository.getInstance().addOnObjectRemovedListener((eventArgs: RepositoryEventArgs<Station>) => this.updateStationOnDeleted(eventArgs));
	}
	
	updateStationOnDeleted(eventArgs: RepositoryEventArgs<Station>): void {
		if (this.station != undefined) {
			if (this.station.id == eventArgs.getId()) {
				this.station = undefined;
				const courses = CourseRepository.getInstance().getObjectList();
				// Somehow 'this' in the for loop is undefined.
				const instanceId = this.id;
				for (const key in courses) {
					let index: number = -1;
					courses[key].getCourseElements().filter((x,i) => {
						if (x.id == instanceId) {
							index = i;
							return true;
						} else {
							return false;
						}
					});
					if (index >= 0) {
						courses[key].getCourseElements().splice(index,1);
					}
				}
			}
		}
	}
	
	public getManual() : boolean {
		return this.manual;
	}
	
	public setManual(manual: boolean) {
		this.manual = manual;
	}
	
	public getWaitAtStation() : boolean {
		return this.waitAtStation;
	}
	
	public setWaitAtStation(waitAtStation: boolean) {
		this.waitAtStation = waitAtStation;
	}
	
	public getStay() : boolean {
		return this.stay;
	}
	
	public setStay(stay: boolean) {
		this.stay = stay;
	}
}