class MapSaver extends JSONFileWriter {

	public static save(filePath: string, fs: any, dialog: any) {
		this.writeInJSONFile(filePath, "{" + ZoneRepository.getInstance().getJSONStringOfRepository("zones").slice(0, -1) + "}", fs, dialog);
	}
}