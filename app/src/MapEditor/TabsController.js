app.controller('TabsCtrl', function ($scope, googleMapsService) {
	$scope.scope = $scope;
	$scope.map = googleMapsService;
	$scope.tabs = [
		{ title: 'Zones', contentURL: '././partials/Zones/ZonesPartial.html'}
	];
});