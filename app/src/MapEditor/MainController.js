var app = angular.module('TimetableSimulator', ['ui.bootstrap']);

app.service('googleMapsService', function () {
	this.map = {};
 
	// Init Google Maps
	this.init = function (element, lat, lng, zoom, options) {
		var self = this,
			mapOptions,
			map;
 
		// Default Optionen
		mapOptions = { // https://developers.google.com/maps/documentation/javascript/reference#MapTypeStyleFeatureType
			center: new google.maps.LatLng(lat, lng),
			zoom: zoom,
			styles: [
				{
					featureType: "poi",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
				},
				{
					featureType: "transit",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
				}

			]
		};
		
		options = options || {};
 
		// Fügt die default Optionen und eigenen Optionen zusammen
		angular.extend(mapOptions, options);
 
		// Erzeugt neue Google Karte mit entsprechenden Optionen, bindet sie an das DOM-Element
		map = new google.maps.Map(element, mapOptions);
		self.map = map;

		return map;
	};
});

app.service('googleDrawingManagerService', function () {
	this.drawingManager = {};

	this.init = function () {
		this.drawingManager = new google.maps.drawing.DrawingManager({
			drawingMode: google.maps.drawing.OverlayType.RECTANGLE,
			drawingControl: false,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_CENTER,
				drawingModes: [
					google.maps.drawing.OverlayType.RECTANGLE
				]
			}
		});
		
		return this.drawingManager;
	};
});

app.controller('MainCtrl', function ($scope, $uibModal, $log, googleMapsService, googleDrawingManagerService) {
	//$scope.items = ['item1', 'item2', 'item3'];

	$scope.animationsEnabled = true;
	
	googleMapsService.init(document.getElementById('map'), 48.2542298, 14.3406374, 10, {});
	
	googleDrawingManagerService.init();
	
	$scope.toggleAnimation = function () {
		$scope.animationsEnabled = !$scope.animationsEnabled;
	};

	/*$scope.open = function (size) {

		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'ModalPartial.html',
			controller: 'ModalInstanceCtrl',
			size: size,
			resolve: {
				items: function () {
					return $scope.items;
				}
			}
		});

		modalInstance.result.then(function (selectedItem) {
			$scope.selected = selectedItem;
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};*/
});