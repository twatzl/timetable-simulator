class LockedEvent<T> extends ObservableEvent<T> {
	
	private delay: number; /** delay in seconds */
	private locked: boolean;
	
	/**
	 * delay: the delay in seconds that this event should have.
	 */
	constructor(delay: number) {
		super();
		this.delay = delay;
	}
	
	private getUnlockCallback(): () => void {
		var event = this;
		return function() {
			event.unlock();
			//console.log("event unlocked");
		}
	}
	
	private isLocked(): boolean {
		return this.locked;
	}
	
	private lock(): void {
		this.locked = true;
	}
	
	private unlock(): void { 
		this.locked = false;
	}
	
	private scheduleUnlock(): void {
		//console.log("scheduled unlock");
		var unlockTime: Date = new Date(FastClock.getInstance().getTime().toString());
		unlockTime.setSeconds(unlockTime.getSeconds() + this.delay);
		FastClock.getInstance().notifyAt(unlockTime, this.getUnlockCallback())
	}
	
	public notifyListeners(eventArgs: T): void {
		if (this.isLocked()) {
			return;
		}
		
		this.lock();
		super.notifyListeners(eventArgs);
		this.scheduleUnlock();
	}
	
}