app.controller('VehiclesCtrl', function($scope, googleMapsService) {
    $scope.selectedIndex = -1;
    $scope.canRecalulate = false;
    $scope.vehicleInfoWindowText = "Show Vehicle Info";
    VehicleInstanceRepository.getInstance().showInfoWindow = false;

    $scope.onVehicleAdded = function() {
        var vehicle = new Vehicle();
        vehicle.setName("Vehicle " + vehicle.id);
        VehicleRepository.getInstance().addObject(vehicle);
        $scope.selectedVehicle = vehicle;
        $scope.canRecalulate = true;
    }

    $scope.onVehicleSelected = function(id, $index) {
        $scope.selectedVehicle = VehicleRepository.getInstance().getObjectById(id);
        $scope.canRecalulate = true;
        $scope.selectedIndex = $index;
    }

    $scope.onVehicleRemoved = function(id) {
        VehicleRepository.getInstance().deleteObjectById(id);
    }

    $scope.recalculateOperatingPrice = function() {
        if ($scope.canRecalulate) {
            console.log("changed!!");
            console.log("The vehicle is: ");
            console.log($scope.selectedVehicle);
            $scope.selectedVehicle.setOperatingPrice(Finance.getInstance().getFinanceData().calculateVehicleOperationPrice($scope.selectedVehicle));
        }
    }

    $scope.vehicles = VehicleRepository.getInstance().getObjectList();

    $scope.onShowOrHideVehicleInfoWindowsButton = function() {
        if (!VehicleInstanceRepository.getInstance().getIsShowInfoWindowActive()) {
            $scope.vehicleInfoWindowText = "Hide Vehicle Info";
            VehicleInstanceRepository.getInstance().showVehicleInfoWindows(googleMapsService.map);
        }
        else {
            $scope.vehicleInfoWindowText = "Show Vehicle Info";
            VehicleInstanceRepository.getInstance().hideVehicleInfoWindows();
        }
    }
});