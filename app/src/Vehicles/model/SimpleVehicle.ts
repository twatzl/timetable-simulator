class SimpleVehicle extends VehicleInstance {
	line: Line;

	constructor(vehicle: Vehicle, line: Line) {
		const lineName = line.data.getName();
		super(vehicle, lineName , new SimpleWaypointIterator(SimpleVehicle.prepareWaypointListForLine(line)));
		this.line = line;
		this.onLastWaypointReachedEvent.addListener((movableObject: MovableObject) => VehicleInstanceRepository.getInstance().deleteObjectById(this.getId()));
		LineRepository.getInstance().addOnObjectRemovedListener((eventArgs: RepositoryEventArgs<Line>) => this.lineDeletedListener(eventArgs));
	}
	
	lineDeletedListener(eventArgs: RepositoryEventArgs<Line>) : void {
		if (eventArgs.getId() == this.line.id) {
			this.onLineDeleted(this);
		}
	}
	
	onLineDeleted(simpleVehicle: SimpleVehicle): void {
		if (simpleVehicle != undefined) {
			VehicleInstanceRepository.getInstance().deleteObjectById(simpleVehicle.id);
		}
	}
	
	private static prepareWaypointListForLine(line: Line): Waypoint[] {
		let waypoints: Waypoint[] = [];
		let stations: Station[] = line.getStationList();
		
		for (var i = 0; i < stations.length - 1; i++) {
			var waypointsBetweenStations = line.getWaypointManager().getWaypointsBetween(stations[i], stations[i+1]);
			waypoints.push(stations[i]);
			waypointsBetweenStations.forEach(waypoint => {
				waypoints.push(waypoint);
			})
		}
		
		for (var i = stations.length - 1; i > 0; i--) {
			var waypointsBetweenStations = line.getWaypointManager().getWaypointsBetween(stations[i], stations[i-1]);
			waypointsBetweenStations.reverse();
			waypoints.push(stations[i]);
			waypointsBetweenStations.forEach(waypoint => {
				waypoints.push(waypoint);
			})
		}
		
		waypoints.push(stations[0]);
		return waypoints;
	}

	public getJSONString(): string {
		// TODO
		return "";
	}
}