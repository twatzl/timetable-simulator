class VehicleInstanceData extends MovableObjectData {

	constructor(currentLocation: google.maps.LatLng, vehicle: Vehicle, stopped: boolean) {
		super(currentLocation, stopped);
		this.setVehicle(vehicle);
	}

	private Vehicle: Vehicle;
	public getVehicle(): Vehicle {
		return this.Vehicle;
	}
	public setVehicle(v: Vehicle) {
		this.Vehicle = v;
	}

	protected getMaxSpeed(): number {
		return this.getVehicle().getMaxSpeed();
	}
	
	public getAcceleration(): number {
		return this.getVehicle().getAcceleration();
	}
	
	public getDeceleration(): number {
		return this.getVehicle().getAcceleration();
	}
	
}