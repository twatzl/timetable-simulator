class VehicleSimulation {

    private currentPassengers: Person[]
    private onStationReachedEvent: ObservableEvent<Station>;

    constructor(private vehicleInstance: VehicleInstance, private lineName: string) {
        this.currentPassengers = [];
        this.onStationReachedEvent = new ObservableEvent<Station>();
        this.vehicleInstance.addOnStationReachedListener((station: Station) => this.onStationReached(station));
    }

    private onStationReached(station: Station) {
        var stationSimulation = station.getSimulation();
        var passengers: Person[] = stationSimulation.collectWaitingPersons(this.lineName, this.getRemainingCapacity());

        this.notifyCurrentPassengers(station);

        for (var i = 0; i < passengers.length; i++) {
            passengers[i].onPublicTransportVehicleEntered(this);
        }
    }

    private notifyCurrentPassengers(station: Station) {
        this.onStationReachedEvent.notifyListeners(station);
    }

    public getRemainingCapacity(): number {
        return this.vehicleInstance.getData().getVehicle().getCapacity() - this.currentPassengers.length;
    }

    public getCurrentPassengers(): number {
        return this.currentPassengers.length;
    }

    public addOnStationReachedListener(listener: (station: Station) => void) {
        this.onStationReachedEvent.addListener(listener);
    }

    public removeOnStationReachedListener(listener: (station: Station) => void) {
        this.onStationReachedEvent.removeListener(listener);
    }

}