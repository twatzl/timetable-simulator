class LineVehicle extends VehicleInstance {

	private lineWaypointIterator: LineWaypointIterator;
	
	constructor(vehicle: Vehicle, private line: Line) {
		super(vehicle, line.data.getName(), new LineWaypointIterator(line));
		this.line = line;
	}

	public getJSONString(): string {
		return JSON.stringify(this, (key, value) => {
			switch(key) {
				case "line":
					return value.id;
				case "lineWaypointIterator":
					return;
				default:
					return value;
			}
		});
	}
}