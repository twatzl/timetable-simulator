class CourseVehicle extends VehicleInstance {

	constructor(vehicle: Vehicle, private course: Kurs) {
		super(vehicle, course.getTimetable().getLine().data.getName(), new CourseWaypointIterator(course));
		this.getCourseWaypointIterator().addOnCourseFinishedListener(this.getOnCourseFinishedListener());
	}

	protected getCourseWaypointIterator(): CourseWaypointIterator {
		return <CourseWaypointIterator>super.getWaypointIterator();
	}

	protected onWaypointReached(): void {
		var departureTime = this.getCourseWaypointIterator().getDepartureTimeForCurrentStation();
		if (departureTime != null) {
			this.stopUntil(departureTime);
		}
		super.onWaypointReached();
	}

	public getOnCourseFinishedListener(): () => void {
		return () => { VehicleInstanceRepository.getInstance().deleteObjectById(this.id); }
	}

	public getJSONString(): string {
		return JSON.stringify(this, function(key, value) {
			switch (key) {
				case "course":
					return value.id;
				default:
					return value;
			}
		});
	}

}