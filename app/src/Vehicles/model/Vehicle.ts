///<reference path="VehicleRepository.ts" />

class Vehicle extends EntityObject {
	
	private onDeletedEvent: ObservableEvent<RepositoryEventArgs<Vehicle>>;

	private Name : string;
	public getName() : string {
		return this.Name;
	}
	public setName(v : string) {
		this.Name = v;
	}
	
	private OperatingPrice: number;
	
	public getOperatingPrice(): number {
		return this.OperatingPrice;
	}
	
	public setOperatingPrice(operatingPrice: number): void {
		this.OperatingPrice = operatingPrice;
	}
	
	private Capacity : number;
	public getCapacity() : number {
		return this.Capacity;
	}
	public setCapacity(v : number) {
		this.Capacity = v;
	}
	
	private MaxSpeed : number;
	public getMaxSpeed() : number {
		return this.MaxSpeed;
	}
	public setMaxSpeed(v : number) {
		this.MaxSpeed = v;
	}
	
	private Acceleration : number;
	public getAcceleration() : number {
		return this.Acceleration;
	}
	public setAcceleration(v : number) {
		this.Acceleration = v;
	}
	
	private Length : number;
	public getLength() : number {
		return this.Length;
	}
	public setLength(v : number) {
		this.Length = v;
	}
	
	private Type : string;
	public getType() : string {
		return this.Type;
	}
	public setType(v : string) {
		this.Type = v;
	}
	
	constructor(jsonData?: any){
		super(VehicleRepository.getInstance().getNextId());
		this.onDeletedEvent = new ObservableEvent<RepositoryEventArgs<Vehicle>>();
		if (jsonData != undefined) {
			this.parseFromJson(jsonData);
		}
	}
	
	public parseFromJson(jsonData: any): void {
		this.Name = jsonData.Name;
		this.Capacity = jsonData.Capacity;
		this.MaxSpeed = jsonData.MaxSpeed;
		this.Acceleration = jsonData.Acceleration;
		this.OperatingPrice = jsonData.OperatingPrice;
		this.Length = jsonData.Length;
		this.Type = jsonData.Type;
		this.id = jsonData.id;
	}
	
	public addOnDeletedListener(listener: (eventArgs: RepositoryEventArgs<Vehicle>) => void) {
		this.onDeletedEvent.addListener(listener);
	}
	
	public removeOnDeletedListener(listener: (eventArgs: RepositoryEventArgs<Vehicle>) => void) {
		this.onDeletedEvent.removeListener(listener);
	}
	
	public deleted(): void {
		VehicleInstanceRepository.getInstance().deleteAllInstancesUsingVehicle(this);
		this.onDeletedEvent.notifyListeners(new RepositoryEventArgs(this.id, this));
	}
	
	public getJSONString(): string {
		return JSON.stringify(this);
	}
}
