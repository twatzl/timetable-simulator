abstract class VehicleInstance extends MovableObject implements DrawableMapObject {

    private static stationRadius: number = 15;
    // the radius around a station that is part of the station

    private marker: google.maps.Circle;
    private simulation: VehicleSimulation;
    private onStationReachedEvent: ObservableEvent<Station>;
    private infoWindow: google.maps.InfoWindow;

    constructor(vehicle: Vehicle, lineName: string, waypointIterator: WaypointIterator) {
		super(VehicleInstanceRepository.getInstance().getNextId(), new VehicleInstanceData(
			waypointIterator.next().getLocation(),
			vehicle,
			false
		), waypointIterator);
		this.onStationReachedEvent = new ObservableEvent<Station>();
		this.simulation = new VehicleSimulation(this, lineName);
		this.addOnWaypointReachedListener((waypoint: Waypoint) => this.onWaypointReachedEventNotification(waypoint));
		VehicleInstanceRepository.getInstance().addObject(this);
		VehicleRepository.getInstance().addOnObjectRemovedListener((eventArgs: RepositoryEventArgs<Vehicle>) => this.vehicleDeletedListener(eventArgs));

		if (VehicleInstanceRepository.getInstance().getIsShowInfoWindowActive()) {
			this.openInfoWindow(MapService.getMap());
		}
		if (vehicle == undefined || vehicle == null) {
			VehicleInstanceRepository.getInstance().deleteObjectById(this.id);
		}
    }

    public getData(): VehicleInstanceData {
        return <VehicleInstanceData>super.getData();
    }

    public getSimulation(): VehicleSimulation {
        return this.simulation;
    }

    protected init(): void {
        // TODO: extract to configuration
        this.marker = new google.maps.Circle({
            strokeColor: '#D62700',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#D62700',
            fillOpacity: 0.35,
            radius: 10,
            center: this.getData().getCurrentLocation()
        });
        this.infoWindow = new google.maps.InfoWindow();
        this.show(MapService.getMap());

        FastClock.getInstance().addOnNextMinuteEventListener(this.initializeFinanceListener());
    }


    private setInfoWindowValues(vehicleInstance: VehicleInstance, map: google.maps.Map, newInfoWindowContent: string): void {
        vehicleInstance.infoWindow.setContent(newInfoWindowContent);
        vehicleInstance.infoWindow.setPosition(this.marker.getCenter());
    }

    private initializeFinanceListener(): (eventArgs: FastClockEventArgs) => void {
        return (eventArgs: FastClockEventArgs): void => {
			const vehicle = this.getData().getVehicle();
			if (vehicle == undefined) return;
			let price = Finance.getInstance().getFinanceData().calculateVehicleOperationPrice(vehicle);
            vehicle.setOperatingPrice(price);
			const transaction = new Transaction(TransactionTypes.PURCHASE, price);
            const status = Finance.getInstance().handleTransaction(transaction, false);
            if (status == false) {
				this.stop();
			}
        }
    }

    private vehicleDeletedListener(eventArgs: RepositoryEventArgs<Vehicle>) : void {
		// call function to make it possible for inhereting classes to override/extend
		const vehicle = this.getData().getVehicle();
		if (vehicle == undefined || eventArgs.getId() == vehicle.getId()) {
			this.onVehicleDeleted(this, vehicle);
		}
    }

    private onWaypointReachedEventNotification(waypoint: Waypoint) {
        if (waypoint instanceof Station) {
            this.onStationReachedEvent.notifyListeners(waypoint);
        }
    }

    protected onVehicleDeleted(vehicleInstance: VehicleInstance, vehicle: Vehicle): void {
		// Why is vehicle also passed? Not needed
		if (vehicleInstance != undefined) {
			VehicleInstanceRepository.getInstance().deleteObjectById(vehicleInstance.id);
		}
    }

    protected getWaypointReachedRadius(): number {
        return VehicleInstance.stationRadius;
    }

    protected afterSimulationStep(): void {
        this.marker.setCenter(this.getData().getCurrentLocation());
    }

    public addOnStationReachedListener(listener: (station: Station) => void) {
        this.onStationReachedEvent.addListener(listener);
    }

    public removeOnStationReachedListener(listener: (station: Station) => void) {
        this.onStationReachedEvent.removeListener(listener);
    }

    public isShown(): boolean {
        return this.isShownOnMap;
    }

    public show(map: google.maps.Map): void {
        this.marker.setMap(map);
        this.isShownOnMap = true;
        this.infoWindow.bindTo("position", this.marker, "center");
    }

    public hide(): void {
        this.marker.setMap(null);
        this.isShownOnMap = false;
    }

    public deleted(): void {
        this.hide();
        super.deleted();
    }

    private getVehicleInfoWindowContent(): string {
        var infoWindowContent =
            "<h3 style='color:#4285F4'>" + this.getData().getVehicle().getName() + "</h3>" +
            "<p>People in Vehicle: " + this.getSimulation().getCurrentPassengers() + " of " + this.getData().getVehicle().getCapacity() + "</p>"
        return infoWindowContent;
    }

    public closeInfoWindow(): void {
        this.infoWindow.close();
    }

    public openInfoWindow(map: google.maps.Map): void {
        this.setInfoWindowValues(this, MapService.getMap(), this.getVehicleInfoWindowContent());
        this.infoWindow.open(map);
    }
}