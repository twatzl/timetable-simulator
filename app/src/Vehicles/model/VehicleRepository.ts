class VehicleRepository extends Repository<Vehicle> {

	private static instance: VehicleRepository;

	constructor() {
		super();
	}

	public static getInstance() {
		if (this.instance === undefined) {
			this.instance = new VehicleRepository();
		}
		return this.instance;
	}

}
