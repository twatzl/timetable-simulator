class VehicleInstanceRepository extends Repository<VehicleInstance> {

    private static instance: VehicleInstanceRepository;
    private showInfoWindow: boolean;

    constructor() {
        super();
        this.showInfoWindow = false;
    }

    public static getInstance(): VehicleInstanceRepository {
        if (this.instance === undefined) {
            this.instance = new VehicleInstanceRepository();
        }
        return this.instance;
    }

    private iterateVehicles(callback: (object: VehicleInstance) => void): void {
        for (var key in this.getObjectList()) {
            var vehicle = this.getObjectById(key);
            if (this.isValidVehicle(vehicle)) {
                callback(this.getObjectById(key));
            }
        }
    }

    private iterateVehiclesWithMap(callback: (object: VehicleInstance, map: google.maps.Map) => void, map: google.maps.Map): void {
        for (var key in this.getObjectList()) {
            var vehicle = this.getObjectById(key);
            if (this.isValidVehicle(vehicle)) {
                callback(this.getObjectById(key), map);
            }
        }
    }

    private isValidVehicle(vehicle: VehicleInstance): boolean {
        return vehicle != undefined;
    }

    public startSimulationAndShowAllVehicles(map: google.maps.Map) {
        var repository = this;
        this.iterateVehiclesWithMap(
            function(vehicle: VehicleInstance, map: google.maps.Map) {
                repository.startVehicle(vehicle);
                if (!vehicle.isShown()) {
                    repository.showVehicle(vehicle, map);
                }
            }, map
        )
    }

    private showVehicle(vehicle: VehicleInstance, map: google.maps.Map) {
        if (!vehicle.isShown()) {
            vehicle.show(map)
        }
    }

    public showVehicles(map: google.maps.Map): void {
        this.iterateVehiclesWithMap(this.showVehicle, map);
    }

    private hideVehicle(vehicle: VehicleInstance) { vehicle.hide() }

    public hideVehicles(): void {
        this.iterateVehicles(this.hideVehicle);
    }

    private startVehicle(vehicle: VehicleInstance) { vehicle.start() }

    public startSimulation(): void {
        this.iterateVehicles(this.startVehicle);
    }

    private stopVehicle(vehicle: VehicleInstance) { vehicle.stop() }

    public stopSimulation() {
        this.iterateVehicles(this.stopVehicle);
    }

    public showVehicleInfoWindows(map: google.maps.Map): void {
        for (var key in this.getObjectList()) {
            var vehicle = this.getObjectById(key);
            if (this.isValidVehicle(vehicle)) {
                vehicle.openInfoWindow(map);
            }
        }

        this.showInfoWindow = true;
    }

    public hideVehicleInfoWindows(): void {
        for (var key in this.getObjectList()) {
            var vehicle = this.getObjectById(key);
            if (this.isValidVehicle(vehicle)) {
                vehicle.closeInfoWindow();
            }
        }

        this.showInfoWindow = false;
    }

    public getIsShowInfoWindowActive(): boolean {
        return this.showInfoWindow;
    }
	
	public deleteAllInstancesUsingVehicle(vehicle: Vehicle): void {
		let vehicleInstances = this.getObjectList();
		for (const key in vehicleInstances) {
			let vehicleInstance = vehicleInstances[key];
			if (vehicleInstance.getData().getVehicle().id == vehicle.id) {
				this.deleteObjectById(vehicleInstance.id);
			}
		}
	}
}
