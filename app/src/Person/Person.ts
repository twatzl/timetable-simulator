class Person extends EntityObject {

	private personRendering: PersonRendering;
	private personRouter: PersonRouter;
	private data: PersonData;
	private onWayCalculationFinished: CountingEvent;
	private currentPublicTransportRouteElement: PublicTransportRouteElement
	private map: google.maps.Map;

	private static ROUTE_ALTERNATIVES_COUNT = 2;

	constructor(startLocation: ILocation, destinationLocation: ILocation, jsonData?: any) {
		if (jsonData == undefined) {
			super(PersonRepository.getInstance().getNextId());
			this.initData(startLocation, destinationLocation);
			this.onWayCalculationFinished = new CountingEvent(Person.ROUTE_ALTERNATIVES_COUNT);
		} else {
			this.parseFromJson(jsonData);
		}
	}

	public getJSONString(): string {
		return JSON.stringify(this);
	}

	public deleted(): void {
		this.personRendering.hide();
	}

	private initData(startLocation: ILocation, destinationLocation: ILocation): void {
		this.data = new PersonData();
		this.data.setCurrentLocation(startLocation);
		this.data.setDestinationLocation(destinationLocation);
	}

	private createNewPersonRenderingAtCurrentLocation(route: Waypoint[]) {
		var startLatLon = GoogleLocation.fromLocation(this.data.getCurrentLocation()).getLatLng();
		this.personRendering = new PersonRendering(this.getId(),
			new PersonRenderingData(
				GoogleLocation.fromLocation(this.data.getCurrentLocation()).getLatLng()
				, false),
			route);

		if (this.map != null && this.map != undefined) {
			this.personRendering.show(this.map);
		}
	}

	public startSimulation(): void {
		let router: PersonRouter = PersonRouter.getInstance();
		let person: Person = this;

		router.calculateCarRoute(this.data.getCurrentLocation(), this.data.getDestinationLocation(),
			(directionsResult: google.maps.DirectionsResult) => {
				let route: Waypoint[] = GeoUtilities.getWaypointsFromDirectionsResult(directionsResult);
				person.data.setCarRoute(route);
				person.onWayCalculationFinished.notifyListeners(null);
			});

		router.calculatePublicTransportRoute(this.data.getCurrentLocation(), this.data.getDestinationLocation(),
			(publicTransportRoute: PersonPublicTransportRoute) => {
				person.data.setPublicTransportRoute(publicTransportRoute);
				person.onWayCalculationFinished.notifyListeners(null);
			});
	}

	private onRoutesCalculated() {
		let router = PersonRouter.getInstance();
		let carTime = router.calculateTravelTimeByCar(this.data.getCarRoute());
		let publicTransportTime = Number.POSITIVE_INFINITY;

		if (this.data.getPublicTransportRoute() != null) {
			publicTransportTime = router.calculateTravelTimeByPublicTransport(this.data.getPublicTransportRoute());
		}

		if (carTime < 0.75 * publicTransportTime) {
			this.startCarRouting();
		} else {
			this.startPublicTransportRouting()
		}
	}

	private startCarRouting() {
		this.createNewPersonRenderingAtCurrentLocation(this.data.getCarRoute());
		this.personRendering.show(this.map);
		this.personRendering.start();
	}

	private startPublicTransportRouting() {
		this.navigateToFirstStation();
		this.personRendering.start();
	}

	private navigateToFirstStation() {
		var routeToStation = this.data.getPublicTransportRoute().getRouteToFirstStation();
		this.createNewPersonRenderingAtCurrentLocation(routeToStation);
		this.personRendering.show(this.map);
		this.personRendering.addOnRouteFinishedListener(() => this.onEnteringStationReached());
	}

	private navigateFromLastStation() {
		var routeFromStation = this.data.getPublicTransportRoute().getRouteFromLastStation();
		this.createNewPersonRenderingAtCurrentLocation(routeFromStation);
		this.personRendering.show(this.map);
		this.personRendering.addOnRouteFinishedListener(() => PersonRepository.getInstance().deleteObjectById(this.getId()));
		this.personRendering.show(this.map);
	}

	private getNextPartOfPublicTransportRoute() {
		var route = this.data.getPublicTransportRoute().getRouteWithPublicTransportSystems();
		this.currentPublicTransportRouteElement = route.popRouteElement();
	}

	private getNextPartOfRouteAndFollowRoute() {
		this.getNextPartOfPublicTransportRoute();
		if (this.currentPublicTransportRouteElement == null) {
			this.navigateFromLastStation();
		} else {
			this.followPublicTransportRoute(this.currentPublicTransportRouteElement);
		}
	}

	private followPublicTransportRoute(routeElement: PublicTransportRouteElement) {
		var station = routeElement.getStartStation();
		station.getSimulation().addWaitingPerson(routeElement.getLine().data.getName(), this);
	}

	public onPublicTransportVehicleEntered(vehicleSimulation: VehicleSimulation) {
		vehicleSimulation.addOnStationReachedListener((station: Station) => this.onStationReachedHandler(station));
		this.personRendering.hide();
	}

	private onStationReachedHandler(station: Station) {
		if (station.getId() == this.currentPublicTransportRouteElement.getStartStation().getId()) {
			this.onLeavingStationReached();
		}
	}

	/**
	 * Handler for when the person reaches the station where it enters
	 * public transport vehicles.
	 */
	private onEnteringStationReached() {
		this.getNextPartOfRouteAndFollowRoute()
	}

	/**
	 * Handler for when the vehicle with the person reaches the 
	 * destination station.
	 */
	private onLeavingStationReached() {
		this.personRendering.show(this.map);
		this.getNextPartOfRouteAndFollowRoute();
		// implement the stuff here (decide whether to get on the next vehicle)
		// or to use the car to get to the destination
	}

	public show(map: google.maps.Map): void {
		this.map = map;
		if (this.personRendering != undefined) {
			this.personRendering.show(this.map);
		}
	}

	public hide(): void {
		this.personRendering.hide();
		this.map = null;
	}

	public parseFromJson(jsonData: any): void {
		this.currentPublicTransportRouteElement = jsonData.currentPublicTransportRouteElement;
		this.data = jsonData.data;
		this.id = jsonData.id;
		this.onWayCalculationFinished = jsonData.onWayCalculationFinished;
		this.personRendering = jsonData.personRendering;
		this.personRouter = jsonData.personRouter;
	}
}