class PersonRepository extends Repository<Person> {
	
	private static instance: PersonRepository;

	constructor() {
		super();
	}

	public static getInstance() {
		if (this.instance === undefined) {
			this.instance = new PersonRepository();
		}
		return this.instance;
	}
	
}