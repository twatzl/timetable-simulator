class PersonRendering extends MovableObject implements DrawableMapObject {
	
	// radius of waypoint in which it counts as reached [m]
	private static waypointReachedRadius: number = 15;
	circle: google.maps.Circle;
	private onRouteFinishedEvent: ObservableEvent<void>;
	
	constructor(id: number, route: Waypoint[], personRenderingData: PersonRenderingData, jsonData?: any) {
		if (jsonData == undefined) {
			super(id, personRenderingData, new SimpleWaypointIterator(route));
			this.onRouteFinishedEvent = new ObservableEvent<void>();
			this.getSimpleWaypointIterator().addOnLastWapointReachedListener(() => this.onRouteFinishedEvent.notifyListeners(null));
		} else {
			this.parseFromJson(jsonData);
		}
	}
	
	protected init(): void {
		this.initializeCircle();
	}
	
	protected getWaypointReachedRadius(): number {
		return PersonRendering.waypointReachedRadius;
	}
	
	protected getSimpleWaypointIterator(): SimpleWaypointIterator {
		return <SimpleWaypointIterator> super.getWaypointIterator();
	}
	
	private initializeCircle(): void {
		this.circle = new google.maps.Circle({
			fillColor: Configuration.PERSON_FILL_COLOR,
			fillOpacity: Configuration.PERSON_FILL_OPACITY,
			strokeColor: Configuration.PERSON_STROKE_COLOR,
			strokeOpacity: Configuration.PERSON_STROKE_OPACITY,
			strokeWeight: Configuration.PERSON_STROKE_WEIGHT,
			draggable: false,
			center: this.getData().getCurrentLocation()
		});
	}
	
	protected afterSimulationStep(): void {
		this.circle.setCenter(this.getData().getCurrentLocation());
	}
	
	public addOnRouteFinishedListener(listener: () => void) {
		this.onRouteFinishedEvent.removeListener(listener);
	}
	
	public removeOnRouteFinishedListener(listener: () => void) {
		this.onRouteFinishedEvent.removeListener(listener);
	}
	
	public show(map: google.maps.Map) {
		this.circle.setMap(map);
	}
	
	public hide() {
		this.circle.setMap(null);
	}
	
	public getJSONString(): string {
		return JSON.stringify(this);
	}
	
	public parseFromJson(jsonData: any): void {
		this.circle = jsonData.circle;
		this.id = jsonData.id;
		this.isShownOnMap = jsonData.isShownOnMap;
	}
}