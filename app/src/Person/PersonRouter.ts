class PersonRouter {

	private static instance: PersonRouter;

	private directionsService: google.maps.DirectionsService;
	private directionsRenderer: google.maps.DirectionsRenderer;
	private publicTransportRouter: PublicTransportRouter;

	public static getInstance(): PersonRouter {
		if (PersonRouter.instance == undefined) {
			PersonRouter.instance = new PersonRouter();
		}
		return PersonRouter.instance;
	}

	constructor() {
		this.directionsService = new google.maps.DirectionsService();
		this.directionsRenderer = new google.maps.DirectionsRenderer();
		this.publicTransportRouter = new PublicTransportRouter(
			WaypointRepository.getInstance().getAllStations(),
			LineRepository.getInstance().getAllLines()
		);
	}
	
	public calculateTravelTimeByPublicTransport(route: PersonPublicTransportRoute): number {
		return route.getTimeEstimation();
	}
	
	public calculateTravelTimeByCar(route: Waypoint[]): number {
		var distance = GeoUtilities.calculateDistanceOfRoute(route);
		return distance / Configuration.DEFAULT_CAR_SPEED;
	}
	
	public calculateCarRoute(
		start: ILocation,
		end: ILocation, 
		callback: (route: google.maps.DirectionsResult) => void) : void {
		
		var directionsRequest = this.getDirectionsRequest(start, end);
		
	}
	
	private getCarGoogleCallback(callback: (route: google.maps.DirectionsResult) => void) {
		return function(result: google.maps.DirectionsResult, status: google.maps.DirectionsStatus) {
			if (status == google.maps.DirectionsStatus.OK) {
				callback(result);
			}
		}
	}
	
	public calculatePublicTransportRoute(
		start: ILocation,
		end: ILocation,
		callback: (route: PersonPublicTransportRoute) => void): void {
			
		var route = new PersonPublicTransportRoute();
		route.addOnAllDataSetListener(this.getPublicTransportRouteDataSetCallback(callback));
		
		var startStation: Station = this.publicTransportRouter.getNearestStation(start);
		var endStation: Station = this.publicTransportRouter.getNearestStation(end);
		var publicTransportRoute = this.publicTransportRouter.calculateFastestRoute(startStation, endStation);
		
		if (publicTransportRoute == null) {
			callback(null);
			return;
		}
		
		route.setRouteWithPublicTransportSystems(publicTransportRoute);
		
		this.sendDirectionsRequest(
			start,
			new GoogleLocation(startStation.getLocation()),
			this.getPublicTransportGoogleCallback(route.setRouteToFirstStation));
		this.sendDirectionsRequest(
			new GoogleLocation(endStation.getLocation()),
			end,
			this.getPublicTransportGoogleCallback(route.setRouteFromLastStation));
	}
	
	private getPublicTransportRouteDataSetCallback(callback: (route: PersonPublicTransportRoute) => void) : (eventArgs: PersonPublicTransportRoute) => void {
		return function(eventArgs: PersonPublicTransportRoute) {
			callback(eventArgs);
		}
	}
	
	private getPublicTransportGoogleCallback(setterMethod: (route: Waypoint[]) => void ): 
	(result: google.maps.DirectionsResult, status: google.maps.DirectionsStatus) => void { 
		return function(result: google.maps.DirectionsResult, status: google.maps.DirectionsStatus) {
			if (status == google.maps.DirectionsStatus.OK) {
				setterMethod(GeoUtilities.getWaypointsFromDirectionsResult(result));
			}
		}
	}

	private sendDirectionsRequest(start: ILocation, end: ILocation, callback: (result: google.maps.DirectionsResult, status: google.maps.DirectionsStatus) => void) {
		var directionsRequest = this.getDirectionsRequest(start, end);
		this.directionsService.route(directionsRequest, callback);
	}
	
	private getDirectionsRequest(start: ILocation, end: ILocation): google.maps.DirectionsRequest {
		var startLatLon = GoogleLocation.fromLocation(start).getLatLng();
		var endLatLon = GoogleLocation.fromLocation(end).getLatLng();
		
		return {
			origin: startLatLon,
			destination: endLatLon,
			travelMode: google.maps.TravelMode.DRIVING,
			optimizeWaypoints: false,
			avoidHighways: false,
			avoidTolls: false
		};
	}

}