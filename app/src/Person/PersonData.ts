class PersonData {

	private currentLocation : ILocation;
	public getCurrentLocation() : ILocation {
		return this.currentLocation;
	}
	public setCurrentLocation(v : ILocation) {
		this.currentLocation = v;
	}
	
	private destinationLocation : ILocation;
	public getDestinationLocation() : ILocation {
		return this.destinationLocation;
	}
	public setDestinationLocation(v : ILocation) {
		this.destinationLocation = v;
	}
	
	private carRoute: Waypoint[];
	public getCarRoute(): Waypoint[] { 
		return this.carRoute;
	}
	public setCarRoute(v: Waypoint[]) {
		this.carRoute = v;
	}
	
	private publicTransportRoute: PersonPublicTransportRoute;
	public getPublicTransportRoute(): PersonPublicTransportRoute {
		return this.publicTransportRoute;
	}
	public setPublicTransportRoute(v: PersonPublicTransportRoute) {
		this.publicTransportRoute = v;
	}
	
}