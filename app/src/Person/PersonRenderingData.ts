class PersonRenderingData extends MovableObjectData {
	
	public getAcceleration(): number {
		return 1;
	}
	
	public getDeceleration(): number {
		return 1
	}
	
	public getMaxSpeed(): number {
		return 15;
	}
	
}