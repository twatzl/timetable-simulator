/**
 * This class is a representation of the route a
 * person will follow when using the public transport
 * systems to reach a target zone.
 */
class PersonPublicTransportRoute {

	private routeToFirstStation: Waypoint[];
	private routeFromLastStation: Waypoint[];
	private routeWithPublicTransportSystems: PublicTransportRoute;
	private onAllDataSet: ObservableEvent<PersonPublicTransportRoute>;

	constructor() {
		this.onAllDataSet = new ObservableEvent<PersonPublicTransportRoute>();
	}

	public getRouteToFirstStation(): Waypoint[] {
		return this.routeToFirstStation;
	}

	public setRouteToFirstStation(route: Waypoint[]): void {
		this.routeToFirstStation = route;
		this.checkAllDataSet();
	}

	public getRouteFromLastStation(): Waypoint[] {
		return this.routeFromLastStation;
	}

	public setRouteFromLastStation(route: Waypoint[]): void {
		this.routeFromLastStation = route;
		this.checkAllDataSet();
	}

	public getRouteWithPublicTransportSystems(): PublicTransportRoute {
		return this.routeWithPublicTransportSystems;
	}

	public setRouteWithPublicTransportSystems(route: PublicTransportRoute): void {
		this.routeWithPublicTransportSystems = route;
		this.checkAllDataSet();
	}
	
	public getDistance(): number { 
		var distance = 0;
		distance += GeoUtilities.calculateDistanceOfRoute(this.getRouteToFirstStation());
		distance += GeoUtilities.calculateDistanceOfRoute(this.getRouteToFirstStation());
		distance += this.getRouteWithPublicTransportSystems().getDistance();
		return distance;
	}
	
	public getTimeEstimation(): number {
		return this.getDistance() / Configuration.DEFAULT_PUBLIC_TRANSPORT_SPEED;
	}

	private checkAllDataSet(): void {
		if (this.routeToFirstStation != undefined && this.routeToFirstStation != []
			&& this.routeFromLastStation != undefined && this.routeFromLastStation != []
			&& this.routeWithPublicTransportSystems != undefined) {
			this.onAllDataSet.notifyListeners(this);
		}
	}

	public addOnAllDataSetListener(listener: (eventArgs: PersonPublicTransportRoute) => void): void {
		this.onAllDataSet.addListener(listener);
	}

	public removeOnAllDataSetListener(listener: (eventArgs: PersonPublicTransportRoute) => void): void {
		this.onAllDataSet.removeListener(listener);
	}

}