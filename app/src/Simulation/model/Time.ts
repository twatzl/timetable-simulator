class Time {
	private hours: number;
	private minutes: number;
	private seconds: number;

	constructor(time?: Time) {
		if (time !== undefined) {
			this.setHours(time.getHours());
			this.setMinutes(time.getMinutes());
			this.setSeconds(time.getSeconds());
		}
	}

	public getHours(): number {
		return this.hours;
	}

	public setHours(v: number) {
		this.hours = v;
	}

	public getMinutes(): number {
		return this.minutes;
	}

	public setMinutes(v: number) {
		this.minutes = v;
	}

	public getSeconds(): number {
		return this.seconds;
	}

	public setSeconds(v: number) {
		this.seconds = v;
	}

	public convertMillisecondsToTime(milliseconds: number): void {
		this.hours = Math.floor(milliseconds / (1000 * 60 * 60));
		var rest = milliseconds % (1000 * 60 * 60);
		this.minutes = Math.floor(rest / (1000 * 60));
		rest = rest % (1000 * 60);
		this.seconds = Math.floor(rest / (1000));
	}

	public convertTimeToMilliseconds(): number {
		return (this.hours * 60 * 60 * 1000) + (this.minutes * 60 * 1000) + (this.seconds * 1000);
	}

	public convertTimeToDate(): Date {
		return new Date(0, 0, 0, this.hours, this.minutes, this.seconds, 0);
	}

	public convertDateToTime(date: Date): void {
		this.hours = date.getHours();
		this.minutes = date.getMinutes();
		this.seconds = date.getSeconds();
	}

	public addMinutes(minutes: number): void {
		let additionalHours = minutes / 60;
		let additionalMinutes = minutes % 60;
		if (additionalHours >= 1) {
			this.setHours(this.getHours() + additionalHours);
		}
		if (additionalMinutes >= 1) {
			if (this.getMinutes() + additionalMinutes >= 60) {
				this.setHours(this.getHours()+1);
				additionalMinutes = (this.getMinutes()+additionalMinutes) - 60;
				this.setMinutes(additionalMinutes);
			} else {
				this.setMinutes(this.getMinutes() + additionalMinutes);	
			}
		}
	}

	public addSeconds(seconds: number): void {
		const additionalHours = seconds / 3600;
		let additionalMinutes = seconds % 3600 / 60;
		let additionalSeconds = seconds % 60;
		if (additionalHours >= 1) {
			this.setHours(this.getHours() + additionalHours);
		}
		if (additionalMinutes >= 1) {
			if (this.getMinutes() + additionalMinutes >= 60) {
				this.setHours(this.getHours()+1);
				additionalMinutes = (this.getMinutes()+additionalMinutes) - 60;
				this.setMinutes(additionalMinutes);
			} else {
				this.setMinutes(this.getMinutes() + additionalMinutes);	
			}
		}
		if (additionalSeconds >= 1) {
			if (this.getSeconds() + additionalSeconds >= 60) {
				this.setMinutes(this.getMinutes()+1);
				additionalSeconds = (this.getSeconds()+additionalSeconds) - 60;
				this.setSeconds(additionalSeconds);
			} else {
				this.setSeconds(this.getSeconds() + additionalSeconds);	
			}
			this.setSeconds(this.getSeconds() + additionalSeconds);
		}
	}

	public addHours(hours: number): void {
		if (hours >= 1) {
			this.setHours(this.getHours() + hours);
		}
	}

	public addTime(time: Time): void {
		this.addHours(time.getHours());
		this.addMinutes(time.getMinutes());
		this.addSeconds(time.getSeconds());
	}

	public subtractMinutes(minutes: number): void {
		const additionalHours = minutes / 60;
		const additionalMinutes = minutes % 60;
		if (additionalHours >= 1) {
			this.setHours(this.getHours() - additionalHours);
		}
		if (additionalMinutes >= 1) {
			this.setMinutes(this.getMinutes() - additionalMinutes);
		}
	}

	public subtractSeconds(seconds: number): void {
		const additionalHours = seconds / 3600;
		const additionalMinutes = seconds % 3600 / 60;
		const additionalSeconds = seconds % 60;
		if (additionalHours >= 1) {
			this.setHours(this.getHours() + additionalHours);
		}
		if (additionalMinutes >= 1) {
			this.setMinutes(this.getMinutes() - additionalMinutes);
		}
		if (additionalSeconds >= 2) {
			this.setMinutes(this.getMinutes() - additionalSeconds);
		}
	}

	public subtractHours(hours: number): void {
		if (hours >= 1) {
			this.setHours(this.getHours() - hours);
		}
	}

	public subtractTime(time: Time): void {
		this.subtractHours(time.getHours());
		this.subtractMinutes(time.getMinutes());
		this.subtractSeconds(time.getSeconds());
	}
	
	public toString(): string {
		return ("00" + this.hours).slice(-2) + ":" + ("00" + this.minutes).slice(-2) + ":" + ("00" + this.seconds).slice(-2);
	}
	
	
	// return value > 0 => this > other
	// return value < 0 => this < other
	// return value == 0 => this == other 
	public compareTo(otherTime: Time): number {
		const hours = this.getHours();
		const minutes = this.getMinutes();
		const seconds = this.getSeconds();
		
		const otherHours = otherTime.getHours();
		const otherMinutes = otherTime.getMinutes();
		const otherSeconds = otherTime.getSeconds();
		
		const comparedHours = Time.compare(hours, otherHours);
		const comparedMinutes = Time.compare(minutes, otherMinutes);
		const comparedSeconds = Time.compare(seconds, otherSeconds);
		
		if (comparedHours > 0) return 1;
		if (comparedHours < 0) return -1;
		if (comparedMinutes > 0) return 1;
		if (comparedMinutes < 0) return -1;
		if (comparedSeconds > 0) return 1;
		if (comparedSeconds < 0) return -1;
		
		return 0;
	}
	
	public static compare(value1: number, value2: number): number {
		if (value1 > value2) return +1;
		if (value1 < value2) return -1;
		return 0;
	}
}