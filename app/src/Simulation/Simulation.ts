class Simulation {
	
	public static startSimulation() {
		var zoneDictionary = ZoneRepository.getInstance().getObjectList();
		var zones: Zone[] = [];
		for (var key in zoneDictionary) {
			zones.push(zoneDictionary[key]);
		}
		
		const se: SimulationEquations = new DefaultSimulationEquations();
		const zds: ZoneDemandSimulation = new ZoneDemandSimulation(se);
		const zs: ZoneSimulation = new ZoneSimulation(zones,zds);
		const ps: PersonSimulation = new PersonSimulation(zs);
			
		ps.startSimulation();
	}
	
}