var date;
var isPaused;
var interval;
var globalFactor;

self.addEventListener('message', function (eventArgs) {

	if (eventArgs.data.state == 'start') {
		isPaused = false;
		date = eventArgs.data.date;
		setFactor(eventArgs.data.factor);
    } else if (eventArgs.data.state == 'pause') {
		setFactor(0);
		isPaused = true;
	} else if (eventArgs.data.state == 'resume') {
		setFactor(globalFactor);
		isPaused = false;
	} else if (eventArgs.data.state == 'setFactor') {
		setFactor(eventArgs.data.factor);
	}

}, false);

function loopClock() {
	date = new Date(date.getTime() + 1000);
	postMessage(date);
}

function setFactor(factor) {
	globalFactor = factor;
	clearInterval(interval);
	if (factor > 0) {
		interval = setInterval(function () { loopClock() }, 1000 / factor);
	}
}