class FastClockEventArgs {
    public currentTime: Date;
}

class FastClock {
    private static instance: FastClock;
    private worker: any;
    private currentTime: Date;

    private nextSecondEvent: ObservableEvent<FastClockEventArgs>;
    private nextMinuteEvent: ObservableEvent<FastClockEventArgs>;
    private nextHourEvent: ObservableEvent<FastClockEventArgs>;
    private nextDayEvent: ObservableEvent<FastClockEventArgs>;

    private callbackDictionary: { [stringDate: string]: Array<any>; } = {};

    constructor() {
        this.nextSecondEvent = new ObservableEvent<FastClockEventArgs>();
        this.nextMinuteEvent = new ObservableEvent<FastClockEventArgs>();
        this.nextHourEvent = new ObservableEvent<FastClockEventArgs>();
        this.nextDayEvent = new ObservableEvent<FastClockEventArgs>();

        this.start(new Date());
    }

    public static getInstance() {
        if (this.instance === undefined) {
            this.instance = new FastClock();
        }
        return this.instance;
    }

    public getTime(): Date {
        return new Date(this.currentTime.toString());
    }

    public start(date: Date): void {
        this.setTime(date);

        if (typeof (this.worker) == "undefined") {
            this.worker = new Worker("js/Simulation/FastClockSimulation.js");
        }

        var fastClockElementsValues = { state: 'start', factor: 1, date: this.getTime() };
        this.worker.postMessage(fastClockElementsValues);

        this.worker.addEventListener('message', function(e: any) {
            FastClock.getInstance().setTime(e.data);
            var eventArgs = new FastClockEventArgs();
            eventArgs.currentTime = FastClock.getInstance().getTime();

            FastClock.getInstance().nextSecondEvent.notifyListeners(eventArgs);

            if (FastClock.getInstance().getTime().getSeconds() == 0) {
                FastClock.getInstance().nextMinuteEvent.notifyListeners(eventArgs);
            }

            if (FastClock.getInstance().getTime().getMinutes() == 0) {
                FastClock.getInstance().nextHourEvent.notifyListeners(eventArgs);
            }

            if (FastClock.getInstance().getTime().getHours() == 0) {
                FastClock.getInstance().nextDayEvent.notifyListeners(eventArgs);
            }

            FastClock.getInstance().notifyAtTimeListeners(eventArgs);
        }, false);
    }

    public pause(): void {
        var fastClockElementsValues = { state: 'pause' };
        this.worker.postMessage(fastClockElementsValues);
    }

    public resume(): void {
        var fastClockElementsValues = { state: 'resume' };
        this.worker.postMessage(fastClockElementsValues);
    }

    public setFactor(factor: number): void {
        var fastClockElementsValues = { state: 'setFactor', factor: factor };
        this.worker.postMessage(fastClockElementsValues);
    }

    private setTime(date: Date): void {
        this.currentTime = date;
    }

    public notifyAt(date: Date, functionCallback: () => void): void {
        this.bindCallbackFunctionToDictionary(date, functionCallback);
    }

    public notifyAtTime(time: Time, functionCallback: () => void): void {
        var date = new Date();
        date.setHours(time.getHours());
        date.setMinutes(time.getMinutes());
        date.setSeconds(time.getSeconds());

        if (this.currentTime.getTime() > date.getTime()) {
            date.setDate(this.currentTime.getDate() + 1);
        }

        this.bindCallbackFunctionToDictionary(date, functionCallback);
    }

    public notifyAfter(date: Date, functionCallback: () => void): void {
        this.bindCallbackFunctionToDictionary(new Date(new Date().getTime() + date.getTime()), functionCallback);
    }

    public addOnNextSecondEventListener(listener: (eventArgs: FastClockEventArgs) => void): void {
        this.nextSecondEvent.addListener(listener);
    }

    public addOnNextMinuteEventListener(listener: (eventArgs: FastClockEventArgs) => void): void {
        this.nextMinuteEvent.addListener(listener);
    }

    public addOnNextHourEventListener(listener: (eventArgs: FastClockEventArgs) => void): void {
        this.nextHourEvent.addListener(listener);
    }

    public addOnNextDayEventListener(listener: (eventArgs: FastClockEventArgs) => void): void {
        this.nextDayEvent.addListener(listener);
    }

    public removeOnNextSecondEventListener(listener: (eventArgs: FastClockEventArgs) => void): void {
        this.nextSecondEvent.removeListener(listener);
    }

    public removeOnNextMinuteEventListener(listener: (eventArgs: FastClockEventArgs) => void): void {
        this.nextMinuteEvent.removeListener(listener);
    }

    public removeOnNextHourEventListener(listener: (eventArgs: FastClockEventArgs) => void): void {
        this.nextHourEvent.removeListener(listener);
    }

    public removeOnNextDayEventListener(listener: (eventArgs: FastClockEventArgs) => void): void {
        this.nextDayEvent.removeListener(listener);
    }

    private notifyAtTimeListeners(eventArgs: FastClockEventArgs): void {
        if (typeof (this.callbackDictionary[eventArgs.currentTime.toLocaleString()]) != "undefined") {
            for (var index in this.callbackDictionary[eventArgs.currentTime.toLocaleString()]) {
                (this.callbackDictionary[eventArgs.currentTime.toLocaleString()])[index].callbackFunction(eventArgs);
            };
            this.clearCallbackFunctionToDictionary(eventArgs.currentTime);
        }
    }


    private bindCallbackFunctionToDictionary(date: Date, callbackFunction: () => void): void {
        if (typeof (this.callbackDictionary[date.toLocaleString()]) == "undefined") {
            this.callbackDictionary[date.toLocaleString()] = [];
        }
        this.callbackDictionary[date.toLocaleString()].push({ callbackFunction: callbackFunction });
    }

    private clearCallbackFunctionToDictionary(date: Date): void {
        this.callbackDictionary[date.toLocaleString()] = undefined;
    }
}