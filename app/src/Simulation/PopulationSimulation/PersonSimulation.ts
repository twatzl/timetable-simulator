class PersonSimulation {

	private zoneSimulation: ZoneSimulation;

	constructor(zoneSimulation: ZoneSimulation, jsonData?: any) {
		if (jsonData == undefined) {
			this.zoneSimulation = zoneSimulation;	
		} else {
			this.parseFromJson(jsonData);
		}
	}
	
	public startSimulation(){
		FastClock.getInstance().addOnNextMinuteEventListener((eventArgs: FastClockEventArgs) => {
			this.simulationStep();
		})
	}

	public simulationStep(): void {
		console.log("simulation step");
		var timeOfDay: number = 0;
		this.zoneSimulation.calculateProbabilitiesForZones(timeOfDay);

		var startZone = this.zoneSimulation.getZoneWithHighestProbability();
		this.zoneSimulation.subtractProbabilityFromZone(startZone);

		var destinationZone = this.zoneSimulation.getDestinationWithHighestProbability(startZone);
		this.zoneSimulation.subtractProbabilityfromDestinationZone(startZone, destinationZone);

		this.spawnNewPersonWithStartZone(startZone, destinationZone);
		console.log("simulation step finished");
	}

	private spawnNewPersonWithStartZone(startZone: Zone, destinationZone: Zone): void {
		var person = new Person(
			startZone.generateRandomLocationInZone(),
			destinationZone.generateRandomLocationInZone());
		person.startSimulation();
		person.show(MapService.getMap());
	}
	
	public getJSONString(): string {
		return JSON.stringify(this);
	}
	
	public parseFromJson(jsonData: any): void {
		this.zoneSimulation = jsonData.zoneSimulation;
	}
}