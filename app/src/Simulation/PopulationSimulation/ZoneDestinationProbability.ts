// TODO: 2015-01-23: this is a fucking bad name
class ZoneDestinationProbability {

	private zone: Zone;
	private probabilities: Dictionary<number>;

	constructor(zone: Zone) {
		this.zone = zone;
		this.probabilities = {};
	}

	public raiseProbability(zone: Zone, probability: number) {
		// TODO: 2015-01-23: maybe we can initialize this in constructor
		if (this.probabilities[zone.getId()] == undefined){
			this.probabilities[zone.getId()] = probability;
		} else {
			this.probabilities[zone.getId()] += probability;
		}
	}
	
	public subtractProbability(zone: Zone, probability: number) {
		if (this.probabilities[zone.getId()] == undefined){
			throw new Error("Probability for zone not defined");
		} else {
			this.probabilities[zone.getId()] -= probability;
		}
	}

	public getIdOfZoneWithHighestProbability(): string {
		var maximumProbability: number = 0;
		var zoneId: string;

		for (var key in this.probabilities) {
			if (this.probabilities[key] > maximumProbability) {
				zoneId = key;
			}
		}
		
		return zoneId;
	}

}