class ZoneDemandSimulation {

	private eqn: SimulationEquations;

	constructor(equations: SimulationEquations) {
		this.eqn = equations;
	}

	public getCurrentDemandPercentage(startZoneType: string, destinationZoneType: string, timeOfDay: number): number {
		if (startZoneType == "ResidentialZone") {
			return this.getCurrentResidentialDemandPercentageForZoneType(destinationZoneType, timeOfDay);
		} else if (startZoneType == "CommercialZone") {
			return this.getCurrentCommercialDemandPercentageForZoneType(destinationZoneType, timeOfDay);
		} else if (startZoneType == "IndustrialZone") {
			return this.getCurrentIndustrialDemandPercentageForZoneType(destinationZoneType, timeOfDay);
		} else {
			throw new Error("Unknown zone type.");
		}
	}

	public getCurrentResidentialDemandPercentageForZoneType(zoneType: string, timeOfDay: number): number {
		return this.getCurrentResidentialDemandForZoneType(zoneType, timeOfDay) / this.getSumOfDemands(zoneType, timeOfDay);
	}

	public getCurrentCommercialDemandPercentageForZoneType(zoneType: string, timeOfDay: number): number {
		return this.getCurrentCommercialDemandForZoneType(zoneType, timeOfDay) / this.getSumOfDemands(zoneType, timeOfDay);
	}

	public getCurrentIndustrialDemandPercentageForZoneType(zoneType: string, timeOfDay: number): number {
		return this.getCurrentIndustrialDemandForZoneType(zoneType, timeOfDay) / this.getSumOfDemands(zoneType, timeOfDay);
	}

	public getCurrentSupplyPercentage(zone: Zone, timeOfDay: number): number {
		var zoneType: string = zone.getType();
		//var area: number = zone.getArea();
		if (zoneType == "ResidentialZone") {
			return this.getCurrentSupplyForResidential(timeOfDay);
		} else if (zoneType == "CommercialZone") {
			return this.getCurrentSupplyForCommercial(timeOfDay);
		} else if (zoneType == "IndustrialZone") {
			return this.getCurrentSupplyForIndustrial(timeOfDay);
		} else {
			throw new Error("Unknown zone type.");
		}
	}

	private getCurrentSupplyForResidential(timeOfDay: number): number {
		return this.eqn.getResidentialSupply(timeOfDay);
	}

	private getCurrentSupplyForCommercial(timeOfDay: number): number {
		return this.eqn.getCommercialSupply(timeOfDay);
	}

	private getCurrentSupplyForIndustrial(timeOfDay: number): number {
		return this.eqn.getIndustrialSupply(timeOfDay);
	}

	public getProbabilityValueToSubtractFromZone(): number {
		return this.eqn.getProbabilityValueToSubtractFromZone();
	}
	
	// zoneType: type the source zone has
	// timeOfDay[seconds]
	private getCurrentResidentialDemandForZoneType(zoneType: string, timeOfDay: number): number {
		if (zoneType == "ResidentialZone") {
			return 0;
		} else if (zoneType == "CommercialZone") {
			return this.eqn.getDemandResidentialToCommercial(timeOfDay);
		} else if (zoneType == "IndustrialZone") {
			return this.eqn.getDemandResidentialToIndustrial(timeOfDay);
		} else {
			throw new Error("Unknown zone type.");
		}
	}

	private getCurrentCommercialDemandForZoneType(zoneType: string, timeOfDay: number): number {
		if (zoneType == "ResidentialZone") {
			return this.eqn.getDemandCommercialToResidential(timeOfDay);
		} else if (zoneType == "CommercialZone") {
			return 0;
		} else if (zoneType == "IndustrialZone") {
			return this.eqn.getDemandCommercialToIndustrial(timeOfDay);
		} else {
			throw new Error("Unknown zone type.");
		}
	}

	private getCurrentIndustrialDemandForZoneType(zoneType: string, timeOfDay: number): number {
		if (zoneType == "ResidentialZone") {
			return this.eqn.getDemandIndustrialToResidential(timeOfDay);
		} else if (zoneType == "CommercialZone") {
			return this.eqn.getDemandIndustrialToCommercial(timeOfDay);
		} else if (zoneType == "IndustrialZone") {
			return 0;
		} else {
			throw new Error("Unknown zone type.");
		}
	}

	private getSumOfDemands(zoneType: string, timeOfDay: number) {
		return this.getCurrentCommercialDemandForZoneType(zoneType, timeOfDay) +
			this.getCurrentIndustrialDemandForZoneType(zoneType, timeOfDay) +
			this.getCurrentResidentialDemandForZoneType(zoneType, timeOfDay);
	}

}