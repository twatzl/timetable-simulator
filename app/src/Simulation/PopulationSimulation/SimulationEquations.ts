interface SimulationEquations {

	getDemandResidentialToCommercial(timeOfDay: number): number;

	getDemandResidentialToIndustrial(timeOfDay: number): number;

	getDemandCommercialToResidential(timeOfDay: number): number;

	getDemandCommercialToIndustrial(timeOfDay: number): number;

	getDemandIndustrialToResidential(timeOfDay: number): number;

	getDemandIndustrialToCommercial(timeOfDay: number): number;

	getDistanceDemandRatio(distance: number): number;

	getProbabilityValueToSubtractFromZone(): number;

	getResidentialSupply(timeOfDay: number): number;

	getCommercialSupply(timeOfDay: number): number;
	
	getIndustrialSupply(timeOfDay: number): number;

}