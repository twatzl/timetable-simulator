class ZoneSimulation { 
	
	private zoneStartProbabilites: Dictionary<number>;
	private zoneDestinationProbabilities: Dictionary<ZoneDestinationProbability>;
	
	constructor(private zones: Zone[], private zoneDemandSimulation: ZoneDemandSimulation, jsonData?: any) {
		this.zoneStartProbabilites = {};
		this.zoneDestinationProbabilities = {};
		if (jsonData == undefined) {
			zones.forEach((zone: Zone) => {
				this.zoneStartProbabilites[zone.getId()] = 0;
				this.zoneDestinationProbabilities[zone.getId()] = new ZoneDestinationProbability(zone);
			});
		} else {
			this.parseFromJson(jsonData);
		}
	}
	
	public calculateProbabilitiesForZones(timeOfDay: number): void { 		
		for (var key in this.zones) {
			const startZone = this.zones[key];
			this.zoneStartProbabilites[startZone.getId()] += this.zoneDemandSimulation.getCurrentSupplyPercentage(startZone, timeOfDay);
			
			for (var destinationZone in this.zones) {
				var startZoneType = startZone.getType();
				var destinationZoneType = this.zones[destinationZone].getType();
				
				var currentDemand = this.zoneDemandSimulation.getCurrentDemandPercentage(startZoneType, destinationZoneType, timeOfDay);
				this.zoneDestinationProbabilities[startZone.getId()].raiseProbability(startZone, currentDemand);
			}
		}
		
	}
	
	public getZoneWithHighestProbability(): Zone {
		var max = 0;
		var zoneId: string;
		
		for (var key in this.zoneStartProbabilites) {
			if (this.zoneStartProbabilites[key] > max) {
				max = this.zoneStartProbabilites[key];
				zoneId = key;
			}
		}
		
		// TODO: handle if zoneStartProbabilites is empty
		return ZoneRepository.getInstance().getObjectById(zoneId);
	}
	
	public getDestinationWithHighestProbability(startZone: Zone): Zone {
		var id = this.zoneDestinationProbabilities[startZone.getId()].getIdOfZoneWithHighestProbability();
		return ZoneRepository.getInstance().getObjectById(id);
	}
	
	public subtractProbabilityFromZone(zone: Zone): void {
		this.zoneStartProbabilites[zone.getId()] -= this.zoneDemandSimulation.getProbabilityValueToSubtractFromZone();
	}
	
	public subtractProbabilityfromDestinationZone(startZone: Zone, destinationZone: Zone): void {
		this.zoneDestinationProbabilities[startZone.getId()]
		.subtractProbability(destinationZone, this.zoneDemandSimulation.getProbabilityValueToSubtractFromZone());
	}
	
	public getJSONString(): string {
		return JSON.stringify(this);
	}
	
	public parseFromJson(jsonData: any): void {
		this.zoneDemandSimulation = jsonData.zoneDemandSimulation;
		this.zoneDestinationProbabilities = jsonData.zoneDestinationProbabilities;
		this.zoneStartProbabilites = jsonData.zoneStartProbabilites;
		this.zones = jsonData.zones;
	}
	
}