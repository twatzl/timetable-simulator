class TestSimulationEquations implements SimulationEquations {

	getDemandResidentialToCommercial(timeOfDay: number): number {
		return 0;
	}

	getDemandResidentialToIndustrial(timeOfDay: number): number {
		return 0;
	}

	getDemandCommercialToResidential(timeOfDay: number): number {
		return 0;
	}

	getDemandCommercialToIndustrial(timeOfDay: number): number {
		return 0;
	}

	getDemandIndustrialToResidential(timeOfDay: number): number {
		return 0;
	}

	getDemandIndustrialToCommercial(timeOfDay: number): number {
		return 0;
	}

	getDistanceDemandRatio(distance: number): number {
		return 0;
	}

	getProbabilityValueToSubtractFromZone(): number {
		return 0;
	}

	getResidentialSupply(timeOfDay: number): number {
		return 0;
	}

	getCommercialSupply(timeOfDay: number): number {
		return 0;
	}

	getIndustrialSupply(timeOfDay: number): number {
		return 0;
	}

}