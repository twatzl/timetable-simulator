class DefaultSimulationEquations implements SimulationEquations {

	getDemandResidentialToCommercial(timeOfDay: number): number {
		return 1;
	}

	getDemandResidentialToIndustrial(timeOfDay: number): number {
		return 1;
	}

	getDemandCommercialToResidential(timeOfDay: number): number {
		return 1;
	}

	getDemandCommercialToIndustrial(timeOfDay: number): number {
		return 1;
	}

	getDemandIndustrialToResidential(timeOfDay: number): number {
		return 1;
	}

	getDemandIndustrialToCommercial(timeOfDay: number): number {
		return 1;
	}

	getDistanceDemandRatio(distance: number): number {
		return 1;
	}

	getProbabilityValueToSubtractFromZone(): number {
		return 1;
	}

	getResidentialSupply(timeOfDay: number): number {
		return 1;
	}

	getCommercialSupply(timeOfDay: number): number {
		return 1;
	}

	getIndustrialSupply(timeOfDay: number): number {
		return 1;
	}

}