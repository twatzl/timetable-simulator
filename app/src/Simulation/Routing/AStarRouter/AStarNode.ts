/// <reference path="../../../../../typings/collections.d.ts" />

class AStarNode<T> implements RoutingNode<T> {

	private estimatedWeight: number;
	private actualWeight: number;
	private estimateRouteWeight: number;
	private predecessor: AStarNode<T>;
	private shortestPath: AStarNode<T>[];

	constructor(private routingNode: RoutingNode<T>, private targetNode: RoutingNode<T>) {
		this.estimatedWeight = this.routingNode.getWeightEstimationToTargetNode(targetNode);
	}

	getEstimatedWeight(): number {
		return this.estimatedWeight;
	}

	getActualWeight(): number {
		return this.actualWeight;
	}

	setActualWeight(weight: number): void {
		this.actualWeight = weight;
	}

	getObject(): T {
		return this.routingNode.getObject();
	}
	
	getOriginalRoutingNode(): RoutingNode<T> {
		return this.routingNode;
	}

	setShortestPath(path: AStarNode<T>[]) {
		this.shortestPath = path;
	}

	getShortestPath() {
		return this.shortestPath;
	}

	setPredecessor(node: AStarNode<T>): void {
		this.predecessor = node;
	}

	getPredecessor(): AStarNode<T> {
		return this.predecessor;
	}

	hasPredecessor(): boolean {
		return this.predecessor != undefined && this.predecessor != null;
	}

	getId(): string {
		return this.routingNode.getId();
	}

	getNeighbourNodes(): RoutingNode<T>[] {
		return this.routingNode.getNeighbourNodes();
	}

	getNeighbourNodesWrapped(): AStarNode<T>[] {
		let wrappedNeighbours: AStarNode<T>[] = [];
		this.getNeighbourNodes().forEach((node:RoutingNode<T>) => {
			wrappedNeighbours.push(new AStarNode<T>(node, this.targetNode))
		})
		return wrappedNeighbours;
	}

	getWeightsToNeighbourNodes(): collections.Dictionary<RoutingNode<T>, number> {
		return this.routingNode.getWeightsToNeighbourNodes();
	}

	getWeightEstimationToTargetNode(targetNode: RoutingNode<T>): number {
		return this.routingNode.getWeightEstimationToTargetNode(targetNode);
	}

	// Typo: weight
	getEstimatedRouteWeigth(targetNode: RoutingNode<T>): number {
		return this.getActualWeight() + this.getWeightEstimationToTargetNode(targetNode);
	}

}