/// <reference path="../../../../../typings/collections.d.ts" />

class AStarRouter<T> {

	nodesToBeInvestigated: collections.Queue<AStarNode<T>>;
	fullKnownNodes: collections.Set<AStarNode<T>>;

	getShortestPath(start: RoutingNode<T>, target: RoutingNode<T>): RoutingNode<T>[] {
		this.nodesToBeInvestigated
			= new collections.Queue<AStarNode<T>>();
		this.fullKnownNodes
			= new collections.Set<AStarNode<T>>();

		var startNode: AStarNode<T> = new AStarNode<T>(start, target);
		var targetNode: AStarNode<T> = new AStarNode<T>(target, target);
		
		startNode.setActualWeight(0);
		this.expandNode(startNode);

		this.nodesToBeInvestigated.enqueue(startNode);
		while (!this.nodesToBeInvestigated.isEmpty()) {
			var currentNode: AStarNode<any> = this.getNextNodeToInvestigate();

			if (currentNode == undefined) {
				console.log("The extremely unlikely case of all remaining elements " +
					"of the AStar list having an actual weight of Infinity has just occured. " +
					"Something might probably go very wrong any moment now.");
				continue;
			}

			this.fullKnownNodes.add(currentNode);
			if (currentNode.getId() == targetNode.getId()) {
				return this.getFoundPath(startNode, targetNode);
			}
			this.expandNode(currentNode);
		}
		return null;
	}

	private getNextNodeToInvestigate(): AStarNode<any> {
		var minimumWeight = Infinity;
		var nodeToReturn: AStarNode<any>;
		this.nodesToBeInvestigated.forEach((node: AStarNode<T>) => {
			if (node.getActualWeight() < minimumWeight) {
				minimumWeight = node.getActualWeight();
				nodeToReturn = node;
			}
		});

		return nodeToReturn;
	}

	public expandNode(currentNode: AStarNode<T>) {
		let edgeWeigths = currentNode.getWeightsToNeighbourNodes();
		let neighbours = currentNode.getNeighbourNodesWrapped();

		for (var key in neighbours) {
			const successor = <AStarNode<T>>neighbours[key];
			if (this.fullKnownNodes.contains(successor)) {
				continue;
			}

			var weightOfSuccessorUsingCalculatedRoute = currentNode.getActualWeight() + edgeWeigths.getValue(successor);

			if (this.previousFoundWayIsShorter(successor, weightOfSuccessorUsingCalculatedRoute)) {
				continue;
			}

			successor.setPredecessor(currentNode);
			successor.setActualWeight(weightOfSuccessorUsingCalculatedRoute);
			if (!this.nodesToBeInvestigated.contains(successor)) {
				this.nodesToBeInvestigated.enqueue(successor);
			}
		}
	}

	private previousFoundWayIsShorter(successor: AStarNode<T>, calculatedWeight: number): boolean {
		return this.nodesToBeInvestigated.contains(successor) && calculatedWeight >= successor.getActualWeight()
	}

	private getFoundPath(start: AStarNode<T>, target: AStarNode<T>): RoutingNode<T>[] {
		var route: RoutingNode<T>[] = [];
		var currentNode: AStarNode<T> = target;


		while (currentNode.hasPredecessor()) {
			currentNode = currentNode.getPredecessor();
			route.push(currentNode.getOriginalRoutingNode());
		}

		route.push(currentNode.getOriginalRoutingNode());
		return route.reverse();
	}

}