interface RoutingNode<T> {
	
	getId(): string;
	
	getNeighbourNodes(): RoutingNode<T>[];
	
	getWeightsToNeighbourNodes(): collections.Dictionary<RoutingNode<T>, number>
	
	getWeightEstimationToTargetNode(targetNode: RoutingNode<T>): number;
	
	getObject(): T;
	
}