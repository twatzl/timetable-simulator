/**
 * This class handles the connections between two StationNodes
 * and manages all the lines which connect the two stations.
 */
class StationConnection {

	start: StationNode;
	end: StationNode;

	connectingLines: Line[];

	constructor(start: StationNode, end: StationNode) {
		this.start = start;
		this.end = end;
		this.connectingLines = [];
	}

	public getStart(): StationNode {
		return this.start;
	}

	public getTarget(): StationNode {
		return this.end;
	}

	public addConnectingLine(line: Line) {
		// TODO: maybe integrity checks?
		this.connectingLines.push(line);
	}

	public getConnectingLines(): Line[] {
		return this.connectingLines;
	}

	getDistanceUsingLine(line: Line): number {
		if (this.connectingLines.indexOf(line) == -1) {
			return Infinity;
		}

		return line.getDistanceBetweenStations(this.start.getObject(), this.end.getObject());
	}

	getDistancesForAllLines(): collections.Dictionary<Line, number> {
		var distances: collections.Dictionary<Line, number> =
			new collections.Dictionary<Line, number>();

		for (var key in this.connectingLines) {
			const line = this.connectingLines[key];
			distances.setValue(line, this.getDistanceUsingLine(line));
		}

		return distances;
	}

	public static getLineWithShortestDistance(connection: StationConnection): Line {
		let distances = connection.getDistancesForAllLines();
		let shortestDistance = Infinity;
		let shortestLine: Line = undefined;

		for (var key in distances.keys()){
			const line: Line = distances.keys()[key];
			var distance = distances.getValue(line);
			if (distance < shortestDistance) {
				shortestDistance = distance;
				shortestLine = line;
			}
		}
		return shortestLine;
	}
	
	public static getShortestDistance(connection: StationConnection): number {
		let distances = connection.getDistancesForAllLines();
		let shortestDistance = Infinity;

		for (var key in distances.keys()) {
			const line = distances.keys()[key];
			var distance = distances.getValue(line);
			if (distance < shortestDistance) {
				shortestDistance = distance;
			}
		}
		return shortestDistance;
	}
	
	// still waiting for interface on line
	/*getTime(line: Line): number {
		if (this.connectingLines.indexOf(line) == -1) { 
			return Infinity;
		}
		
		return line.getTimeBetweenStations(this.start, this.end);
	}*/

}