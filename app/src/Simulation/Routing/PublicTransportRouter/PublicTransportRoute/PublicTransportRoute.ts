class PublicTransportRoute {

	route: PublicTransportRouteElement[];

	constructor() {
		this.route = [];
	}

	public pushRouteElement(element: PublicTransportRouteElement): void {
		this.route.push(element);
	}

	public popRouteElement(): PublicTransportRouteElement {
		return this.route.pop();
	}

	public getDistance(): number {
		var distance = 0;
		
		for (var key in this.route) {
			const connection = this.route[key];
			distance += google.maps.geometry.spherical.computeDistanceBetween(
				connection.getStartStation().getLocation(), connection.getEndStation().getLocation());
		}

		return distance;
	}

}