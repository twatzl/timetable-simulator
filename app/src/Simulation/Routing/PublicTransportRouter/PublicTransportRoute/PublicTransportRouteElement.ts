class PublicTransportRouteElement {
	
	private from: Station;
	private to: Station;
	private line: Line;
	
	public constructor(from: Station, to: Station, usingLine: Line) {
		this.from = from;
		this.to = to;
		this.line = usingLine;
	}
	
	public getStartStation(): Station {
		return this.from;
	}
	
	public getEndStation(): Station {
		return this.to;
	}
	
	public getLine(): Line {
		return this.line;
	}
	
}