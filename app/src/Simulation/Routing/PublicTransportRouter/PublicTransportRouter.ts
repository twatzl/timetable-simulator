/**
 * This class provides methods to calculate routes between two stations
 * and returns a PublicTransportRoute, which contains routing information
 * as well as line information, as a result.
 */
class PublicTransportRouter {

	stationNodes: collections.Dictionary<Station, StationNode>;

	constructor(stations: Station[], lines: Line[]) {
		this.initialize(stations, lines);
	}

	public calculateFastestRoute(start: Station, target: Station): PublicTransportRoute {
		if (start == target) {
			return null;
		}
		
		var router: AStarRouter<Station> = new AStarRouter<Station>();
		var startNode: StationNode = this.stationNodes.getValue(start);
		var targetNode: StationNode = this.stationNodes.getValue(target);
		var route: RoutingNode<Station>[] = router.getShortestPath(startNode, targetNode);
		var publicTransportRoute: PublicTransportRoute = new PublicTransportRoute();

		var index = route.length - 1;

		for (index; index >= 0; index--) {
			let currentNode: StationNode = <StationNode>route[index];
			let nextNode: StationNode = <StationNode>route[index - 1];
			var connection = currentNode.getConnectionTo(nextNode);
			var routeElement = new PublicTransportRouteElement(
				currentNode.getObject(),
				nextNode.getObject(),
				StationConnection.getLineWithShortestDistance(connection));

			publicTransportRoute.pushRouteElement(routeElement);
		}

		return publicTransportRoute;
	}

	public getNearestStation(location: ILocation): Station {
		var nearestStation: Station = undefined;
		var nearestDistance: number = Infinity;
		var stations: Station[] = WaypointRepository.getInstance().getAllStations();
		var locationLatLon = GoogleLocation.fromLocation(location).getLatLng();

		for (var key in stations) {
			const currentStation = stations[key];
			var currentDistance = google.maps.geometry.spherical.
				computeDistanceBetween(currentStation.getLocation(), locationLatLon);
			if (currentDistance < nearestDistance) {
				nearestDistance = currentDistance;
				nearestStation = currentStation;
			}
		}
		
		return nearestStation;
	}

	private initialize(stations: Station[], lines: Line[]) {
		this.stationNodes = new collections.Dictionary<Station, StationNode>();

		for (var key in stations) {
			const station = stations[key];
			this.stationNodes.setValue(station, new StationNode(station));
		}

		for (var key in lines) {
			const line = lines[key];
			var stationIndex = 1;
			var stationsOnLine: Station[] = line.getStationList();
			
			if (stationsOnLine.length == 0) {
				continue;
			}

			var nextStation: StationNode;
			var previousStation: StationNode;
			var currentStation: StationNode = this.stationNodes.getValue(stationsOnLine[0]);

			while (stationIndex + 1 < stationsOnLine.length) {
				nextStation = this.stationNodes.getValue(stationsOnLine[stationIndex]);
				currentStation.addConnectedStation(nextStation, line);

				if (previousStation != undefined) {
					currentStation.addConnectedStation(previousStation, line);
				}

				previousStation = currentStation
				currentStation = nextStation;
				stationIndex++;
			}

			if (previousStation != undefined) {
				currentStation.addConnectedStation(previousStation, line);
			}
		}
	}

}