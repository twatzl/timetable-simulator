/**
 * This is a special RoutingNode which handles the weight
 * calculation for a Station.
 */
class StationNode implements RoutingNode<Station> {

	station: Station;
	connections: collections.Dictionary<StationNode, StationConnection>;

	constructor(station: Station) {
		this.station = station;
		this.connections = new collections.Dictionary<StationNode, StationConnection>();
	}

	public addConnectedStation(station: StationNode, viaLine: Line): void {
		var connection: StationConnection;
		if (this.connections.containsKey(station)) {
			connection = this.connections.getValue(station);
			connection.addConnectingLine(viaLine);
		} else {
			connection = new StationConnection(this, station);
			this.connections.setValue(station, connection);
		}
	}

	public isConnectedTo(otherStationNode: StationNode): boolean {
		return this.getConnectionTo(otherStationNode) == undefined;
	}

	public getConnectionTo(otherStationNode: StationNode): StationConnection {
		return this.connections.getValue(otherStationNode);
	}

	/**
	 * interface RoutingNode
	 */

	getId(): string {
		return this.station.getId();
	}

	public getNeighbourNodes(): RoutingNode<Station>[] {
		let neighbours: RoutingNode<Station>[] = [];

		this.connections.forEach((key: StationNode, connection: StationConnection) => {
			let endnode = connection.getTarget();
			neighbours.push(endnode);
		});

		return neighbours;
	}

	getWeightsToNeighbourNodes(): collections.Dictionary<RoutingNode<Station>, number> {
		var weights: collections.Dictionary<RoutingNode<Station>, number>
			= new collections.Dictionary<RoutingNode<Station>, number>();

		this.connections.forEach( (key: StationNode, connection: StationConnection) => {
			weights.setValue(connection.getTarget(), StationConnection.getShortestDistance(connection));
		});

		return weights;
	}

	getWeightEstimationToTargetNode(targetNode: RoutingNode<Station>): number {
		var targetStation = targetNode.getObject();

		var targetLocation = targetStation.getLocation();
		var thisLocation = this.getObject().getLocation();

		return google.maps.geometry.spherical.computeDistanceBetween(targetLocation, thisLocation);
	}

	getObject(): Station {
		return this.station;
	}

}