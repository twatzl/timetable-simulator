/// <reference path="JSONFileWriter.ts"/>
/// <reference path="Timetable/model/TimetableRepository.ts"/>
/// <reference path="Timetable/model/CourseRepository.ts"/>

class RepositorySaver extends JSONFileWriter {

	public static save (filePath: string, fs:any, dialog: any) {
		
		var jsonObjects = "{" +
		TimetableRepository.getInstance().getJSONStringOfRepository("timetables") + 
		CourseRepository.getInstance().getJSONStringOfRepository("courses") +
		LineRepository.getInstance().getJSONStringOfRepository("lines") +
		WaypointRepository.getInstance().getJSONStringOfRepository("waypoints") + 
		VehicleRepository.getInstance().getJSONStringOfRepository("vehicles");
		
		jsonObjects = jsonObjects.slice(0, -1);
		jsonObjects += "}";
		
		this.writeInJSONFile(filePath, jsonObjects, fs, dialog);
		
		/*this.writeInJSONFile(filePath, TimetableRepository.getInstance().getJSONStringOfRepository("timetables"), fs, dialog);
		this.writeInJSONFile(filePath, CourseRepository.getInstance().getJSONStringOfRepository("courses"), fs, dialog);*/
		//this.writeInJSONFile(filePath, TimetableRepository.getInstance().getJSONStringOfRepository("stations"), fs);
		/*this.writeInJSONFile(filePath, LineRepository.getInstance().getJSONStringOfRepository("lines"), fs, dialog);
		this.writeInJSONFile(filePath, WaypointRepository.getInstance().getJSONStringOfRepository("waypoints"), fs, dialog);*/
	}
}