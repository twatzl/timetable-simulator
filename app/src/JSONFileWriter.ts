class JSONFileWriter {
    public static writeInJSONFile(filePath: string, jsonStringOfRepository: string, fs: any, dialog: any): void {
        fs.writeFile(filePath, jsonStringOfRepository, function(err: Error) {
            if (err === null) {
                dialog.showMessageBox({ message: "The file has been saved!", buttons: ["OK"] });
            } else {
                dialog.showErrorBox("File Save Error", err.message);
            }
        });
    }
}