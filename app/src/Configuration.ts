class Configuration {
	
	public static PERSON_FILL_COLOR = '#0080FF';
	public static PERSON_FILL_OPACITY = 0.35;
	public static PERSON_STROKE_COLOR = '#0080FF';
	public static PERSON_STROKE_OPACITY = 0.8;
	public static PERSON_STROKE_WEIGHT = 2;
	
	public static DEFAULT_CAR_SPEED = 15;
	public static DEFAULT_PUBLIC_TRANSPORT_SPEED = 15;
	
}