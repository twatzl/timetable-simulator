class JSONFileReader {
    public static readFromJSONFile(filePath: string, fs: any, dialog: any): JSON {
        var fileContent = "";

        fileContent = fs.readFileSync(filePath, fileContent, function(err: Error) {
            if (err === null) {
                dialog.showMessageBox({ message: "The file has been loaded!", buttons: ["OK"] });
            } else {
                dialog.showErrorBox("File Load Error", err.message);
            }
        });

        return JSON.parse(fileContent);
    }
}