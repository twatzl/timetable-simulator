class ObservableEvent<T> {
	private eventHandlers: Array<(eventArgs: T) => void>; // yep, thats a function that takes event args and returns nothing. pretty crazy definition.
	
	constructor() {
		this.eventHandlers = new Array<(eventArgs: T) => void>();
	}
	
	public addListener(listener: (eventArgs: T) => void): void {
		var index = this.eventHandlers.indexOf(listener);
		if (index != -1) {
			return; // do not allow to subscribe twice
		}
		this.eventHandlers.push(listener);
	}
	
	public removeListener(listener: (eventArgs: T) => void): void {
		var index = this.eventHandlers.indexOf(listener);
		if (index > -1) {
			this.eventHandlers.splice(index, 1);
		}
	}
	
	public notifyListeners(eventArgs: T) {
		this.eventHandlers.forEach((value: (eventArgs: T) => void, index: number, array: ((eventArgs:T)=> void)[]) => {
			value(eventArgs);
		});
	}
}