/// <reference path="Simulation/FastClock.ts"/>
/// <reference path="RepositorySaver.ts"/>
/// <reference path="RepositoryLoader.ts"/>

var app = angular.module('TimetableSimulator', ['ui.bootstrap']);

var createStationMode = false;
var zoneManager;

app.service('googleMapsService', function() {
    this.map = {};

    // Init Google Maps
    this.init = function(element, lat, lng, zoom, options) {
        MapService.init(element, lat, lng, zoom, options);
        this.map = MapService.getMap();
        return this.map;
    };
});

app.controller('MainCtrl', function($scope, $uibModal, $log, googleMapsService) {
    $scope.finance = Finance.getInstance();
    $scope.items = ['item1', 'item2', 'item3'];
    $scope.animationsEnabled = true;
    googleMapsService.init(document.getElementById('map'), 48.2542298, 14.3406374, 10, {});
    zoneManager = new ZoneManager(googleMapsService.map);
    $scope.isShowActive = true;
    $scope.areZonesLoaded = false;
    $scope.zoneButtonText = "Load Zones";

    FastClock.getInstance().addOnNextSecondEventListener(function(eventArgs) {
        $scope.currentTime = eventArgs.currentTime;
        $scope.$apply();
    });

    $scope.pause = function() {
        $scope.activeFastClockMode = "pause";
        FastClock.getInstance().pause();
    }

    $scope.play = function() {
        $scope.activeFastClockMode = "play";
        FastClock.getInstance().resume();
        FastClock.getInstance().setFactor(5);
    }

    $scope.playFast = function() {
        $scope.activeFastClockMode = "playFast";
        FastClock.getInstance().resume();
        FastClock.getInstance().setFactor(10);
    }

    $scope.playFaster = function() {
        $scope.activeFastClockMode = "playFaster";
        FastClock.getInstance().resume();
        FastClock.getInstance().setFactor(200);
    }

    $scope.isActive = function(id) {
        return $scope.activeFastClockMode == id;
    }

    $scope.play();
    initializeTestData(googleMapsService);

    $scope.toTwoDigitNumber = function(number) {
        return ("0" + Number(number)).slice(-2)
    }


    $scope.saveGame = function() { }

    var remote = require('remote');
    var dialog = remote.require('dialog');
    var fs = require('fs');

    $scope.saveFile = function() {
        dialog.showSaveDialog(
            {
                filters: [{ name: 'json', extensions: ['json'] }]
            }, function(fileName) {
                //var timetableData = TimetableRepository.getInstance().getObjectList();
                var vehicleData = VehicleRepository.getInstance().getObjectList();
				/*var lineData = LineRepository.getInstance().getObjectList();
				var stationData = WaypointRepository.getInstance().getObjectList();*/
                var sd = [];
                for (var key in TimetableRepository.getInstance().getObjectList()) {
                    var value = TimetableRepository.getInstance().getObjectList()[key];
                    sd.push(value.getJSONString());
                    // Use `key` and `value`
                }
                var sdd = [];
                for (var key in WaypointRepository.getInstance().getObjectList()) {
                    var value = WaypointRepository.getInstance().getObjectList()[key];
                    sdd.push(value.getJSONString());
                    // Use `key` and `value`
                }
                var sddd = [];
                for (var key in CourseRepository.getInstance().getObjectList()) {
                    var value = CourseRepository.getInstance().getObjectList()[key];
                    sddd.push(value.getJSONString());
                }
                var sds = [];
                for (var key in LineRepository.getInstance().getObjectList()) {
                    var value = LineRepository.getInstance().getObjectList()[key];
                    sds.push(value.getJSONString());
                }
                var x = [];
                for (var key in WaypointRepository.getInstance().getObjectList()) {
                    var value = WaypointRepository.getInstance().getObjectList()[key];
                    x.push(value.getJSONString());
                }
                console.log(LineRepository.getInstance().getObjectList());
                console.log(WaypointRepository.getInstance().getObjectList());
				/*var savedData = {
					//timetableJSON: JSON.stringify(timetableData),
					vehicleJSON: JSON.stringify(vehicleData),
					timetableJSON: TimetableRepository.getInstance().getObjectList()["1"].getJSONString()
					/*lineJSON: JSON.stringify(lineData),
					stationJSON: JSON.stringify(stationData)
				}*/

                //console.log(savedData.timetableJSON);
                if (fileName === undefined)
                    return;
                fs.writeFile(fileName, x, function(err) {
                    dialog.showMessageBox({ message: "The file has been saved!", buttons: ["OK"] });
                });
            });
    }

    $scope.onSaveButtonPressed = function() {
        var repositorySaver = new RepositorySaver();
        dialog.showSaveDialog({
            filters: [{ name: 'json', extensions: ['json'] }]
        }, function(fileName) {
            if (fileName === undefined) {
                return;
            }
            RepositorySaver.save(fileName, fs, dialog);
        });
    }

    $scope.onLoadButtonPressed = function() {
        dialog.showOpenDialog(
            {
                filters: [{ name: 'json', extensions: ['json'] }]
            }, function(fileName) {

                if (fileName === undefined) {
                    return;
                }
                $scope.loadGame(RepositoryLoader.load(fileName[0], fs, dialog));
            });
    }

    $scope.onLoadZonesButtonPressed = function() {
        dialog.showOpenDialog(
            {
                filters: [{ name: 'json', extensions: ['json'] }]
            }, function(fileName) {

                if (fileName === undefined) {
                    return;
                }
                zoneManager.clearZoneRepository();
                zoneManager.fillZoneRepository(MapLoader.load(fileName[0], fs, dialog));
                zoneManager.enterLockModeListeners();
				
				Simulation.startSimulation();
				
                $scope.areZonesLoaded = true;
                $scope.zoneButtonText = "Hide Zones";
            });
    }

    $scope.onShowOrHideOrLoadZonesButton = function() {
        if (!$scope.areZonesLoaded) {
            $scope.onLoadZonesButtonPressed();
        }
        else {
            if (!$scope.isShowActive) {
                zoneManager.showZones();
                $scope.isShowActive = true;
                $scope.zoneButtonText = "Hide Zones";
            }
            else {
                zoneManager.hideZones();
                $scope.isShowActive = false;
                $scope.zoneButtonText = "Show Zones";
            }
        }
    }

    $scope.clearAllRepositories = function() {
        $scope.deleteRepositoryElements(VehicleRepository.getInstance());
        $scope.deleteRepositoryElements(CourseRepository.getInstance());
        $scope.deleteRepositoryElements(TimetableRepository.getInstance());
        $scope.deleteRepositoryElements(LineRepository.getInstance());
        $scope.deleteRepositoryElements(WaypointRepository.getInstance());
    }

    $scope.deleteRepositoryElements = function(repositoryInstance) {
        for (var k in repositoryInstance.getObjectList()) {
            repositoryInstance.deleteObjectById(k);
        }
    }

    $scope.showInfo = function() {
        alert("fds");
    }

    $scope.createVehicleFromGameLoad = function(element, index) {
        var newVehicle = new Vehicle(element);
        VehicleRepository.getInstance().addObject(newVehicle);
    }

    $scope.createCourseFromGameLoad = function(element, index) {
        var newCourse = new Kurs(element);
        CourseRepository.getInstance().addObject(newCourse);
    }

    $scope.createTimetableFromGameLoad = function(element, index) {
        var newTimetable = new Timetable(element);
        TimetableRepository.getInstance().addObject(newTimetable);
    }

    $scope.createLineFromGameLoad = function(element, index) {
        var newLine;
        if (element.polyline == "UnboundLine") {
            newLine = new UnboundLine(element);
        }
        else {
            newLine = new StreetBoundLine(element);
        }
        LineRepository.getInstance().addObject(newLine);
        newLine.show(googleMapsService.map);
    }

    $scope.createWaypointsFromGameLoad = function(element, index) {
        var newStation = new Station(element.name, element.size, new google.maps.LatLng(element.location.lat, element.location.lng), element);
        WaypointRepository.getInstance().addObject(newStation);
        newStation.show(googleMapsService.map);
    }

    $scope.loadGame = function(jsonObject) {
        console.log(jsonObject);
        $scope.clearAllRepositories();
        if (jsonObject.vehicles != undefined) {
            jsonObject.vehicles.forEach(function(element, index) {
                $scope.createVehicleFromGameLoad(element, index);
            });
        }
        if (jsonObject.courses != undefined) {
            jsonObject.courses.forEach(function(element, index) {
                $scope.createCourseFromGameLoad(element, index);
            });
        }
        if (jsonObject.timetables != undefined) {
            jsonObject.timetables.forEach(function(element, index) {
                $scope.createTimetableFromGameLoad(element, index);
            });
        }
        if (jsonObject.lines != undefined) {
            jsonObject.lines.forEach(function(element, index) {
                $scope.createLineFromGameLoad(element, index);
            });
        }
        if (jsonObject.waypoints != undefined) {
            jsonObject.waypoints.forEach(function(element, index) {
                $scope.createWaypointsFromGameLoad(element, index);
            });
        }
        // Some objects need data from other objects, load those additional data:

        // Set Waypoints in Line

        jsonObject.lines.forEach(function(element, index) {
            var id = element.id;
            if (Object.prototype.toString.call(element.waypointManager) === '[object Array]') {
                element.waypointManager.forEach(function(el, index) {
                    if (WaypointRepository.getInstance().getObjectById(el) != undefined) {
                        LineRepository.getInstance().getObjectById(id).getWaypointManager().add(WaypointRepository.getInstance().getObjectById(el));
                        LineRepository.getInstance().getObjectById(id).show(googleMapsService.map);
                    }
                });
            }
        });

        // Set Line in Timetables

        jsonObject.timetables.forEach(function(element, index) {
            var timetableId = element.id;
            var lineId = element.line;
            TimetableRepository.getInstance().getObjectById(timetableId).setLine(LineRepository.getInstance().getObjectById(lineId));
        });

        // Set Timetable in Courses

        jsonObject.courses.forEach(function(element, index) {
            var kursId = element.id;
            var timetableId = element.timetable;
            CourseRepository.getInstance().getObjectById(kursId).setTimetable(TimetableRepository.getInstance().getObjectById(timetableId));
        });

        // Set Station in CourseElements

        jsonObject.courses.forEach(function(element, index) {
            var currCourse = CourseRepository.getInstance().getObjectById(element.id);
            currCourse.getCourseElements().forEach(function(el, i) {
                el.station = WaypointRepository.getInstance().getObjectById(element.courseElements[i].station);
            });
        });
    }
});

function initializeTestData(googleMapsService) {
    initializeStationData(googleMapsService);
    initializeLineData(googleMapsService);
    initializeVehicleData();
    initializeTimetableData();
}

function initializeVehicleData() {
    var vehicle = new Vehicle();
    vehicle.setName("Bus");
    vehicle.setCapacity(25);
    vehicle.setMaxSpeed(14);
    vehicle.setAcceleration(0.6);
    VehicleRepository.getInstance().addObject(vehicle);

    var vehicle = new Vehicle();
    vehicle.setName("Tram");
    vehicle.setCapacity(65);
    vehicle.setMaxSpeed(24);
    vehicle.setAcceleration(1.2);
    VehicleRepository.getInstance().addObject(vehicle);
}

function initializeStationData(googleMapsService) {
    var station1 = new Station("Station 1", 5, new google.maps.LatLng(48.273496, 14.313447));
    var station2 = new Station("Station 2", 5, new google.maps.LatLng(48.276709, 14.312266));
    var station3 = new Station("Station 3", 5, new google.maps.LatLng(48.280813, 14.308956));

    WaypointRepository.getInstance().addObject(station1);
    WaypointRepository.getInstance().addObject(station2);
    WaypointRepository.getInstance().addObject(station3);

    station1.show(googleMapsService.map);
    station2.show(googleMapsService.map);
    station3.show(googleMapsService.map);
}

function initializeLineData(googleMapsService) {
    var station1 = WaypointRepository.getInstance().getObjectById(1);
    var station2 = WaypointRepository.getInstance().getObjectById(2);
    var station3 = WaypointRepository.getInstance().getObjectById(3);

    var lineA = new UnboundLine();
    lineA.data.setName("Linea A");

    var lineB = new StreetBoundLine();
    lineB.data.setName("Linea B");

    var lineC = new UnboundLine();
    lineC.data.setName("Linea C");

    lineA.addWaypoint(station1);
    lineA.addWaypoint(station2);
    lineA.addWaypoint(station3);

    lineB.addWaypoint(station1);
    lineB.addWaypoint(station3);

    lineA.show(googleMapsService.map);
    lineB.show(googleMapsService.map);
    lineC.show(googleMapsService.map);

    var repository = LineRepository.getInstance();
    repository.addObject(lineA);
    repository.addObject(lineB);
    repository.addObject(lineC);
}

function initializeTimetableData() {
    var timetableA = new Timetable();
    timetableA.setValidFromDate(new Date('1/1/2016'));
    timetableA.setValidToDate(new Date('12/31/2016'));
    timetableA.setBeginTime(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 23, 0, 0, 0));
    timetableA.setEndTime(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 23, 59, 0, 0));
    timetableA.setLine(LineRepository.getInstance().getObjectList()["1"]);
    TimetableRepository.getInstance().addObject(timetableA);
    LineRepository.getInstance().getObjectList()["1"].getTimetableManager().add(timetableA);

    var timetableB = new Timetable();
    timetableB.setValidFromDate(new Date('1/1/2016'));
    timetableB.setValidToDate(new Date('12/31/2016'));
    timetableB.setBeginTime(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 23, 0, 0, 0));
    timetableB.setEndTime(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 23, 59, 0, 0));
    timetableB.setLine(LineRepository.getInstance().getObjectList()["2"]);
    TimetableRepository.getInstance().addObject(timetableB);
    LineRepository.getInstance().getObjectList()["2"].getTimetableManager().add(timetableB);
}