var onLinesTabEntered = new ObservableEvent();
var onLinesTabLeft = new ObservableEvent();
var onVehiclesTabEntered = new ObservableEvent();
var onVehiclesTabLeft = new ObservableEvent();
var onStationsTabEntered = new ObservableEvent();
var onStationsTabLeft = new ObservableEvent();
var onFinancesTabEntered = new ObservableEvent();
var onFinancesTabLeft = new ObservableEvent();

app.controller('TabsCtrl', function ($scope, googleMapsService) {
	$scope.scope = $scope;
	$scope.map = googleMapsService;

	var onLinesTabSelected = function() { onLinesTabEntered.notifyListeners(); };
	var onLinesTabDeselected = function() { onLinesTabLeft.notifyListeners(); };
	var onVehiclesTabSelected = function() { onVehiclesTabEntered.notifyListeners(); }
	var onVehiclesTabDeselected = function() { onVehiclesTabLeft.notifyListeners(); }	
	var onStationsTabSelected = function() { onStationsTabEntered.notifyListeners(googleMapsService.map); }
	var onStationsTabDeselected = function() {onStationsTabLeft.notifyListeners(); }
	var onFinancesTabSelected = function () { onFinancesTabEntered.notifyListeners(); }
	var onFinancesTabDeselected = function () { onFinancesTabLeft.notifyListeners(); }
	
	$scope.tabs = [
		{ title: 'Lines', contentURL: 'partials/Lines/LinesPartial.html', onTabSelected: onLinesTabSelected, onTabDeselected: onLinesTabDeselected },
		{ title: 'Vehicles', contentURL: 'partials/Vehicles/VehiclesPartial.html', onTabSelected: onVehiclesTabSelected, onTabDeselected: onVehiclesTabDeselected },
		{ title: 'Stations', contentURL: 'partials/Stations/StationsPartial.html', onTabSelected: onStationsTabSelected, onTabDeselected: onStationsTabDeselected },
		{ title: 'Finance', contentURL: 'partials/Finances/FinancePartial.html', onTabSelected: onFinancesTabSelected, onTabDeselected: onFinancesTabDeselected }
	];
	
});

