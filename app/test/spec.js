describe("Test line repo length", function() {
	LineRepository.getInstance().addObject(new Line());
	console.log(LineRepository.getInstance().getObjectList());
	var line = new Line();
	console.log(line);
	it("check length", function() {
		var counter = 0;
		for (var i in LineRepository.getInstance().getObjectList()) 
		{
			counter++;
		}
		expect(counter).toBe(1);
	});
});

describe("SimulationEquation", function () {
	var se = new TestSimulationEquations();
	console.log(se);
	var zds = new ZoneDemandSimulation(se);
	console.log(zds);
	var x = zds.getCurrentDemandPercentage("ResidentialZone", "IndustrialZone", 12);
	console.log(x);
	
});