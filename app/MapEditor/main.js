// Our app
var app = require('app');
var fs = require('fs');

var BrowserWindow = require('browser-window');
var mainWindow = null;

app.on('window-all-closed', onWindowClosed);

app.on('ready', onWindowReady);

function onWindowClosed() {
	if (process.platform != 'darwin') {
		app.quit();
	}
}

function onWindowReady() {
	mainWindow = new BrowserWindow({ width: 1280, height: 720 });
	mainWindow.loadUrl('file://' + __dirname + '/index.html');
	mainWindow.openDevTools();
	mainWindow.on('closed', function () {
		mainWindow = null;
	});
}