# Notes for Finance System and Simulation

## Finance System:

- operating vehicles costs money every minute, value calculated depending on vehicle properties
	- Is called in vehicleinstance every minute as long as vehicle is running.
	- As Vehicle properties are changed vehicleOperatingPrice is recalculated in Vehicle class
- creating lines costs money
- creating stations costs money
- assigning a station to a line costs money, depending on the distance
	- Is called when user clicked button to stop moving, if station is part of a line or when station is assigned to line.
- when creating or moving a station the user should accept the transaction after he finished moving, by pressing a button which is displayed in an info window
- ticket prices are per station (can be set by the user)
	- Money is transferred when VehicleInstance reaches station. Ticket price multiplied by number of passengers
- maintenance = general factor that changes the likelyhood of people choosing public transport instead of cars (image)

Transactions werden an stellen platziert an denen die Finanzen geändert werden müssen. Sie enthalten einen type welcher festlegt um welche art transaction es sich handelt. In der singleton finanzklasse werden die transactions dann behandelt. Sie könnnen entweder erfolgreich sein oder fehlschlagen. Wenn der user zu wenig geld hat dann meldung anzeigen. Wenn die transaktion fehlschlägt darf die aktion nicht durchgeführt werden.


## Simulation:

Depending on size and density of a zone the number of people living there and the number of jobs provided is determined.

For the simulation only a small fraction of the inhabitants is simulated and shown on the map. These spawn randomly in the source zone and hava a random target in the destination zone. The source and destination zone are choosen by several factors including number of available jobs and distance. Depending on the current time of day it is more or less likely for inhabitants to spawn and they spawn in different areas. (morning residential -> industrial/commercial; evening industrial/commercial -> residential) These can also differ for certain times of the year e.g. holidays.

After spawning inhabitants calculations determine if he goes by foot, by car or uses public transport. This depends on the time that a transport system takes and on the image the company has.

When choosing public transport the person walks to a nearby station and waits for a vehicle to come by.
Vehicle instances will have to be extended with a capacity.

The simulation is recalculated ever hour/half hour/quater hour.

A penalty on the speed of vehicles is given based on the ratio of individual transport to public transport.
