#How to deploy our application and get an app:

There are two applications in our project
- Our main application timetable-simulator in the directory app
- and a tool called MapEditor in the directory MapEditor.

For getting a .app Application of the timetable-simulator follow the steps below:

#Step 1:

For using our application, please visit nodejs.org and download Nodejs.
It's a javascript runtime that powers our application.

#Step 2:

Now that you have Node installed, you can install some additional packages via the node package manager (short: npm):

	npm install -g typescript
	npm install -g electron-prebuilt
	npm install -g gulp

**Note: You might need to run these commands with "sudo".**

These two commands will install typescript, which is a so-called "superset" of javascript. It extends Javascript's capabilities
and introduces nice features from object-oriented languages like classes, interfaces and type-safety.
The seconds package is electron, it's basically a chromium-browser that displays one specific website: our application. That means
we can have a desktop application but can also use the widely available web development technologies. And the third package is the 
javascript task runner gulp which is a tool for automating tasks like releasing your application, compile typescript to javascript and so on.

#Step 2:

Go to the root directory of our application and type in:

	npm install

This will install all needed dependencies for the project.

**Note: You might need to run these commands with "sudo".**

There are two types of dependencies which will be installed when calling npm install:

- The first being general development tools like the electron modules or the task runner gulp.
- The other being mainly for development frameworks like our frontend javascript framework AngularJS and our design frameworks bootstrap and material design.

#Step 3:

There are two different ways to run the application. The first is to simply start the application with a gulp task. The second is to create a release with the release gulp task.

##Run

In order to simply run the application type:

	gulp run-game

for the main game application and

	gulp run-mapeditor

in order to run the MapEditor application.

##Release

**Note: currenty the release task is not working due to changes in the gulp file.**

Go to the project's root again.
Type in:

	gulp release --releaseTarget=app/MapEditor --outputName=mapeditor

or

	gulp release --releaseTarget=app --outputName=timetable-simulator

whether you want to release our main application or our tool.

**Note: Depending on your computer's computing capability, releasing the app might take several minutes.**

The result wil be a .app Mac Application that can be started with a double-click as expected.
The timetable-simulator application can be found in the project root, and the MapEditor in the /app/ directory.