Convert Google Sheet to Markdown:

- Save Googe Sheet as csv file from the Google Drive website.
- Open this website: https://donatstudios.com/CsvToMarkdownTable or http://www.tablesgenerator.com/markdown_tables
- Copy the content of the csv file into the website.
- Finish!

Convert Issue json to Markdown:

- Copy the content of the issue.json file in this website: https://json-csv.com/  
- Copy and save as csv.
- Delete the superfluous colums in the csv file.
- Follow the tutorial: "Convert Google Sheet to Markdown".
- Finish!


At this website you can see the converted result: http://dillinger.io/

If you have question please contact Dobroka Andreas.