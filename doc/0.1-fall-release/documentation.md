Public Transport Systems:

Adding a Line:
To add a line, press the add line button in the Lines tab.

Removing a Line:
To remove a line, first select the line and then press the remove line button.

Editing a Line:
To edit a Line, first select it and then change its properties as you want them to be. You can change its name and color.

Creating a Station:
To create stations, switch to the stations tab, click add station and then click to the point in the map where the station should be created

Editing a Station:
To edit a Station, first select it in the list of stations in the stations tab and then change its properties as you want them to be.

Removing a Station:
To remove stations, select the station in the stations list in the stations tab and then click the remove station from map button in the stations tab.

Adding a Station to a Line:
To add stations to lines, click the add station button in the lines tab and click on the station to be added in the map view.

Removing a Station from a Line:
To remove stations from lines, first select the line, then click the remove station button and click on the stationto be removed in the map view.