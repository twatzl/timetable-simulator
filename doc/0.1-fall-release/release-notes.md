For this release, we have completely rewritten our project.
We switched programming languages from C++ to HTML/JavaScript.
We now use Electron, Angular, Bootstrap and Typescript as our technologies.

Features currently available include:
- A Google Map
- User can create and delete lines
- User can create and delete stations
- Stations are shown on the map
- Lines are shown on the map
- Stations' properties can be changed
- Lines' properties can be changed
- Stations can be added/removed to/from a Line
- Userfriendly design via Bootstrap
