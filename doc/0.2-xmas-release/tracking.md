|  Task | **Original Estimate** | **Current Estimate** | **Effort** | **Remaining** | **Responsible** |  |
|  ------ | ------ | ------ | ------ | ------ | ------ | ------ |
|  Create basic application structure | 2 | 3 | 3 |  | twatzl |  |
|  Add angular library | 1 | 2 | 2 |  | sselig |  |
|  Add google maps library | 1 | 1 | 1 |  | sselig |  |
|  Add angular app and controller | 1 | 1 | 1 |  | twatzl |  |
|  Create station model  | 2 | 4 | 4 |  | adobroka |  |
|  Implement fast clock | 4 | 6 | 6 |  | adobroka |  |
|  Create basic view with tabs | 3 | 4 | 4 |  | sselig |  |
|  Create vehicle model | 2 | 2 | 2 |  | twatzl |  |
|  Implement model repositories | 4 | 6 | 6 |  | twatzl |  |
|  Implement crud ops. for lines | 4 | 4 | 4 |  | twatzl |  |
|  Implement line rendering | 4 | 5 | 5 |  | twatzl |  |
|  Implement a WaypointRepository | 2 | 2 | 2 |  | twatzl |  |
|  Implement Line property view | 3 | 4 | 4 |  | sselig |  |
|  Make station drawable | 4 | 4 | 4 |  | adobroka |  |
|  Make lists scrollable | 2 | 2 | 2 |  | sselig |  |
|  Create a Timetable model | 3 | 3 | 3 |  | twatzl |  |
|  Bugfix lines | 2 | 2 | 2 |  | sselig |  |
|  Make it possible to add stations to lines | 3 | 3 | 3 |  | adobroka |  |
|  Use ui bootstrap for tabs | 4 | 4 | 4 |  | twatzl |  |
|  Implement timetable view | 3 | 3 | 3 |  | twatzl | basic work done (modal view) |
|  Implement timetable view | 8 | 12 | 12 |  | sselig | including bugfixing |
|  Implement station info windows | 3 | 4 | 4 |  | adobroka |  |
|  Bugfix stations  | 3 | 6 | 6 |  | adobroka |  |
|  provide google map as service | 2 | 3 | 3 |  | twatzl |  |
|  Refactor the app structure | 8 | 6 | 6 |  | twatzl |  |
|  Info window improvements (make it editable) | 4 | 3 | 3 |  | adobroka |  |
|  Make stations draggable | 3 | 2 | 2 |  | adobroka |  |
|  Redesign the gui | 8 | 15 | 15 |  | twatzl |  |
|  Documents for fall release | 8 | 10 | 10 |  | sselig |  |
|  Testing | 5 | 4 | 4 |  | sselig |  |
|  Implement drawing of bus and train lines | 6 | 8 | 8 |  | twatzl |  |
|  Bugfix stations gui | 4 | 4 | 4 |  | adobroka |  |
|  Bugfix the timetable gui | 6 | 8 | 8 |  | sselig |  |
|  Implement observable events | 3 | 3 | 3 |  | twatzl |  |
|  Implement the vehicles tab | 8 | 6 | 6 |  | twatzl |  |
|  Implement vehicle simulation logic | 6 | 8 | 8 |  | twatzl |  |
|  Implement a map editor application | 6 | 8 | 8 |  | adobroka |  |
|  update fall release documents | 3 | 4 | 4 |  | sselig |  |
|  Implement the basic simulation  | 8 | 8 | 8 |  | twatzl |  |
|  Implement vehicle simulation and rendering | 4 | 4 | 4 |  | twatzl |  |
|  Implement zoning in map editor | 8 | 6 | 6 |  | adobroka |  |
|  Export zones to .json | 6 | 8 | 8 |  | adobroka |  |
|  Implement courses view | 6 | 5 | 5 |  | sselig |  |
|  Import zones from .json | 6 | 5 | 5 |  | adobroka |  |
|  Implement vehicles which follow timetable | 3 | 3 | 3 |  | twatzl |  |
|  Add icon set | 1 | 2 | 2 |  | twatzl |  |
|  Bugfix timetable view | 3 | 4 | 4 |  | sselig |  |
|  Update stations gui | 5 | 5 | 5 |  | adobroka |  |
|  Refactoring models and test data generation | 4 | 4 | 4 |  | twatzl |  |
|  Implement vehicle creation for courses | 2 | 2 | 2 |  | twatzl |  |
|  Update mapeditor gui | 3 | 4 | 4 |  | adobroka |  |
|  Implement loading and saving | 6 | 8 | 8 |  | sselig |  |
|  Bugfix stations | 6 | 8 | 8 |  | adobroka |  |
|  Refactoring load/save and CourseController | 4 | 5 | 5 |  | sselig |  |
|  Bugfixing timetable view  | 6 | 7 | 7 |  | sselig |  |
|  Bugfixing lines | 2 | 3 | 3 |  | twatzl |  |
|  Resolve issues from Issue tracker | 12 | 15 | 15 |  | adobroka |  |
|  Resolve issues from Issue tracker | 10 | 16 | 16 |  | twatzl |  |
|  Resolve issues from Issue tracker | 12 | 22 | 22 |  | sselig |  |
|  Improve the usabiltiy of timetable view | 4 | 5 | 5 |  | sselig |  |
|  Implement functionality for vehicles to follow the routes of Lines. | 6 | 6 | 6 |  | twatzl |  |
|  Resolve merge issues | 2 | 1 | 1 |  | sselig |  |
|  Resolve merge issues | 1 | 1 | 1 |  | twatzl |  |
|  Change routing and rendering of lines | 6 | 7 | 7 |  | twatzl |  |
|  Implement first version of executable generation (incl. gulp task) | 5 | 8 | 8 |  | sselig |  |
|  Fix issues from Issue tracker | 3 | 3 | 3 |  | twatzl |  |
|  Fix issues from Issue tracker | 4 | 5 | 5 |  | adobroka |  |
|  Resolve merge issues | 3 | 3 | 3 |  | sselig |  |
|  Change implementation of CourseWaypointIterator | 2 | 2 | 2 |  | twatzl |  |
|  Change timetable handling of Vehicles | 3 | 3 | 3 |  | twatzl |  |
|  Add iterator interface | 3 | 3 | 3 |  | twatzl |  |
|  Change courses implementation | 6 | 10 | 10 |  | sselig |  |
|  Fix merge issues | 2 | 2 | 2 |  | adobroka |  |
|  Implement time class | 2 | 2 | 2 |  | sselig |  |
|  Change courses to use time class | 4 | 5 | 5 |  | sselig |  |
|  Fix issues from Issue tracker | 12 | 11 | 11 |  | twatzl |  |
|  Fix issues from Issue tracker | 12 | 12 | 12 |  | adobroka |  |
|  Fix file save/load issues | 5 | 5 | 5 |  | sselig |  |
|  Finish x-mas release documents | 5 | 5 | 5 |  | sselig |  |
|  Add Help texts for timetables plus review help texts. | 1 | 1 | 1 |  | sselig |  |
|  Fixing issues from Issue tracker | 4 | 4 | 4 |  | sselig |  |
|  Testing | 6 | 6 | 6 |  | sselig |  |
|  Testing | 6 | 5 | 5 |  | twatzl |  |
|  Testing | 6 | 6 | 6 |  | adobroka |  |