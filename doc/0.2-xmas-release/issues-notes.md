| ID         | Status         | Title                                                                                       | Assignee         | 
|------------|----------------|---------------------------------------------------------------------------------------------|------------------| 
| 117        | new            | Vehicles of a Bus Line do not follow the route                                              | twatzl           | 
| 116        | new            | When removing the vehicle from a timetable the vehicles keep on moving                      | stefanselig      | 
| 115        | new            | When setting the interval of a timetable too many vehicles are created                      | stefanselig      | 
| 114        | new            | When deleting a bus line which has vehicles assigned the vehicles continue to move.         | stefanselig      | 
| 113        | new            | Vehicles which are assigned to a Bus line will not follow the line properly.                | twatzl           | 
| 112        | new            | When deleting a vehicle which is assigned to a timetable, vehicles will still appear        | stefanselig      | 
| 111        | new            | When setting the start time of a timetable without setting a new interval it is ignored     | stefanselig      | 
| 110        | new            | Loading a map file crashes the application and does not work                                | Andreas_Dobroka  | 
| 109        | new            | After saving a map in the MapEditor an exception is thrown when any button is pressed.      | Andreas_Dobroka  | 
| 108        | new            | Exception when resizing a zone in the MapEditor                                             | Andreas_Dobroka  | 
| 107        | new            | Use higher contrast colors for the buttons in the map editor.                               | Andreas_Dobroka  | 
| 106        | new            | Stations are not deleted from courses when they are deleted from a line.                    | stefanselig      | 
| 103        | new            | Create station is sometimes buggy                                                           | Andreas_Dobroka  | 
| 101        | new            | When deleting a vehicle which is currently in use, the moving vehicles will not be deleted. | stefanselig      | 
| 100        | new            | Vehicle deviate from the lines, when stations are deleted while vehicles are moving.        | twatzl           | 
| 99         | new            | When deleting a station, occasionally a line will not be removed.                           | twatzl           | 
| 93         | open           | Exception when drawing a zone in the map editor                                             | Andreas_Dobroka  | 
| 92         | new            | Clicking on stations does not always work on the first try.                                 | Andreas_Dobroka  | 
| 73         | new            | Deassigning stations from a Line, marker.                                                   | Andreas_Dobroka  | 
| 79         | new            | Refactor Map Editor's "add zone"-buttons to a dropdown menu.                                | Andreas_Dobroka  | 
| 88         | new            | Vertically center the button for help texts.                                                | Andreas_Dobroka  | 
| 76         | invalid        | Deleting a station  doesn't delete a drawed line.                                           | twatzl           | 
| 74         | new            | Misbehavior when changing vehicle properties                                                | twatzl           | 
| 72         | new            | Assigning a station does not create a marker on the map immediately.                        | twatzl           | 
| 71         | new            | Line color is not updated immediately.                                                      | twatzl           | 
| 46         | new            | Vertically center the icons in the buttons                                                  | Andreas_Dobroka  | 
| 11         | new            | Make the stations in a line reorderable                                                     | null             | 