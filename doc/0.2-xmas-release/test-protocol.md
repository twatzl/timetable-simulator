|  **TestId** | **Test Name** | **Expected Result** | **Comment** | **Test run from 29.10.15** | **Test run from 30.09.15** | **Test run from 11.12.2015 (a5a5cda)** | **Test run from 17.12.2015 (ec165b5)** |
|  ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
|  **TC01** | **Add Station** | Station is added and displayed on map. |  | failed | failed | passed | passed |
|  **TC02a** | **Add train line** | Line is added to the model (repository) and selectable through the list in the lines tab. |  | passed | passed | passed | passed |
|  **TC02b** | **Add bus line** | Line is added to the model (repository) and selectable through the list in the lines tab. |  |  |  | passed | passed |
|  **TC03a** | **Add station to train line** | Line is shown between the selected stations immidiately |  | failed | failed | failed | passed |
|  **TC03b** | **Add station to bus line** | Line is shown between the selected stations immidiately |  |  |  | failed | passed |
|  **TC04a** | **Remove station from train line** | Station is removed from line and drawed line is removed from the map. |  | failed | passed | failed | passed |
|  **TC04b** | **Remove station from bus line** | Station is removed from line and drawed line is removed from the map. |  |  |  | failed | passed |
|  **TC05** | **Remove line** | Line is removed and drawed line is also removed from map. |  | failed | passed | passed | passed |
|  **TC06** | **Edit line** | The properties of a line are the same after switching away and back. |  | passed | passed | passed | passed |
|  **TC07** | **Remove station** | Station is removed from the map. |  | passed | passed | passed | passed |
|  **TC08** | **Editing a station** | The properties of a station are the same after switching away and back. |  | failed | passed | passed | passed |
|  **TC09** | **Remove a station that is part of a line** | The station is removed from the map and the line is redrawn without the station. |  |  |  | failed | passed |
|  **TC10** | **Station editing should only be possible in Stations tab** | When switching to another tab station dragging should no longer be possible. |  |  |  | passed | passed |
|  **TC11** | **Delete station while vehicles are moving along the line** | The vehicles move along the new path rather than the old one. | Issue #100 |  |  | failed | failed |
|  **TC12** | **Station add mode should be left when switching tabs** | It should not be possible to add stations anymore. |  |  |  | passed | passed |
|  **TC13** | **Delete vehicle which is assigned to a timetable** | Vehicles should not appear and timetables are set to have no vehicle | Issue #112 |  |  | failed | failed |
|  **TC14** | **Remove station from a bus line (complex pattern)** | The line should have no stations anymore. |  |  |  | passed | passed |
|  **TC15** | **Set the fastclock to the fastest speed** | The simulation happens now with a faster speed, but is still correct (fullfills other TC) |  |  |  | passed | passed |
|  **TC16** | **Pause the fastclock after setting it faster** | The simulation should be paused and no movements (e.g. vehicle) should occur. |  |  |  | passed | passed |
|  **TC17** | **Unpause the simulation** | The simulation should again happen with the set speed. |  |  |  | passed | passed |
|  **TC18** | **Add a timetable and change a value** | A new timetable should be added to the line and displayed when opening the timetable view. |  |  |  | passed | passed |
|  **TC19** | **Set the interval of a timetable** | When opening the timetable view, the interval of the timetable should be set |  |  |  | passed | passed |
|  **TC20** | **Set a vehicle for a timetable** | Vehicles start to move around as soon as the timetable time is reached. |  |  |  | failed | passed |
|  **TC21** | **Change the vehicle for an active timetable** | Vehicle changes without notice. Vehicles which are en route will not change. |  |  |  | passed | passed |
|  **TC22** | **When changing the interval of a timetable, the vehicle should stay the same** | The vehicle which was selected before, will be selected again. |  |  |  | passed | passed |
|  **TC23** | **Change the color of a line** | The line is immidiately drawn with the new color. | Issue #71 |  |  | failed | failed |
|  **TC24** | **Move a station which is assigned to a bus line** |  |  |  |  | failed | passed |
|  **TC25** | **Move a station which is assigned to a train line** |  |  |  |  | passed | passed |
|  **TC26** | **Move a station which is assigned to a bus line where vehicles move** |  |  |  |  | failed | blocked |
|  **TC27** | **Move a station which is assigned to a train line where vehicles move** |  |  |  |  | passed | passed |
|  **TC28** | **Add a vehicle** | Vehicle is added and displayed in the vehicle list. |  |  |  | passed | passed |
|  **TC29** | **Change the acceleration of a vehicle which is currently in use** | The vehicles which move on the map should now use the new acceleration. |  |  |  | passed | passed |
|  **TC30** | **Change the maximum speed of a vehicle which is currently in use** | The vehicles which move on the map should now use the new max speed. |  |  |  | passed | passed |
|  **TC31** | **Remove a vehicle from a timetable** | The timetable has no vehicle set anymore and is ignored (no vehicles are driving) | Issue #116 |  |  | blocked | failed |
|  **TC32** | **Create zones in the map editor** | A zone of each type should be visible on the map. The zones can be distinguished by their color. | Issues #93 #107 |  |  | failed | failed |
|  **TC33** | **Remove zones in the map editor** | The removed zones should no longer be shown and not saved when saving the map. |  |  |  | blocked | passed |
|  **TC34** | **Switch to the edit zone mode in the map editor** | The user should now be able to resize and move zones. | Issue #108 |  |  | failed | failed |
|  **TC35** | **Switch to the lock zones mode in the map editor** | The user should no longer be able to resize or move zones, but be able to navigate on the map. |  |  |  | blocked | passed |
|  **TC36** | **Save a map to a file** | A file containing the map should now exist. |  |  |  | blocked | passed |
|  **TC37** | **Load a map from a file** | The map state should be the same as when it was saved to the file. | Issue #110 |  |  | blocked | failed |
|  **TC38** | **Delete a train line which has vehicles assigned** | The vehicles should be gone away |  |  |  | failed | passed |
|  **TC39** | **Delete a bus line which has vehicles assigned** | The vehicles should be gone away | Issue #102 |  |  | failed | failed |
|  **TC40** | **Lock Zone editing mode after saving** | The user should now be able to resize and move zones. | Issue #109 |  |  |  | failed |
|  **TC41** | **Enter Edit Zones editing mode after saving** | The user should no longer be able to resize or move zones, but be able to navigate on the map. | Issue #109 |  |  |  | failed |
|  **TC42** | **Set the interval and vehicle of a timetable and then set start time to a different time** | Vehicles should start moving once the start time is reached. | Issue #111 |  |  |  | failed |
|  **TC43a** | **Assign vehicles to a Bus Line** | The vehicles follow the route of the line | Issue #117 |  |  |  | failed |
|  **TC43b** | **Assign vehicles to a Train Line** | The vehicles follow the route of the line |  |  |  |  | passed |
|  **TC44** | **Remove a timetable while vehicles are moving** | The vehicles are removed from the map |  |  |  |  | passed |