###TC01

1. Click station tab
2. Click add station
1. Click on map

-----------------

###TC02

1. "Add line" button is clicked.

-----------------

###TC03 

1. Line is selected
1. Add station is clicked
3. Click on two different stations on the map
4. Edit Stations is clicked again
5. Click on any station again

-----------------

###TC04

1. Select Line
1. Click "Remove station" button
1. Click the station to be removed on the map.

-----------------

###TC05

1. Select line
1. Click "Remove line" button

-----------------

###TC06

1. Select line
1. Type in new properties for the selected line.

-----------------

###TC07

1. Select Station tab
1. Select Station to be deleted
1. Click "Remove Station"

-----------------

###TC08

1. Select Station tab
1. Click on the Stations' properties
1. Type in new properties for the selected station.

-----------------

###TC09

1. create line
1. create some stations (>3)
1. add stations to line
1. display line
1. remove a station via the stations tab

-----------------

###TC10

1. switch to stations tab and add a station
1. switch to another tab
1. click on the station

-----------------

###TC11

1. Create a line
1. assign multiple stations (>3)
1. create a timetable with a course
1. assign a vehicle to the course
1. delete a station

-----------------

###TC12

1. press add station
1. add a station (optional)
1. switch to another tab (i.e. lines tab)
1. click in the map

-----------------

###TC13

1. create a vehicle, a line with some stations and a timetable with course for the line
1. assign a vehicle to the course
1. delete the vehicle in the vehicles tab

-----------------

###TC14

1. Add 3 station on the map.
1. Now add the 3 station to a bus line.
1. Remove the second added station.
1. Remove the first added station.
1. Now remove the third station.

-----------------

###TC15

1. select a line
2. edit timetable
3. set interval to 15
4. select a vehicle
5. click ok
6. set to fastclock to fastest speed

-----------------

###TC16

1-6. same as TC15
7. pause the fastclock

-----------------

###TC17

1-6. same as TC15
7. pause the fastclock
8. set the fastclock to the normal speed again

-----------------

###TC18

1. Select a line
1. click 'edit timetables'
1. click 'add timetable'
1. close the dialog
1. click 'edit timetables' again

-----------------

###TC19

1. edit timetable
1. set interval
1. accept timetable

-----------------

###TC20

1. edit timetable
1. set interval and vehicle
1. accept timetable

-----------------

###TC21

1. edit timetable
1. set interval and vehicle
1. accept timetable
1. wait
1. open timetable again
1. set different vehicle
1. accept

-----------------

###TC22

1. edit timetable
1. set interval and vehicle
1. accept
1. edit timetable again
1. set interval
1. accept
1. edit timetable again

-----------------

###TC23

1. create a line which has two station assigned
2. change the color of the line

-----------------

###TC24

1. create a bus line which has 3 stations assigned
2. move one of the stations around

-----------------

###TC25

1. create a train line which has 3 stations assigned
2. move one of the stations around

-----------------

###TC26

1. create a bus line which has 3 stations assigned
2. move one of the stations around

-----------------

###TC27

1. create a train line which has 3 stations assigned
2. move one of the stations around

-----------------

###TC28

1. click on the add vehicles tab
2. click add vehicle

-----------------

###TC29

1. create a line and add a timetable so that a vehicle moves
2. change the acceleration of this vehicle

eventually this test must be combined with tc30 to see clear results

-----------------

###TC30

1. create a line and add a timetable so that a vehicle moves
2. change the max speed of this vehicle

eventually this test must be combined with tc29 to see clear results

-----------------

###TC31

1. edit timetable
1. set interval and vehicle
1. accept
1. wait for the vehicles to appear
1. edit timetable again
1. click remove vehicle

-----------------

###TC32

Repeat the following procedure for each of the 9 zone buttons:
1. click the create zone button
2. drag on the map and define an area

-----------------

###TC33

1. click a create zone button
2. drag on the map and define an area
3. right click on the zone to delete it

-----------------

###TC34

1. create two or more zones in the map editor
2. press the edit zones button
3. try to resize and drag a zone


-----------------

###TC35

1. create two or more zones in the map editor
2. press the lock zones button
3. try to rezise and drag a zone

3a. also try to switch between edit zones and lock zones mode

-----------------

###TC36

1. create one zone of resenditial, commercial and industrial
2. click the save button
3. choose a file name
4. press save

-----------------

###TC37

requires tc36 to be successful

1. click open file
2. choose a file
3. press open

-----------------

###TC38

1. create a train line and add a timetable so that a vehicle moves
2. delete the line

-----------------

###TC39

1. create a bus line and add a timetable so that a vehicle moves
2. delete the line

-----------------

###TC40

1. create one zone of resenditial, commercial and industrial
2. click the save button
3. choose a file name
4. press save
5. now click on the Lock Zones button

-----------------

###TC41

1. create one zone of resenditial, commercial and industrial
2. click the Lock Zones button
3. click the save button
4. choose a file name
5. press save
6. now click on the Edit Zones button

-----------------

###TC42

1. select a line
2. click on edit timetables
3. set start time to a distant future time
4. set interval to 10
5. set vehicle
6. close timetable edit view by pressing ok button
7. open timetable edit view again
8. set start time to a time which will soon be
9. click ok and wait for the time

-----------------

###TC43

1. select a line
2. click on edit timetables
3. set start time to a close future time
4. set interval to 10
5. set vehicle
9. click ok and wait for the time

-----------------

###TC44

1. edit timetable
1. set interval and vehicle
1. accept
1. wait for the vehicles to appear
1. edit timetable again
1. click remove timetable