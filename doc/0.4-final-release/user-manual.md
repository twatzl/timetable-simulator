#Documentation/Manual
by Andreas Dobroka, Stefan Selig, Tobias Watzl

#Content:
- General Explanation of PTS User Interface
- Lines
- Timetables
- Vehicles
- Stations
- MapEditor/Zones
- Clock
- File load/save
- Finance System
- Population Simulation

#The program’s GUI in general:

![General game view](../images/general_game_view.png "General game view")

##Map-View:
- On the left side, the google map is visible. The user can drag and zoom like in the normal google maps application.

##Control-View: 
- On the right side, there is the gui for the gameplay. 
- On the upper right side there are the tabs for the different gameplay parts (Lines, Vehicles, Stations). 
- Under the tabs there are buttons for adding new objects to the game. 
- Under the add buttons there is a list of all available objects and under that there is the detail view for the selected object.
- Additional gameplay buttons view: That’s the bar at the bottom of the game window. There are additional buttons for controlling the clock and options.

#Lines:

![Lines tab](../images/lines_tab.png "Lines tab")

- **Add bus line:** A bus line will be added to your Lines. A bus line is bound on streets. The line’s way is calculated through google maps routing algorithms.
- **Add train line:** A train line will be added to your Lines. A train line is not bound on streets. The direct way from A to B will be used.
- **The Lines list:** All the Lines are displayed in this list. Pressing the “x”-button deletes the Line from your Lines.
- **The detail view:** After selecting a Line, the Line’s details are shown and can be edited. Just type in the text you want, it will be updated immediately, there’s no save button.
- **Id:** The Line’s unique identification number
- **Name:** The Line’s name. Needs not be unique.
- **Color:** The displayed Line color in the map.
- **The “Edit Timetables”-button:** Opens a new modal window for controlling the Line’s timetables. Will be explained in chapter “Timetables”.
- **The Stations list:** All stations that are assigned to the selected Line are displayed in the Stations list. Pressing the “x”-button deletes a station.
- **The “Assign Station” button:** Assigns a station to the selected Line. For doing that press the button then click on the station to be assigned on the google map.
- **The “Remove Station Assignment” button:** Removes the assignment of a station to a line. For doing that press the button  and click on the station to be removed from the line on the google map. (It does the same as pressing the “x-button” in the selected Line’s station list.)

![Lines example](../images/example_line.png "Line example")

An example for a train line with 3 assigned stations colored in red.

#Timetables:

![Timetable View](../images/timetable_view.png "Timetable view")

- **Valid From:** The date the timetable starts from.
- **Valid To:** The date the timetable runs to.
- **Begin Time:** The time the timetable starts each day.
- **End Time:** The time the timetable runs to each day.
- **Valid Week Days:** The week days the timetable is active each week.
- **Remove Timetable:** Removes the selected timetable from your timetables.
- **Set Interval:** Sets the interval the timetable courses are repeated. That means every x minutes a vehicle will start the course of the line.
- **Set Vehicle:** Assigns a vehicle to the timetable’s courses.
- **Add Timetable:** Adds a new timetable to your timetables.

#Vehicles:

![Vehicle Tab](../images/vehicle_tab.png "Vehicle tab")

- **Add Vehicle:** A Vehicle is added to your Vehicles.
- **Vehicles List:** A list of your vehicles.
- **Vehicle Details View:**
	- **Id:** The vehicle’s unique Identifier
	- **Name:** The name of the vehicle. Needs not be unique.
	- **Capacity:** How many people the selected vehicle can transport.
	- **Max Speed:** How fast the selected Vehicle is. The value is used in the map when the Vehicle moves.
	- **Acceleration:** How fast the Vehicle can accelerate. The value is used in the map when the Vehicle moves.
	- **Length:** The length of the Vehicle.
	- **Type:** The type of the Vehicle.

![Vehicle Example](../images/vehicle_example.png "Vehicle Example")

In this picture you can see two lines with vehicles, one is a Streetbound Line that is positioned in the streets where a Bus moves, the other one is an Unbound Line where a Train moves.

#Stations:

![Stations Tab](../images/stations_tab.png "Stations tab")

- **Add Station:** A station will be added to your stations. Just press the button and click on the map on the position you want your new station to be.
- **Stations list:** Your stations are displayed in the list. Clicking the “x” Button deletes a station. It will also be removed from the map.
- **The stations detail view:** Properties of your selected station can be edited here:
- **Id:** The unique identifier of your station.
- **Name:** Your station’s name.
- **Size:** The size of your station that is displayed on the map.

Clicking a station in the station list recolors the station in the map for better usability.

![Stations Example](../images/stations_example.png "Stations Example")

A line with 3 stations is displayed on the map in this picture.

#MapEditor/Zones:

![MapEditor View](../images/general_mapeditor_view.png "MapEditor Example")

- **Create Large Residential Zone:** Creates a large residential zone. That's an area where people live.
- **Create Medium Residential Zone:** Creates a medium residential zone. That's an area where people live.
- **Create Small Residential Zone:** Creates a small residential zone. That's an area where people live.
- **Create Large Industry Zone:** Creates a large Industry zone. That's an area where people can work.
- **Create Medium Industry Zone:** Creates a medium Industry zone. That's an area where people can work.
- **Create Small Industry Zone:** Creates a small Industry zone. That's an area where people can work.
- **Create Large Commercial Zone:** Creates a large Commercial zone. That's an area where people can work, but it's also possible that people live there.
- **Create Medium Commercial Zone:** Creates a medium Commercial zone. That's an area where people can work, but it's also possible that people live there.
- **Create Small Commercial Zone:** Creates a medium Commercial zone. That's an area where people can work, but it's also possible that people live there.
- **Edit Zones:** When this button is activated, you can move and resize zones on the map.
- **Lock Zones:** When this button is activated, the zones are locked, but you can move the map.
- **Save:** Opens a File dialog for saving your game.
- **Open File:** Opens a File dialog for loading your game.
- **Help Button:** When this button is clicked, you can move among the game elements and an info window will show up to give you information what it does.

![Zones Example](../images/zones_example.png "Zones Example")

In this picture you can see a commercial zone colored blue and a residential zone colored green.

#Zones in the main game

- Load Zones button in the options dropdown: Loads a zone file into the game
- Show Zones/Hide Zones: You can toggle to show zones in the map or hide them

![Zones in the Main Game](../images/zones_view.png "Zones in the Main Game")

#Clock:

The clock shows the time in the game. 
It runs faster so that one day in the game does not last like one day in real life.

![Clock Example](../images/clock_example.png "Clock Example")

- **Pause:** Pauses the clock.
- **Play:** Plays the clock at normal speed.
- **PlayFast:** Plays the clock at fast speed.
- **PlayFaster:** Plays the clock at extremely fast speed.

#File load/save:

Saving a loading files is very easy in our application. 
Just click the “Options” button and choose whether you want to save or load a game.

![File Save Example](../images/save_game.png "File Save Example")

![Options Example](../images/options_example.png "Options Example")

#Finance System

Of course a public transport company has expenses as well as income.
There are several game actions that have some finance handeling.
If there isn't enough money left, the transaction fails.

Those are:

- **Buying Stations:** Buying a station costs a fixed amount of money
- **Buying Lines:** Buying a line costs a fixed amount of money
- **Assigning a Station to a Line/Moving a station that's part of a Line:** This costs money depending on the distance
- **Operating a Vehicle:** Costs money per minute and depends on vehicle properties e.g. velocity, acceleration, etc.
- **Finance View:**
	- Balance: How much money there's left to spend
	- Ticket Price: How much inhabitants will have to pay
	- Company Image: Your company's image in the public

![Finance View](../images/finance_view.png "Finance View")

# Population Simulation

As soon as zones are loaded into the Game, the population simulation will start.
Simulation means a population is calculated in the background and people want to get from a source zone to a destination zone.

The main parts of the simulation are:

- ** Person Simulation **
- ** Person Routing **
- ** PublicTransportRouting **
- ** Calculating Zone Demand **