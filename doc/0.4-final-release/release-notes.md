This is the final release of our project. Some minor bugfixes will eventually follow, but 
We switched programming languages from C++ to HTML/JavaScript.
We now use Electron, Angular, Bootstrap and Typescript as our technologies.

Features currently available include:
- A Google Map
- User can create and delete lines
- User can create and delete stations
- Stations are shown on the map
- Lines are shown on the map
- Stations' properties can be changed
- Lines' properties can be changed
- Stations can be added/removed to/from a Line
- Userfriendly design via Bootstrap
- A working finance system

# Changes to previous release

In the version 0.4 of our project our project now includes population simulation. In the background we have simplified the handling of vehicles and timetables in order to respond to the changed requirements.

Also the Gulp release tasks are working again.

# Features

## Stations
### Creating And Removing Stations

The user can create and remove stations. Created stations are then shown on the map and can be assigned to lines.

### Editing Station Properties

Stations have properties which play a role in the simulation. These can be edited by the user in the stations tab.

## Vehicles

Vehicles are "templates" to be assigned to timetables. Vehicles can be created and removed. Furthermore the user can edit the properties of the vehicles, which are used for the simulation.

## Lines
### Creating And Removing Lines

The user can create and remove lines. Lines connect stations and have timetables assigned. 

#### Bus Lines (Street Bound Lines)

The first type of lines is the bus line. As busses can only operate on streets, this lines follow the road.

#### Train Lines (Unbound Lines)

The second type of lines are train lines. As train tracks can be built anywhere (at least we assume so in our simplified model) these lines provide the direct connection between stations. Also with train lines it is possible to use stations which are not next to a road.

### Assigning Stations To A Line

In order to set the route of a line the user can assign stations to it. Vehicles drive to the stations in the order the stations were assigned to the line.

### Manage Timetables
	
The user can manage individual timetables for each line. Each line can have multiple timetables which are valid for certain times at certain days (e.g. 9 to 5 on Monday to Friday). For each timetable an intervall can be set. Vehicles then operate, so that the intervalls are met.

### Assigning Vehicles To Lines

Lines would have no benefit, if there were no vehicles to drive along them and pick up passengers. Vehicles can be assigned to a line in the timetable view. For each timetable a different vehicle can be chosen. The game then generates enough vehicles to meet the intervall which was set for the timetable.

## Simulation Of Time (Fast Clock)

The game simulates a game time which is independent of the real time. Unlike real time it can be accelerated via time compression or paused. All simulations which happen in the game are accelerated or pause accordingly.

## Finance System

The game now also simulates a finance system which consists of a bank account that represents the money which is available to the players company. Every time when when an action is conducted by the player (such as buying a station, buying a line etc.) money is taken from this bank account. Once the simulation is finished the player will earn money by transporting passengers. Additionally the player has to pay a fee for operating his vehicles.

## Simulation of Population

Depending on size and density of a zone the number of people living there and the number of jobs provided is determined.

For the simulation only a small fraction of the inhabitants is simulated and shown on the map. These spawn randomly in the source zone and hava a random target in the destination zone. The source and destination zone are choosen by several factors including number of available jobs and distance. Depending on the current time of day it is more or less likely for inhabitants to spawn and they spawn in different areas. (morning residential -> industrial/commercial; evening industrial/commercial -> residential) These can also differ for certain times of the year e.g. holidays.

After spawning inhabitants calculations determine if he goes by foot, by car or uses public transport. This depends on the time that a transport system takes and on the image the company has.

When choosing public transport the person walks to a nearby station and waits for a vehicle to come by.
Vehicle instances will have to be extended with a capacity.

## Save And Load Games

The "Save Game" and "Load Game" can be used to save and load games. When the user clicks on one of those buttons a file chooser dialog gets opened and the user selects the file he wants to save to or load from. Then the whole game state is saved/loaded from/to this file.

## Map Editor

Additionally to the game there is a Map Editor, which serves the purpose to add information about the population to the Google Map. This information is then used by the simulation of the game, in order to simulate the inhabitants of a city.

### Defining Zones

In the Map Editor zones can be defined. These zones are used for the simulation of the population of a city. There are three types of zones: Residential, Commercial and Industrial.

- The residential zones provide homes for the people.
- The commercial zones provide jobs, but also offer shopping opportunities.
- The industrial zones do only provide jobs.

For each zone there is a sparse, medium and a dense version available which are then handled differently in the simulation in terms of the number of homes, jobs, etc.

### Saving Defined Zones To A Map

Similar to loading and saving of game states in the main game it is possible to save a map which was created in the map editor.

## Zones in the Main Game

Now it's possible to load zones via a file into the main game. Zones can be hidden and shown for better usability.

# Test Results

In this release we increased the number of test. The weeks before the release were mainly a bugfixing time. Despite our efforts it was not possible to fix all the bugs. Approximately 75% of our 48 test cases still fail,
but the detailed information in the test report shows, that we are making progress to a more stable application.

|  **TestId** | **Test Name** | **Expected Result** | **Test run from 01.04.2016 (df346fa)** |
|  ------ | ------ | ------ | ------ |
|  **TC01** | **Add Station** | Station is added and displayed on map. | passed |
|  **TC02a** | **Add train line** | Line is added to the model (repository) and selectable through the list in the lines tab. | passed |
|  **TC02b** | **Add bus line** | Line is added to the model (repository) and selectable through the list in the lines tab. | passed |
|  **TC03a** | **Add station to train line** | Line is shown between the selected stations immidiately | passed |
|  **TC03b** | **Add station to bus line** | Line is shown between the selected stations immidiately | passed |
|  **TC04a** | **Remove station from train line** | Station is removed from line and drawed line is removed from the map. | passed |
|  **TC04b** | **Remove station from bus line** | Station is removed from line and drawed line is removed from the map. | passed |
|  **TC05** | **Remove line** | Line is removed and drawed line is also removed from map. | passed |
|  **TC06** | **Edit line** | The properties of a line are the same after switching away and back. | passed |
|  **TC07** | **Remove station** | Station is removed from the map. | passed |
|  **TC08** | **Editing a station** | The properties of a station are the same after switching away and back. | passed |
|  **TC09a** | **Remove a station that is part of a train line** | The station is removed from the map and the line is redrawn without the station. | passed |
|  **TC09b** | **Remove a station that is part of a bus line** | The station is removed from the map and the line is redrawn without the station. | passed |
|  **TC10** | **Station editing should only be possible in Stations tab** | When switching to another tab station dragging should no longer be possible. | passed |
|  **TC11** | **Delete station while vehicles are moving along the line** | The vehicles move along the new path rather than the old one. | failed |
|  **TC12** | **Station add mode should be left when switching tabs** | It should not be possible to add stations anymore. | passed |
|  **TC13** | **Delete vehicle which is assigned to a timetable** | Vehicles should not appear and timetables are set to have no vehicle | passed |
|  **TC14** | **Remove station from a bus line (complex pattern)** | The line should have no stations anymore. | passed |
|  **TC15** | **Set the fastclock to the fastest speed** | The simulation happens now with a faster speed, but is still correct (fullfills other TC) | passed |
|  **TC16** | **Pause the fastclock after setting it faster** | The simulation should be paused and no movements (e.g. vehicle) should occur. | passed |
|  **TC17** | **Unpause the simulation** | The simulation should again happen with the set speed. | passed |
|  **TC18** | **Add a timetable and change a value** | A new timetable should be added to the line and displayed when opening the timetable view. | passed |
|  **TC19** | **Set the interval of a timetable** | When opening the timetable view, the interval of the timetable should be set | passed |
|  **TC20** | **Set a vehicle for a timetable** | Vehicles start to move around as soon as the timetable time is reached. | passed |
|  **TC21** | **Change the vehicle for an active timetable** | Vehicle changes without notice. Vehicles which are en route will not change. | passed |
|  **TC22** | **When changing the interval of a timetable, the vehicle should stay the same** | The vehicle which was selected before, will be selected again. | failed |
|  **TC23** | **Change the color of a line** | The line is immidiately drawn with the new color. | failed |
|  **TC24** | **Move a station which is assigned to a bus line** |  | passed |
|  **TC25** | **Move a station which is assigned to a train line** |  | passed |
|  **TC26** | **Move a station which is assigned to a bus line where vehicles move** |  | failed |
|  **TC27** | **Move a station which is assigned to a train line where vehicles move** |  | failed |
|  **TC28** | **Add a vehicle** | Vehicle is added and displayed in the vehicle list. | passed |
|  **TC29** | **Change the acceleration of a vehicle which is currently in use** | The vehicles which move on the map should now use the new acceleration. | failed |
|  **TC30** | **Change the maximum speed of a vehicle which is currently in use** | The vehicles which move on the map should now use the new max speed. | passed |
|  **TC31** | **Remove a vehicle from a timetable** | The timetable has no vehicle set anymore and is ignored (no vehicles are driving) | blocked (another feature makes this test case obsolete) |
|  **TCM32** | **Create zones in the map editor** | A zone of each type should be visible on the map. The zones can be distinguished by their color. | passed |
|  **TCM33** | **Remove zones in the map editor** | The removed zones should no longer be shown and not saved when saving the map. | passed |
|  **TCM34** | **Switch to the edit zone mode in the map editor** | The user should now be able to resize and move zones. | passed |
|  **TCM35** | **Switch to the lock zones mode in the map editor** | The user should no longer be able to resize or move zones, but be able to navigate on the map. | passed |
|  **TCM36** | **Save a map to a file** | A file containing the map should now exist. | passed |
|  **TCM37** | **Load a map from a file** | The map state should be the same as when it was saved to the file. | passed |
|  **TC38** | **Delete a train line which has vehicles assigned** | The vehicles should be gone away | failed |
|  **TC39** | **Delete a bus line which has vehicles assigned** | The vehicles should be gone away | failed |
|  **TCM40** | **Lock Zone editing mode after saving** | The user should now be able to resize and move zones. | passed |
|  **TCM41** | **Enter Edit Zones editing mode after saving** | The user should no longer be able to resize or move zones, but be able to navigate on the map. | passed |
|  **TC42** | **Set the interval and vehicle of a timetable and then set start time to a different time** | Vehicles should start moving once the start time is reached. | passed |
|  **TC43a** | **Assign vehicles to a Bus Line** | The vehicles follow the route of the line | passed |
|  **TC43b** | **Assign vehicles to a Train Line** | The vehicles follow the route of the line | passed |
|  **TC44** | **Remove a timetable while vehicles are moving** | The vehicles are removed from the map | failed |
|  **TC45** | **Select Station and Switch to other tab** | The Station is no longer highlighted on the map | passed |
|  **TCF01a** | **Move an assigned Station (bus)** | A popup window should popup if station is assigned to a bus line. | passed |
|  **TCF01a** | **Move an assigned Station (train)** | A popup window should popup if station is assigned to a train line. | passed |
|  **TCF02** | **Finance system decrease money depended on distance the station was moved** | Fincance system decrease money depending on moved distance and show sucess or failure. | passed |
|  **TCF03a** | **Press undo button in move popup window when moving a station assigne to a bus line** | Station should jump back to original position. <br/>No transaction should happen and the drawed line should jump back to the original position. | passed |
|  **TCF03b** | **Press undo button in move popup window when moving a station assigne to a bus line** | Station should jump back to original position. <br/>No transaction should happen and the drawed line should jump back to the original position. | passed |
|  **TCF04** | **Finance system check money availability** | The finance system check whether enought money is available and decrease the money. | passed |
|  **TCF05** | **Finance system show message** | The finance system show sucess or failure, depending whether enought money is available. | passed |
|  **TCF06** | **Press apply button in move popup window** | The station set his new position and line path is changed depending on the new position of the moved station. | passed |
|  **TCF08** | **Create bus line** | Money is subtracted from the company account | passed |
|  **TCF09** | **Create train line** | Money is subtracted from the company account | passed |
|  **TCF10** | **Not enought money by creating bus line.** | Show error message and not reduce the money. | passed |
|  **TCF11** | **Not enought money by creating tram line.** | Show error message and not reduce the money. | passed |

# Known Issues

As of releasing this version the following issues are known to exist but there is no fix available. Further details can be found in the issues section of the Bitbucket repository. The known issues are mainly issues with usability and user interface.

As programming the simulation was extremely complex (e.g. uses AStar Routing Algorithm), some bugs might occur while using it, but in theory simulation works.

| ID  | Status | Title                                                                                | Assignee        |
|-----|--------|--------------------------------------------------------------------------------------|-----------------|
| 100 | new    | Vehicle deviate from the lines, when stations are deleted while vehicles are moving. | twatzl          |
| 144 | new    | Fix navbar when resizing window                                                      | twatzl          |
| 163 | new    | Changing the interval after timetable has been changed does not work.                | stefanselig     |
| 150 | new    | Save Simulation Data when saving the game                                            | stefanselig     |
| 11  | new    | Make the stations in a line reorderable                                              | Andreas_Dobroka |
| 147 | new    | Move ObservableEvent and LockedEvent to Utilties/Events/                             | twatzl          |
| 148 | new    | Move all controllers to a separate controller directory                              | twatzl          |
| 149 | new    | Move all views to a separate views directory                                         | twatzl          |
| 116 | new    | When removing the vehicle from a timetable the vehicles keep on moving               | stefanselig     |
| 73  | new    | Deassigning stations from a Line, marker.                                            | Andreas_Dobroka |
| 74  | new    | Misbehavior when changing vehicle properties                                         | twatzl          |
| 72  | new    | Assigning a station does not create a marker on the map immediately.                 | twatzl          |
| 71  | new    | Line color is not updated immediately.                                               | twatzl          |

# State of the Code

Unfortunately the current state of the code is not as good as we would like it to be. The short amount of time available to finish the simulation and finance system required to concentrate our workforce on these topic. Refactoring of code and simplifications have been postponed until we had no time left anymore. The only exceptions are the unused parts of the code which have been removed for this release. Also the planned implementation of unit tests was cancelled in order to save time.