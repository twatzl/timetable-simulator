'use strict';

var gulp = require('gulp'),
    tsc = require('gulp-typescript'),
    sourcemaps = require('gulp-sourcemaps'),
    del = require('del'),
	runSequence = require('run-sequence'),
    Config = require('./gulpfile.config'),
	Helper = require('./typescript-build-functions.js'),
	childProcess = require('child_process'),
	electron = require('electron-prebuilt'),
	yargs = require('yargs'),
	packager = require('electron-packager');
	
// we need to have multiple projects otherwise
// typescript compiler might deliver unwanted results
var tsProjectGame = tsc.createProject('tsconfig.json');
var tsProjectMapEditor = tsc.createProject('tsconfig.json');

var config = new Config();
var helperGame = new Helper(gulp, tsc, sourcemaps, del, config, tsProjectGame);
var helperMapEditor = new Helper(gulp, tsc, sourcemaps, del, config, tsProjectMapEditor);

/**
 * Compile TypeScript and include references to library and app .d.ts files.
 */
gulp.task('compile-ts-game', function (cb) {
    return helperGame.compileTypescript(config.allTypeScript, config.gamePath + config.tsOutputPath, cb);
});

gulp.task('compile-ts-mapeditor', function (cb) {
	return helperMapEditor.compileTypescript(config.allTypeScript, config.mapEditorPath + config.tsOutputPath, cb);
})

gulp.task('compile-ts-test', function (cb) {
	return helperMapEditor.compileTypescript(config.allTypeScript, config.testPath + config.tsOutputPath, cb);
})

/**
 * Copy all partial html files from the src folder to js folder.
 */
gulp.task('copy-partials-game', function (cb) {
    return helperGame.copyPartialViews(config.source, config.gamePath + config.partialsPath, cb);
});

gulp.task('copy-partials-mapeditor', function (cb) {
	return helperMapEditor.copyPartialViews(config.source, config.mapEditorPath + config.partialsPath, cb);
})

gulp.task('copy-partials-test', function (cb) {
	return helperMapEditor.copyPartialViews(config.source, config.testPath + config.partialsPath, cb);
})

/**
 * Copy all JavaScript files from the src folder to js folder.
 */
gulp.task('copy-js-game', function (cb) {
    return helperGame.copyJavascript(config.allJavaScript, config.gamePath + config.jsOutputPath, cb);
});

gulp.task('copy-js-mapeditor', function (cb) {
	return helperMapEditor.copyJavascript(config.allJavaScript, config.mapEditorPath + config.jsOutputPath, cb);
})

gulp.task('copy-js-test', function (cb) {
	return helperMapEditor.copyJavascript(config.allJavaScript, config.testPath + config.jsOutputPath, cb);
})

/**
 * Remove all generated JavaScript files from TypeScript compilation.
 */
gulp.task('clean-game', function (cb) {
	return helperGame.cleanFiles(cb, config.gamePath);
});

gulp.task('clean-mapeditor', function (cb) {
	return helperMapEditor.cleanFiles(cb, config.mapEditorPath);
});

gulp.task('clean-test', function (cb) {
	return helperMapEditor.cleanFiles(cb, config.testPath);
});

/**
 * Meta tasks to run all the compile stuff
 */
gulp.task('build-game', ['compile-ts-game', 'copy-partials-game', 'copy-js-game'], function () { });
gulp.task('build-mapeditor', ['compile-ts-mapeditor', 'copy-partials-mapeditor', 'copy-js-mapeditor'], function () { });
gulp.task('build-test', ['compile-ts-test', 'copy-partials-test', 'copy-js-test'], function () { });

gulp.task('clean', ['clean-game', 'clean-mapeditor']);

/**
 * These tasks run the applications.
 */
gulp.task('run-game', [], function () {
	childProcess.spawn(electron, [config.gamePath], { stdio: 'inherit' });
});

gulp.task('run-mapeditor', [], function () {
	childProcess.spawn(electron, [config.mapEditorPath], { stdio: 'inherit' });
})

/**
 * Special tasks for VisualStudio and co.
 */
gulp.task('build'/*, ['build-step-one', 'build-step-two']*/, function () {
	// These tasks cannot be run in parallel but have to be run sequential
	runSequence("build-game", "build-mapeditor");
});

gulp.task('rebuild', function () {
	// These tasks cannot be run in parallel but have to be run sequential
	runSequence("clean", "build");
});

gulp.task('default', ['run-game']);

/**
 * Release tasks.
 */
gulp.task('release-game', function () {
	var directory = yargs.argv.releaseTarget != undefined ? yargs.argv.releaseTarget : config.releaseGameDir;
	
	var options = {
        "dir": config.gamePath,
        "name": config.gameReleaseName,
        "platform": process.platform,
		"overwrite": true,
        "arch": config.release.arch,
        "version": config.release.electronVersion,
        "app-version": config.release.appVersion,
        "version-string": config.release.versionString
    };
	packager(options, function done(err, appPath) { 
		var folderName = config.gameReleaseName + "-" + process.platform + "-" + config.release.arch;
		if (process.platform == "darwin") {
			folderName += '/PublicTransportSystems.app';
		}
		config.nodeModulesNeededForRelease.forEach(function (value, index, array) {
			gulp.src([value]).pipe(gulp.dest(folderName + '/Contents/' + config.nodeModulesNeededForReleasePaths[index]));
		})
		config.otherFilesNeededForRelease.forEach(function (value, index, array) {
			gulp.src([value]).pipe(gulp.dest(folderName + '/Contents/Resources/'));
		});
	});
});

gulp.task('release-mapeditor', function () {
	var directory = yargs.argv.releaseTarget != undefined ? yargs.argv.releaseTarget : config.releaseMapEditorDir;
	
	var options = {
        "dir": config.mapEditorPath,
        "name": config.mapEditorReleaseName,
        "platform": process.platform,
		"overwrite": true,
        "arch": config.release.arch,
        "version": config.release.electronVersion,
        "app-version": config.release.appVersion,
        "version-string": config.release.versionString
    };
	packager(options, function done(err, appPath) { 
		var folderName = config.mapEditorReleaseName + "-" + process.platform + "-" + config.release.arch;
		if (process.platform == "darwin") {
			folderName += '/MapEditor.app';
		}
		config.nodeModulesNeededForRelease.forEach(function (value, index, array) {
			gulp.src([value]).pipe(gulp.dest(folderName + '/Contents/' + config.nodeModulesNeededForReleasePaths[index]));
		})
		config.otherFilesNeededForRelease.forEach(function (value, index, array) {
			gulp.src([value]).pipe(gulp.dest(folderName + '/Contents/Resources/'));
		});
	});
});

gulp.task('release-all', function () {
	runSequence("release-game", "release-mapeditor")
});