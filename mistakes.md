# Andreas Dobroka

app/src/Zones/model/Zone.ts
```
class Zone extends EntityObject implements DrawableMapObject {
	...
	constructor(name: string, type: string, density: string) {
        super(ZoneRepository.getInstance().getNextId());

        this.name = name;
        this.type = type;
        this.density = density;
    }
	...
}
```

Use enums instead of string for type where only certain values are allowed.

# Stefan Selig 

## **2015-11-24**: 90ec27b

 app/src/Lines/model/StreetBoundLine.ts
```
+	public timesBetweenStations: number[];
```

public member without getters and setters.

# Tobias Watzl

to be blamed...