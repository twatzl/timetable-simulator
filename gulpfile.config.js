'use strict';
var GulpConfig = (function () {
    function gulpConfig() {
        //Got tired of scrolling through all the comments so removed them
        //Don't hurt me AC :-)
        this.appRoot = './app/';
        this.source = this.appRoot + 'src/';
		this.releaseDir = './release/';

		this.gameReleaseName = "PublicTransportSystems";
		this.mapEditorReleaseName = "MapEditor";

		this.gameDir = 'Game/';
		this.mapEditorDir = 'MapEditor/';

		this.gamePath = this.appRoot + this.gameDir;
		this.mapEditorPath = this.appRoot + this.mapEditorDir;
		this.testPath = this.appRoot + 'Test/';
		
		this.releaseGameDir = this.releaseDir + this.gameDir;
		this.releaseMapEditorDir = this.releaseDir + this.mapEditorDir;

        this.tsOutputPath = 'js/';
		this.jsOutputPath = 'js/';
		this.partialsPath = 'partials/';
        this.allJavaScript = this.source;
        this.allTypeScript = this.source;

        this.libraryTypeScriptDefinitions = './typings/**/*.d.ts';

		this.release = {
			arch: "x64",
			electronVersion: "0.33.4",
			appVersion: "0.3",
			versionString: {
				"CompanyName": "PTS",
				"ProductName": "timetable-simulator"
			}
		};
	
		this.nodeModulesNeededForRelease = [
			"node_modules/material-design-icons/**/*",
			"node_modules/angular/angular.js",
			"node_modules/angular-ui-bootstrap/ui-bootstrap-tpls.js",
			"node_modules/jquery/jquery.js",
			"node_modules/bootstrap/dist/js/bootstrap.min.js",
			"node_modules/bootstrap/dist/css/bootstrap.min.css"
		];

		this.nodeModulesNeededForReleasePaths = [
			"node_modules/material-design-icons/",
			"node_modules/angular/",
			"node_modules/angular-ui-bootstrap/",
			"node_modules/jquery/",
			"node_modules/bootstrap/dist/js/",
			"node_modules/bootstrap/dist/css/"
		];

		this.otherFilesNeededForRelease = [
				"app/style.css"	
		];

		this.otherFilesNeededForReleasePaths = [
			"app/"
		];
		
    }
    return gulpConfig;
})();
module.exports = GulpConfig;