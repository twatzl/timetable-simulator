error: Uncaught TypeError: Cannot read property 'prototype' of undefined
explanation: currently a compiler problem of the tsc. when outputting to a single file the contents are not sorted correctly.
solution: output to single .js files and include them in the correct order in the .html file

error: duplicate definition of symbol 'xxx'
explanation: vsc reads the generated .d.ts files and finds additional definitions
solution: add "build" and "app/build" to the list of excluded files in tsconfig.json